import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Observable, Subject, fromEvent } from 'rxjs';
import {
  map,
  debounceTime,
  tap,
  distinctUntilChanged,
  startWith,
  takeUntil,
} from 'rxjs/operators';
import { StoreCategoriesInterface } from './interfaces/store-categories-interface';
import { SignInInterface } from './interfaces/sign-in-interface';
import { Router } from '@angular/router';
import { UpdateMetaDataAndTitleService } from './services/update-meta-data-and-title.service';
import { Meta } from '@angular/platform-browser';

import { Store, select } from '@ngrx/store';
import { applicationState } from './stores/reducers/application.reducer';
import { AuthStateInterface } from './stores/reducers/auth.reducers';
import { StoreStateInterface } from './stores/reducers/store.reducer';
import { ApplicationService } from './services/application.service';
import { GET_STORE_CATEGORIES } from './stores/actions/store.action';
import { GET_LAT_LNG_CITY } from './stores/actions/latLngCity.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
  public title = 'grossify';

  private Userdata: SignInInterface;

  public opened = false;

  public storeCategories: { [catId: number]: StoreCategoriesInterface };

  private ngUnsubscribe = new Subject();
  private _resize$: Observable<number>;
  public mobile$ = this.applicationservice.mobile$;
  // public tablet$ = this.applicationservice.tablet$;
  // public desktop$ = this.applicationservice.desktop$;

  public categoryHover = false; // for category menu oping and closing

  private getStoreCategoriesFromServer(): void {
    if (this.Userdata && this.Userdata.UserID) {
      this.store.dispatch(
        GET_STORE_CATEGORIES({ userId: this.Userdata.UserID })
      );
    } else {
      // user not logged in
      const UserId = 1;
      this.store.dispatch(GET_STORE_CATEGORIES({ userId: UserId }));
    }
  }

  public clickOnStoreCategorySubMenu(
    storeCategory: StoreCategoriesInterface
  ): void {
    // remove hover class after clicking
    this.categoryHover = false;

    // router navigation
    const id = storeCategory.cat_id;
    this.router.navigate(['/etc/store-category', id]);
  }

  // usesd for taking page up when user change router

  public onActivate(event: Event) {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

  constructor(
    private router: Router,
    private metaAndTitle: UpdateMetaDataAndTitleService,
    private meta: Meta,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
      store: StoreStateInterface;
    }>,
    private applicationservice: ApplicationService
  ) {
    this.meta.updateTag({ name: 'description', content: 'Home' });
    this.meta.updateTag({ name: 'keywords', content: 'Home, Shopping' });
    this.metaAndTitle.updateTitle('Home');

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        this.opened = data.sideNavBarState;
      });

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.storeCategories = data.StoreCatState.storeCategories;
      });
  }

  ngOnInit() {
    // get screen size
    this._resize$ = fromEvent(window, 'resize').pipe(
      takeUntil(this.ngUnsubscribe),
      debounceTime(200),
      map(() => window.innerWidth),
      distinctUntilChanged(),
      startWith(window.innerWidth),
      tap((width: number) => this.applicationservice.setWindowWidth(width))
      // tap(console.log)
    );
    this._resize$.subscribe();

    this.getStoreCategoriesFromServer();

    this.store.dispatch(GET_LAT_LNG_CITY());
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
