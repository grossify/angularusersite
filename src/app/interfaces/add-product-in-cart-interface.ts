export interface AddProductInCartInterface {
  status?: boolean;
  message?: string;
}

export interface AddProductInCartInputInterface {
  UserId: number;
  storeProductId: number;
  ProductUnitID: number;
  qty: number;
  storeId: number;
}