export interface GetNearByStoreInterface {
  num_of_items: number;
  Limit: number;
  Offset: number;
  result: Array<GetNearByStoreItemInterface>;
}

export interface GetNearByStoreItemInterface {
  store_id?: number;
  store_code?: number;
  store_name?: string;
  min_amt?: number;
  store_address?: string;
  store_city?: string;
  store_state?: string;
  store_pincode?: string;
  owner_name?: string;
  contact_no?: string;
  email_id?: string;
  latitude?: string;
  longitude?: string;
  owner_photo?: string;
  store_photo?: string;
  status?: boolean;
  created_date?: Date;
  avg_rating?: number;
  cat_id?: number;
  distance_in_meter?: number;
}

export interface GetNearByStoreReqInterface {
  userId?: number;
  latitude?: number;
  longitude?: number;
  pincode?: string;
  limit?: number;
  offset?: number;
  city?: string;
}
