export interface UserAddressInterface {
  address_id?: number;
  address_line1?: string;
  address_line2?: string;
  city?: string;
  state?: string;
  country?: string;
  postal_code?: string;
  latitude?: number;
  longitude?: number;
  status?: boolean;
  UserId?: number;
}

export interface UserAddressInputInterface {
  userid?: number;
  address_line1?: string;
  address_line2?: string;
  city?: string;
  state?: string;
  country?: string;
  postal_code?: string;
  lattitude?: number;
  longitude?: number;
}
