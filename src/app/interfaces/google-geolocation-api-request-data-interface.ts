export interface CellTowerInterface {
  cellId?: number;
  locationAreaCode?: number;
  mobileCountryCode?: number;
  mobileNetworkCode?: number;
  age?: number;
  signalStrength?: number;
  timingAdvance?: number;
}

export interface CellTowersInterface {
  cellTowers?: CellTowerInterface[];
}

export interface WifiAccessPointsInterface {
  macAddress?: string;
  signalStrength?: number;
  age?: number;
  channel?: number;
  signalToNoiseRatio?: number;
}

export interface GoogleGeolocationAPIRequestDataInterface {
  homeMobileCountryCode?: number;
  homeMobileNetworkCode?: number;
  radioType?: string;
  carrier?: string;
  considerIp?: boolean;
  cellTowers?: CellTowersInterface[];
  wifiAccessPoints?: WifiAccessPointsInterface[];
}
