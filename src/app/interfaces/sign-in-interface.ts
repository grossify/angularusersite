export interface SignInInterface {
  UserID?: number;
  UserEmail?: string;
  UserPassword?: string;
  UserName?: string;
  FirstName?: string;
  LastName?: string;
  UserCity?: string;
  UserState?: string;
  UserZip?: string;
  UserEmailVerified?: boolean;
  UserRegistrationDate?: Date;
  UserVerificationCode?: string;
  UserIP?: string;
  UserPhone?: string;
  UserCountry?: string;
  isAddressAvilabe?: boolean;
  userProfile?: string;
  DOB?: Date;
  Gender?: string;
  Altername_Number?: string;
}

export interface LoginIputDataInterface {
  username: string,
  password: string
}
