export interface NewUserInterface {
  UserID?: number;
  CustomerName?: string;
  OrderShipAddress?: string;
  OrderCity?: string;
  OrderState?: string;
  OrderPin?: string;
  OrderCountry?: string;
  OrderPhone?: string;
  OrderEmail?: string;
  status?: string;
  OrderNote?: string;
}
