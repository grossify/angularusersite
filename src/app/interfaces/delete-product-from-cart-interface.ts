export interface DeleteProductFromCartInterface {
}

export interface DeleteProductInCartInputInterface  {
    UserId: number;
    storeProductId: number;
    ProductUnitID: number;
    storeId: number;
}