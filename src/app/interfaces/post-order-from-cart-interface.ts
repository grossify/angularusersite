export interface PostOrderFromCartInterface {
  store_name?: string;
  OrderNo?: number;
  OrderID?: number;
}

export interface PostOrdersReqInterface {
  UserID?: number;
  storeId?: number;
  OrderNote?: string;
}