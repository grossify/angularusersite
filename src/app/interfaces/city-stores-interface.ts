export interface CityStoresInterface {
    num_of_stores: number;
    result: Array<GetCityStoresItemInterface>;
}

export interface GetCityStoresItemInterface {
    store_id?: number;
    store_name?: string;
    store_address?: string;
    owner_name?: string;
    contact_no?: string;
    owner_photo?: string | null;
    store_photo?: string;
    store_city?: string;
    store_pincode?: string;
    email_id?: string;
    min_amt?: number;
    DeliveryRate?: number;
    DeliveryRanage?: string | null;
    slug?: string | null;
    avg_rating?: number;
    file_name?: string | null;
}
  
export interface CityStoresReqInterface {
    userId: number;
    city: string;
}
