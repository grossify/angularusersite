export interface UpdateEndUserProfileInterface {
    status?: boolean;
    message?: string;
    profileImage?: string;
}
