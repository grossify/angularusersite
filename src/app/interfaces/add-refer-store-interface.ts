export interface AddReferStoreInterface {
  ref_store_id?: number;
  store_name?: string;
  stor_address?: string;
  owner_name?: string;
  contact_number?: string;
  email?: string;
  received_date?: Date;
}
