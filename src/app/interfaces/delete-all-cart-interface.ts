export interface DeleteAllCartInterface {
    status?: boolean;
    message?: string;
}
