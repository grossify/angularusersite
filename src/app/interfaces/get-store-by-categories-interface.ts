export interface GetStoreByCategoriesInterface {
  num_of_stores?: number;
  result?: Array<GetStoreByCategoriesItemInterface>;
}

export interface GetStoreByCategoriesItemInterface {
  cat_id?: number;
  cat_name?: string;
  cat_desc?: string;
  file_name?: string;
  status?: boolean;
  parent_category?: string;
  category_level?: number;
  slug?: string;
  store_id?: number;
  store_name?: string;
  store_address?: string;
  owner_name?: string;
  contact_no?: string;
  owner_photo?: string;
  store_photo?: string;
  cat_type_image?: string;
}


export interface StoreByCategoriesReqInterface {
  userId?: number;
  latitude?: number;
  longitude?: number;
  pincode?: string;
  city?: string;
  limit?: number;
  offset?: number;
  catId?: number;
}

export interface StoreByCategoriesReqInterface {
  userId?: number;
  latitude?: number;
  longitude?: number;
  pincode?: string;
  city?: string;
  limit?: number;
  offset?: number;
  catId?: number;
}
