export interface GetProductDetailsInterface {
  store_product_id?: number;
  product_name?: string;
  shelf_life?: string;
  slug?: string;
  status?: boolean;
  prod_id?: number;
  store_id?: number;
  created_date?: Date;
  cat_id?: number;
  prod_title?: string;
  prod_desc?: string;
  store_cat_id?: number;
  prod_status?: boolean;
  prod_images?: string;
  product_brand?: string;
  product_type?: string;
  common_name?: string;
  organic?: string;
  manufacturer?: string;
  images?: Array<string>;
}
