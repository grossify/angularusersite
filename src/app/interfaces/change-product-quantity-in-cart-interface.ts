export interface ChangeProductQuantityInCartInterface {
  status?: boolean;
  message?: string;
}

export interface ChangeProductFromCartInputInterface {
  UserId: number;
  storeProductId: number;
  ProductUnitID: number;
  storeId: number;
  qty: number;
}