export interface StoreCategoriesInterface {
  cat_id?: number;
  cat_name?: string;
  cat_desc?: string;
  file_name?: string;
  status?: string;
  parent_category?: string;
  category_level?: string;
  slug?: string;
  cat_type_image?: string;
}
