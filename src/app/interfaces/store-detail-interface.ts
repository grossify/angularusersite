export interface StoreDetailInterface {
  store_id?: number;
  store_name?: string;
  store_address?: string;
  owner_name?: string;
  contact_no?: string;
  owner_photo?: string;
  store_photo?: string;
  store_code?: number;
  min_amt?: number;
  store_city?: string;
  store_state?: string;
  store_pincode?: number;
  status?: number;
  created_date?: Date;
  avg_rating?: number;
  cat_id?: number;
}

export interface StoreDetailReqInterface {
  UserId: number;
  StoreId: number;
}
