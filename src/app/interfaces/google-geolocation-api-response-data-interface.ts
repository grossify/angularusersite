export interface LocationInterface {
  lat?: number;
  lng?: number;
}

export interface GoogleGeolocationAPIResponseDataInterface {
  location?: LocationInterface;
  accuracy?: number;
}

export interface ErrorsInterface {
  domain?: string;
  reason?: string;
  message?: string;
}

export interface ErrorInterface {
  errors?: ErrorsInterface[];
  code?: number;
  message?: string;
}

export interface GoogleGeolocationAPIErrorsInterface {
  error?: ErrorInterface;
}
