export interface UpdateEndUserInterface {
  UserID?: number;
  DOB?: Date;
  Gender?: string;
  UserPhone?: string;
  Altername_Number?: string;
  FirstName?: string;
  LastName?: string;
  UserCity?: string;
  UserState?: string;
  UserCountry?: string;
  UserZip?: string;
}
