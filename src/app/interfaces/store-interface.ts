export interface StoreInterface {
  store_code?: string;
  store_name?: string;
  store_address?: string;
  store_city?: string;
  store_state?: string;
  store_pincode?: string;
  store_license_no?: string;
  store_gst_no?: string;
  owner_name?: string;
  contact_no?: string;
  email_id?: string;
  from_date?: Date;
  to_date?: Date;
  min_amt?: string;
  latitude?: string;
  longitude?: string;
  owner_photo?: string;
  store_photo?: string;
  status?: boolean;
  created_date?: Date;
  user_id?: number;
  cat_id?: number;
  slug?: string;
}
