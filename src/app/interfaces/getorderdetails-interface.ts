export interface GetorderdetailsInterface {
  OrderDetailID?: number;
  StoreOrderID?: number;
  DetailProductID?: number;
  ProductName?: string;
  ProductPrice?: number;
  ProductQuantity?: number;
  ProductUnit?: number;
  ProductDiscount?: number;
  ProductTAX?: number;
  TotalPrice?: number;
  OrderID?: number;
}
