export interface GetCartByUserIdInterface {
  message?: string;
  num_of_items?: number;
  total_price?: number;
  store?: Array<StoreDetaisInterface>;
  result: Array<CartItemArrayInterface>
}


export interface StoreDetaisInterface {
  store_id?: number;
  store_code?: number;
  store_name?: string;
  min_amt?: number;
  store_address?: string;
  store_city?: string;
  store_state?: string;
  store_pincode?: string;
  store_license_no?: string;
  store_gst_no?: string;
  owner_name?: string;
  contact_no?: string;
  email_id?: string;
  latitude?: string;
  longitude?: string;
  owner_photo?: string;
  store_photo?: string;
  status?: boolean;
  created_date?: Date;
  DeliveryRate?: number;
  DeliveryRanage?: string;
}

export interface CartItemArrayInterface {
  prod_title?: string;
  shelf_life?: number;
  prod_desc?: string;
  prod_images?: string;
  price?: number;
  unit?: string;
  cart_id?: number;
  store_product_id?: number;
  punitid?: number;
  qty?: number;
  store_id?: number;
  user_id?: number;
  store_name?: string;
  min_amt?: number;
  store_address?: string;
  UserName?: string;
}

export interface GetCartEmptyRes {
  status: boolean;
  message: string;
}