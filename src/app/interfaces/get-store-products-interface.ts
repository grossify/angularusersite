export interface PricesInterface {
  punitid?: number;
  unit?: string;
  price?: number;
}

export interface GetStoreProductsInterface {
  num_of_items?: number;
  Limit?: number;
  Offset?: number;
  result: Array<GetStoreProductsItemInterface>
}



export interface GetStoreProductsItemInterface {
  store_product_id?: number;
  product_name?: string;
  shelf_life?: number;
  slug?: string;
  status?: boolean;
  prod_id?: number;
  store_id?: number;
  created_date?: Date;
  cat_id?: number;
  prod_title?: string;
  prod_desc?: string;
  store_cat_id?: number;
  prod_status: boolean;
  prod_images?: string;
  product_brand?: string;
  product_type?: string;
  common_name?: string;
  organic?: string;
  manufacturer?: string;
  images?: string[];
  prices?: PricesInterface[];
}


export interface StoreProductsReqInterface {
  StoreId?: number;
  CatId?: number;
  EndUserId?: number;
  limit?: number;
  offset?: number;
}