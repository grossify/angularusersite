export interface GetOrdersInterface {
  num_of_items: number;
  Limit: number;
  offset: number;
  result: Array<OrderItemInterface>
}

export interface OrderItemInterface {
  OrderID?: number;
  OrderNo?: number;
  UserID?: number;
  CustomerName?: string;
  OrderShipAddress?: string;
  OrderCity?: string;
  OrderState?: string;
  OrderPin?: number;
  OrderCountry?: string;
  OrderPrice?: number;
  DeliveryRate?: number;
  TotalQuantity?: number;
  TotalDiscount?: number;
  OrderPhone?: string;
  OrderTax?: number;
  OrderEmail?: string;
  OrderDate?: Date;
  store_id?: number;
  status?: number;
}

export interface GetOrdersInputInterface {
  userId: number;
  limit?: number;
  offset?: number;
}
