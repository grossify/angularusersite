export interface UserInterface {
  UserID?: number;
  UserEmail?: string;
  UserName?: string;
  FirstName?: string;
  LastName?: string;
  UserCity?: string;
  UserState?: string;
  UserZip?: string;
  UserEmailVerified?: boolean;
  UserRegistrationDate?: Date;
  UserPhone?: string;
  UserCountry?: string;
  isAddressAvilabe?: boolean;
  userProfile?: string;
  DOB?: Date;
  Gender?: string;
  Altername_Number?: string;
}
