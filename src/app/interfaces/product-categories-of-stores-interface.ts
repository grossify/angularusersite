export interface ProductCategoriesOfStoresInterface {
  cat_id?: number;
  cat_name?: string;
  cat_desc?: string;
  file_name?: string;
  status?: boolean;
  cat_type?: string;
  slug?: string;
  store_product_id?: number;
  product_name?: string;
  shelf_life?: string;
  prod_id?: number;
  store_id?: number;
  created_date?: Date;
}
