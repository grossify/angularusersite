export interface AddFavouriteStoreInterface {
  ID?: number;
  store_id?: number;
  end_user_id?: number;
}


export interface AddFavouriteStoreReqInterface {
  store_id?: number;
  end_user_id?: number;
}
