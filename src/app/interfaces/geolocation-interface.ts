export interface GeolocationCoordinatesInterface {
	latitude?: number;
	longitude?: number;
	altitude?: number;
	accuracy?: number;
	altitudeAccuracy?: number;
	heading?: number;
	speed?: number;
}

export interface GeolocationPositionInterface {
	coords: GeolocationCoordinatesInterface,
	timestamp: Date
}
