export interface GooglePlaceAutocompleteResponseDataInterface {
  status?: string;
  predictions?: Predictions[];
}

export interface Predictions {
  description?: string;
  distance_meters?: number;
  id?: string;
  matched_substrings?: MatchedSubstrings;
  place_id?: string;
  reference?: string;
  terms?: Terms;
  types?: string[];
}

export interface MatchedSubstrings {
  length?: number;
  offset?: number;
}

export interface Terms {
  offset?: number;
  value?: string;
}
