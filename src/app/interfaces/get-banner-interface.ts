export interface GetBannerInterface {
  ID?: number;
  title?: string;
  file_name?: string;
  user_id?: number;
  created_date?: Date;
}
