export interface FevouriteStoresInterface {
  num_of_items: number;
  Limit: number;
  Offset: number;
  result: Array<GetNearByFavouriteStoreItemInterface>;
}

export interface GetNearByFavouriteStoreItemInterface {
  store_id?: number;
  store_code?: number;
  store_name?: string;
  min_amt?: number;
  store_address?: string;
  store_city?: string;
  store_state?: string;
  store_pincode?: string;
  store_license_no?: string;
  store_gst_no?: string;
  owner_name?: number;
  contact_no?: string;
  email_id?: string;
  latitude?: string;
  longitude?: string;
  owner_photo?: string;
  store_photo?: string;
  status?: boolean;
  created_date?: Date;
  avg_rating?: number;
  distance_in_meter?: number;
}

export interface GetNearByFevouriteStoresReqInterface {
  userId?: number;
  latitude?: number;
  longitude?: number;
  pincode?: string;
  limit?: number;
  offset?: number;
  city?: string;
}

export interface RemoveFavouriteStoreReqInterface {
  store_id?: number;
  end_user_id?: number;
}

export interface RemoveFavouriteStoreResInterface {
  status?: boolean;
  message?: string;
}
