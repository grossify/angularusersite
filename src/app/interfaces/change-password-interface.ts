export interface ChangePasswordInputInterface {
    newPass: string;
    oldPass: string;
    userId: number;
}

export interface ChangePasswordResponseSuccessInterface {
    "status": boolean;
    "message": string;      // "password changed successful ,log in! "
}

export interface ChangePasswordResponseErrorInterface {
    "status": boolean;
    "data": {
        "status": number;   // 422
        "msg": string;   // "password does not match."
    }
}
