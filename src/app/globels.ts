// export const baseServerUrl = 'http://grossify.tk/webapi/Api/';
// export const baseServerUrl = 'http://grossifyin.000webhostapp.com/webapi/Api/';
// export const baseServerUrl = 'http://localhost/webapi/Api/';
// export const baseServerUrl = 'http://localhost/grossify_php/webapi/Api/';
// export const baseServerUrl = 'https://staging.grossify.in/webapi/Api/';
export const baseServerUrl = 'https://staging.grossify.in/webapi/';
export const AuthKey = 'bf4335bac47567d5167cf53678d1f7aafad30d31';
// export const GoogleAPIKey = 'AIzaSyA1qVDB8E8MXzQNGxfK6hRUrZdEuUQUlFs';
// export const GoogleAPIKey = 'AIzaSyCxgLB3TwtuHq17nMKhM1-agLJtY8cUY0c';
// export const GoogleAPIKey = 'AIzaSyBwj16vXw3s00Bwg_Zn6slSGSPk0hR5OGQ';

// export const GoogleAPIKey = 'AIzaSyDoYUa5syJBB9do8IlyLAA3qvHeZgA74pM';
export const GoogleAPIKey = 'AIzaSyASynNXpP9v040cNSh2f_A8XVnPkQ5mUEY';
export const LocationKey = '^&*^$@@!546gjgTU%$';
export const UserDataKey = '^%####3336)(*:K:GL$@@!546gjgTU%$';

// firebaseConfig is also in /assets/js/firebase-messaging-sw.js
export const firebaseConfig = {
  apiKey: 'AIzaSyCmzFDuMEmjtjutBfVgLCdIwAehMU8ifIg',
  authDomain: 'grossify-v1.firebaseapp.com',
  databaseURL: 'https://grossify-v1.firebaseio.com',
  projectId: 'grossify-v1',
  storageBucket: 'grossify-v1.appspot.com',
  messagingSenderId: '293070585185',
  appId: '1:293070585185:web:62e7b34a04cc32b37a1920'
};

export const OrderStatus = {
  0: {
    status: 'New',
    message: ''
  },
  1: {
    status: 'Rejected',
    message: ''
  },
  2: {
    status: 'Accepted',
    message: ''
  },
  3: {
    status: 'Change Back',
    message: ''
  },
  4: {
    status: 'Complete',
    message: ''
  },
  5: {
    status: 'Denied',
    message: ''
  },
  6: {
    status: 'Expired',
    message: ''
  },
  7: {
    status: 'Failed',
    message: ''
  },
  8: {
    status: 'Pending',
    message: ''
  },
  9: {
    status: 'Processed',
    message: ''
  },
  10: {
    status: 'Processing',
    message: ''
  },
  11: {
    status: 'Refunded',
    message: ''
  },
  12: {
    status: 'Reversed',
    message: ''
  },
  13: {
    status: 'Skipped',
    message: ''
  },
  14: {
    status: 'Voided',
    message: ''
  },
  15: {
    status: 'Rejected by Admin',
    message: ''
  }
};

export const IndianStateAndUnionTerritoriesData = [
  {
    stateName: 'Andhra Pradesh',
    stateValue: 'Andhra Pradesh'
  },
  {
    stateName: 'Arunachal Pradesh',
    stateValue: 'Arunachal Pradesh'
  },
  {
    stateName: 'Assam',
    stateValue: 'Assam'
  },
  {
    stateName: 'Bihar',
    stateValue: 'Bihar'
  },
  {
    stateName: 'Chhattisgarh',
    stateValue: 'Chhattisgarh'
  },
  {
    stateName: 'Goa',
    stateValue: 'Goa'
  },
  {
    stateName: 'Gujarat',
    stateValue: 'Gujarat'
  },
  {
    stateName: 'Haryana',
    stateValue: 'Haryana'
  },
  {
    stateName: 'Himachal Pradesh',
    stateValue: 'Himachal Pradesh'
  },
  {
    stateName: 'Jammu and Kashmir',
    stateValue: 'Jammu and Kashmir'
  },
  {
    stateName: 'Jharkhand',
    stateValue: 'Jharkhand'
  },
  {
    stateName: 'Karnataka',
    stateValue: 'Karnataka'
  },
  {
    stateName: 'Kerala',
    stateValue: 'Kerala'
  },
  {
    stateName: 'Madhya Pradesh',
    stateValue: 'Madhya Pradesh'
  },
  {
    stateName: 'Maharashtra',
    stateValue: 'Maharashtra'
  },
  {
    stateName: 'Manipur',
    stateValue: 'Manipur'
  },
  {
    stateName: 'Meghalaya',
    stateValue: 'Meghalaya'
  },
  {
    stateName: 'Mizoram',
    stateValue: 'Mizoram'
  },
  {
    stateName: 'Nagaland',
    stateValue: 'Nagaland'
  },
  {
    stateName: 'Odisha',
    stateValue: 'Odisha'
  },
  {
    stateName: 'Punjab',
    stateValue: 'Punjab'
  },
  {
    stateName: 'Rajasthan',
    stateValue: 'Rajasthan'
  },
  {
    stateName: 'Sikkim',
    stateValue: 'Sikkim'
  },
  {
    stateName: 'Tamil Nadu',
    stateValue: 'Tamil Nadu'
  },
  {
    stateName: 'Telangana',
    stateValue: 'Telangana'
  },
  {
    stateName: 'Tripura',
    stateValue: 'Tripura'
  },
  {
    stateName: 'Uttar Pradesh',
    stateValue: 'Uttar Pradesh'
  },
  {
    stateName: 'Uttarakhand',
    stateValue: 'Uttarakhand'
  },
  {
    stateName: 'West Bengal',
    stateValue: 'West Bengal'
  },
  {
    stateName: 'Andaman and Nicobar Islands',
    stateValue: 'Andaman and Nicobar Islands'
  },
  {
    stateName: 'Chandigarh',
    stateValue: 'Chandigarh'
  },
  {
    stateName: 'Dadra and Nagar Haveli',
    stateValue: 'Dadra and Nagar Haveli'
  },
  {
    stateName: 'Daman and Diu',
    stateValue: 'Daman and Diu'
  },
  {
    stateName: 'Delhi',
    stateValue: 'Delhi'
  },
  {
    stateName: 'Lakshadweep',
    stateValue: 'Lakshadweep'
  },
  {
    stateName: 'Puducherry (Pondicherry)',
    stateValue: 'Puducherry'
  }
];

export const CountriesData = [
  {
    countryName: 'India',
    countryValue: 'India'
  }
];
