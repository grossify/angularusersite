import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';

import { EtcComponent } from './etc.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingSecurityComponent } from './components/setting-security/setting-security.component';
import { SettingAddressComponent } from './components/setting-address/setting-address.component';
import { OrdersComponent } from './components/orders/orders.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { OffersComponent } from './components/offers/offers.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ReferStoreComponent } from './components/refer-store/refer-store.component';
import { CartComponent } from './components/cart/cart.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { StoreDetailComponent } from './components/store-detail/store-detail.component';
import { StoreCategoryComponent } from './components/store-category/store-category.component';
import { NearbyStoresComponent } from './components/nearby-stores/nearby-stores.component';
import { FavouriteStoresComponent } from './components/favourite-stores/favourite-stores.component';
import { StoreProductComponent } from './components/store-product/store-product.component';
import { OrderPlacedComponent } from './components/order-placed/order-placed.component';
import { TrackOrderComponent } from './components/track-order/track-order.component';
import { CancelOrderComponent } from './components/cancel-order/cancel-order.component';
import { StoresComponent } from './components/stores/stores.component';
import { PlaceOrderComponent } from './components/place-order/place-order.component';
import { CityStoresComponent } from './components/city-stores/city-stores.component';
import { EditAddressComponent } from './components/edit-address/edit-address.component';

import { UpdateUserDataDialogComponent } from '../dialogs/update-profile-data.dialog';
import { ProfilePicUploderDialogComponent } from '../dialogs/profile-pic-uploder.dialog';
import { UpdateAddressComponent } from '../dialogs/update-address-dialog.component';
import { UpdatePasswordDialogComponent } from '../dialogs/update-password.dialog';
import { SameStoreItemsDailogComponent } from '../dialogs/app-same-store-items.dialog';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home',
    // component: EtcComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'registration',
    component: RegistrationComponent,
  },
  {
    path: 'settings',
    component: SettingsComponent,
    children: [
      {
        path: '',
        redirectTo: 'security',
        pathMatch: 'full',
      },
      {
        path: 'security',
        component: SettingSecurityComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'address',
        children: [
          {
            path: '',
            redirectTo: 'view-address',
            pathMatch: 'full',
          },
          {
            path: 'view-address',
            component: SettingAddressComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'edit-address',
            component: EditAddressComponent,
            canActivate: [AuthGuard],
          },
        ],
      },
    ],
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'offers',
    component: OffersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'refer-store',
    component: ReferStoreComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'order-details/:orderId',
    component: OrderDetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'store-detail/:id',
    component: StoreDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'store-category/:id',
    component: StoreCategoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'nearby-stores',
    component: NearbyStoresComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'favourite-stores',
    component: FavouriteStoresComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'city-stores',
    component: CityStoresComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'stores/:id',
    component: StoresComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'store-product/:id',
    component: StoreProductComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'order-placed',
    component: OrderPlacedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'track-order/:orderId',
    component: TrackOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'cancel-order/:orderId',
    component: CancelOrderComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'place-order',
    component: PlaceOrderComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EtcRoutingModule {}

export const appRoutingExport = [
  EtcComponent,
  LoginComponent,
  RegistrationComponent,
  SettingsComponent,
  SettingSecurityComponent,
  SettingAddressComponent,
  OrdersComponent,
  UserProfileComponent,
  OffersComponent,
  LoginComponent,
  RegistrationComponent,
  CategoriesComponent,
  ReferStoreComponent,
  CartComponent,
  OrderDetailsComponent,
  StoreDetailComponent,
  StoreCategoryComponent,
  NearbyStoresComponent,
  FavouriteStoresComponent,
  StoreProductComponent,
  OrderPlacedComponent,
  TrackOrderComponent,
  CancelOrderComponent,
  StoresComponent,
  PlaceOrderComponent,
  CityStoresComponent,
  ProfilePicUploderDialogComponent,
  UpdateUserDataDialogComponent,
  UpdatePasswordDialogComponent,
  SameStoreItemsDailogComponent,
  UpdateAddressComponent,
  EditAddressComponent,
];
