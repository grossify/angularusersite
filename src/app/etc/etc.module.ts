import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { EtcRoutingModule, appRoutingExport } from './etc-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LayoutModule } from '@angular/cdk/layout';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

import { GoogleAPIKey } from '../globels';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';

@NgModule({
  declarations: [...appRoutingExport],
  imports: [
    CommonModule,
    SharedModule,
    EtcRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    ImageCropperModule,
    MatSidenavModule,
    MatMenuModule,
    MatTooltipModule,
    MatSelectModule,
    LayoutModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    AgmCoreModule.forRoot({
      apiKey: GoogleAPIKey,
      language: 'en',
      region: 'IN',
      libraries: ['places'],
    }),
  ],
  providers: [
    GoogleMapsAPIWrapper,
  ]
})
export class EtcModule {}
