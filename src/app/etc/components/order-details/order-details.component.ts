import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  GetOrdersInputInterface,
  OrderItemInterface,
} from '../../../interfaces/get-orders-interface';
import { GetorderdetailsInterface } from '../../../interfaces/getorderdetails-interface';
import { UserInterface } from '../../../interfaces/user-interface';
import { OrderStatus } from '../../../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { OrderState } from '../../../stores/reducers/order.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import {
  GET_ALL_ORDERS,
  GET_ORDER_DETALS,
} from '../../../stores/actions/order.action';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  public Userdata: UserInterface;

  public allOrder: OrderItemInterface;
  public orderDetail: Array<GetorderdetailsInterface>;
  private orderId: number;
  public orderStatus = OrderStatus;

  private getAllOrdersFromServer(): void {
    const reqData: GetOrdersInputInterface = {
      userId: this.Userdata.UserID,
      limit: 0,
      offset: 20,
    };

    this.store.dispatch(GET_ALL_ORDERS({ reqData: reqData }));
  }

  private getRouteParameter(): void {
    // const orderId = this.route.snapshot.paramMap.get('orderId');
    // this.orderId = parseInt(orderId, 10);
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.orderId = parseInt(params.get('orderId'), 10);
      this.store.dispatch(GET_ORDER_DETALS({ orderId: this.orderId }));
    });
  }

  private getAllOrderDetailsFromServer(): void {
    this.store.dispatch(GET_ORDER_DETALS({ orderId: this.orderId }));
  }

  public clickOnTrackOrderButton(order: OrderItemInterface): void {
    const id = order.OrderID;
    this.router.navigate(['/etc/track-order', id]);
  }

  public clickOnCancelOrderButton(order: OrderItemInterface): void {
    const id = order.OrderID;
    this.router.navigate(['/etc/cancel-order', id]);
  }

  public clickOnContactToSellerButton(order: OrderItemInterface): void {
    const id = order.store_id;
    this.router.navigate(['/etc/store-detail', id]);
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<{
      order: OrderState;
      authState: AuthStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getRouteParameter();
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.Userdata = data.user[0];
      });

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('order'))
      .subscribe((data: OrderState) => {
        if (
          data.getOrderDetailsState &&
          data.getOrderDetailsState.orderDetail
        ) {
          this.orderDetail = data.getOrderDetailsState.orderDetail;
          this.cd.markForCheck();
        }
        if (data && data.getAllOrderState && !data.getAllOrderState.loaded) {
          // this.getAllOrdersFromServer();
        }
        if (data && data.getAllOrderState) {
          if (data.getAllOrderState.ordersRes) {
            this.allOrder = data.getAllOrderState.orders[this.orderId];
            this.cd.markForCheck();
          }
        }
      });
  }

  ngOnInit() {
    this.getAllOrdersFromServer();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
