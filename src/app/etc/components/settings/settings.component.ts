import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../../stores/reducers/application.reducer';
import {
  OPEN_MAIN_SIDENAV_BAR,
  CLOSE_MAIN_SIDENAV_BAR,
  CHANGE_SELECTED_SETTING_SIDENAV_BAR_ITEM,
} from '../../../stores/actions/application.action';

import { ApplicationService } from '../../../services/application.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();

  public mobileQueryMain: boolean;
  public mobile$ = this.applicationservice.mobile$;

  public opened = true;

  public clickOnSetting(selectedSetting: string): void {
    this.store.dispatch(
      CHANGE_SELECTED_SETTING_SIDENAV_BAR_ITEM(selectedSetting)
    );
  }

  constructor(
    private store: Store<{ applicationState: applicationState }>,
    private applicationservice: ApplicationService
  ) {
    // subscribe to store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        this.opened = data.settingSideNavBarState;
      });
  }

  ngOnInit() {
    this.store.dispatch(CLOSE_MAIN_SIDENAV_BAR());
  }

  ngOnDestroy() {
    this.store.dispatch(OPEN_MAIN_SIDENAV_BAR());
  }
}
