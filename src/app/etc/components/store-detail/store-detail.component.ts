import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { StoreDetailInterface, StoreDetailReqInterface } from '../../../interfaces/store-detail-interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UserInterface } from '../../../interfaces/user-interface';

import { Store, select } from '@ngrx/store';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_STORE_DETAIL } from '../../../stores/actions/store.action';

@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoreDetailComponent implements OnInit, OnDestroy {
  public storeDetail: StoreDetailInterface;
  public storeId: number;
  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';
  public ownerPlaceholder = './assets/images/logo/profile-picture-female.png';

  private ngUnsubscribe = new Subject();
  public Userdata: UserInterface;

  public productCategoriesLoading: boolean;
  public productCategoriesLoaded: boolean;

  private getStoreDetailFromServer(): void {
    const reqData: StoreDetailReqInterface = {
      UserId: this.Userdata.UserID,
      StoreId: this.storeId
    };
    this.store.dispatch(GET_STORE_DETAIL({getStoreDetail: reqData}))
  }

  private getRouteParameter(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.storeId = parseInt(params.get('id'), 10);
    });
  }

  constructor(
    private route: ActivatedRoute,
    private store: Store<{
      store: StoreStateInterface,
      authState: AuthStateInterface
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getRouteParameter();
      // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });


    // subscribe to store's store state
    store
      .pipe(
      takeUntil(this.ngUnsubscribe),
      select('store')
      )
      .subscribe( (data: StoreStateInterface) => {
        this.productCategoriesLoading = data.getStoreDetalsState.loading;
        this.productCategoriesLoaded = data.getStoreDetalsState.loaded;

        if (data.getStoreDetalsState.loaded && data.getStoreDetalsState.loaded) {
          this.storeDetail = data.getStoreDetalsState.storeDetails;
          this.cd.markForCheck();
        }

    });
  }

  ngOnInit() {
    this.getStoreDetailFromServer();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
