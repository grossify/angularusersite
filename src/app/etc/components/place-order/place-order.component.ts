import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';
import { Order } from '../../../classes/order';
import { IndianStateAndUnionTerritoriesData } from '../../../globels';
import { UserInterface } from '../../../interfaces/user-interface';
import { GetCartByUserIdInterface } from '../../../interfaces/get-cart-by-user-id-interface';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { CartStateInterface } from '../../../stores/reducers/cart.reducer';
import { POST_ORDER_FROM_CART } from '../../../stores/actions/order.action';


@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaceOrderComponent implements OnInit, OnDestroy {
  public selectedIndex = 1;
  public UserAddress: UserAddressInterface;
  public Userdata: UserInterface;
  private ngUnsubscribe = new Subject();
  public stateData = IndianStateAndUnionTerritoriesData;
  public totalAmount: number;
  public OrderData = new Order();
  public cartItems: GetCartByUserIdInterface;


  public clickOnTab(index: number): void {
    if (this.selectedIndex === index) {
      return;
    }
    if (this.selectedIndex === 1 && (index === 2 || index === 3)) {
      this.selectedIndex = index;
    }
    this.selectedIndex = index;
  }

  public clickOnDeleverOnThisAddressButton(): void {
    this.OrderData.UserID = this.Userdata.UserID;
    this.OrderData.storeId = this.cartItems.store[0].store_id

    this.selectedIndex = 2;
  }

  public clickOnContinueOrder(): void {
    // if (!this.orderIsValid) {
    //   this.toastr.error(
    //     'Only products from one seller is allowed, please shop form one seller at time. ' +
    //       'Remove extra products from cart.',
    //     'Error',
    //     {
    //       timeOut: 3000,
    //       closeButton: true
    //     }
    //   );
    //   return;
    // }
  }

  public clickOnReviewBackButton(): void {
    this.selectedIndex = 1;
  }

  public clickOnContinueOrderButton(): void {
    this.selectedIndex = 3;
  }

  public clickOnOrderBackButton(): void {
    this.selectedIndex = 2;
  }


  public clickOnPlaceOrderButton(): void {
    // place order
    this.store.dispatch(POST_ORDER_FROM_CART({orderReq: this.OrderData}));
  }

  constructor(
    public dialog: MatDialog,
    private store: Store<{
      authState: AuthStateInterface,
      address: AddressStateInterface,
      cart: CartStateInterface
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to address store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('address')
      )
      .subscribe( (data: AddressStateInterface) => {
        if (data.userAddressState && data.userAddressState.UserAddress && data.userAddressState.UserAddress.city) {
          this.cd.markForCheck();
          this.UserAddress = data.userAddressState.UserAddress;
        }
    });

    // subscribe to cart store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('cart')
      )
      .subscribe( (data: CartStateInterface) => {
        this.cd.markForCheck();
        this.cartItems = data.CartState.Cart;

        if (data.CartState.loaded) {
          this.totalAmount = data.CartState.Cart.total_price;
        }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
