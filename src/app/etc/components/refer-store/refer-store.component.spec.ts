import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferStoreComponent } from './refer-store.component';

describe('ReferStoreComponent', () => {
  let component: ReferStoreComponent;
  let fixture: ComponentFixture<ReferStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
