import { Component, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AddReferStoreClass } from '../../../classes/add-refer-store-class';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SignInInterface } from '../../../interfaces/sign-in-interface';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { POST_REFER_STORE } from '../../../stores/actions/store.action';

@Component({
  selector: 'app-refer-store',
  templateUrl: './refer-store.component.html',
  styleUrls: ['./refer-store.component.scss']
})
export class ReferStoreComponent implements OnDestroy {
  private ngUnsubscribe = new Subject();
  public model: AddReferStoreClass = new AddReferStoreClass();
  private Userdata: SignInInterface;
  public submitButtonValid: boolean;

  public OnFormSubmit(): boolean {
    // validation
    if (!this.model.contact_number) {
      this.toastr.error('Contact Number is Required', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      return false;
    }
    if (!this.model.email) {
      this.toastr.error('Email is Required', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      return false;
    }
    if (!this.model.owner_name) {
      this.toastr.error('Owner Name is Required', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      return false;
    }
    if (!this.model.stor_address) {
      this.toastr.error('Store Address is Required', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      return false;
    }
    if (!this.model.store_name) {
      this.toastr.error('Store Name is Required', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      return false;
    }
    this.toastr.info('Please Wait !', 'info', {
      timeOut: 3000,
      closeButton: true
    });

    this.model.userid = this.Userdata.UserID;

    this.store.dispatch(POST_REFER_STORE({AddReferStoreData: this.model}))

    return true;
  }

  constructor(
    private toastr: ToastrService,
    private store: Store<{
      applicationState: applicationState,
      authState: AuthStateInterface,
      store: StoreStateInterface
    }>
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      if(data.user && data.user[0]) {
        this.Userdata = data.user[0];
      }
    });

    // subscribe to store's store state
    store
      .pipe(
      takeUntil(this.ngUnsubscribe),
      select('store')
      )
      .subscribe( (data: StoreStateInterface) => {
        this.submitButtonValid = data.StoreCatState.loading;
    });

  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
