import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserInterface } from '../../../interfaces/user-interface';
import {
  GetNearByStoreReqInterface,
  GetNearByStoreItemInterface,
} from '../../../interfaces/get-near-by-store';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { GET_NEARBY_STORES } from '../../../stores//actions/store.action';

@Component({
  selector: 'app-nearby-stores',
  templateUrl: './nearby-stores.component.html',
  styleUrls: ['./nearby-stores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NearbyStoresComponent implements OnInit, OnDestroy {
  public nearByStore: Array<GetNearByStoreItemInterface>;
  public nearByStoresLoading: boolean;
  public nearByStoresLoaded: boolean;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';

  private ngUnsubscribe = new Subject();

  public Userdata: UserInterface;
  public UserAddress: UserAddressInterface;

  public clickOnNearbyStoreItem(store: GetNearByStoreItemInterface): void {
    const id = store.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  private getNearByStoresFromServer(reqData: GetNearByStoreReqInterface): void {
    this.store.dispatch(GET_NEARBY_STORES({ nearbyStoreData: reqData }));
  }

  constructor(
    private router: Router,
    private store: Store<{
      authState: AuthStateInterface;
      store: StoreStateInterface;
      address: AddressStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.Userdata = data.user[0];
      });

    // subscribe to address store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('address'))
      .subscribe((data: AddressStateInterface) => {
        if (
          data.userAddressState &&
          data.userAddressState.UserAddress &&
          data.userAddressState.UserAddress.city
        ) {
          this.UserAddress = data.userAddressState.UserAddress;

          const reqData: GetNearByStoreReqInterface = {
            userId: this.Userdata.UserID,
            latitude: this.UserAddress.latitude,
            longitude: this.UserAddress.longitude,
            pincode: this.UserAddress.postal_code,
            limit: 20,
            offset: 0,
            city: this.UserAddress.city,
          };

          this.getNearByStoresFromServer(reqData);
        }
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.nearByStoresLoading = data.getNearbyStoresState.loading;
        this.nearByStoresLoaded = data.getNearbyStoresState.loaded;
        if (
          data.getNearbyStoresState &&
          data.getNearbyStoresState.NearbystoresRes
        ) {
          this.nearByStore = data.getNearbyStoresState.NearbystoresRes.result;
        }
        this.cd.markForCheck();
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
