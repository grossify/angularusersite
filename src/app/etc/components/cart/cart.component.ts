import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import {
  CartItemArrayInterface,
  StoreDetaisInterface,
} from '../../../interfaces/get-cart-by-user-id-interface';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserInterface } from '../../../interfaces/user-interface';
import { UpdateMetaDataAndTitleService } from '../../../services/update-meta-data-and-title.service';
import { Meta } from '@angular/platform-browser';
import { AddProductInCartInputInterface } from '../../../interfaces/add-product-in-cart-interface';
import { DeleteProductInCartInputInterface } from '../../../interfaces/delete-product-from-cart-interface';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { CartStateInterface } from '../../../stores/reducers/cart.reducer';
import {
  GET_CART,
  DELETE_PRODUCT_FROM_CART,
  ADD_PRODUCT_IN_CART,
  DELETE_ALL_CART,
} from '../../../stores/actions/cart.action';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartComponent implements OnInit, OnDestroy {
  public cartItems: Array<CartItemArrayInterface>;
  public cartStoreDetail: StoreDetaisInterface;
  public Userdata: UserInterface;
  public totalAmount: number;
  public cartQtyModel: {} = {};
  private minOrderValue: number;

  private ngUnsubscribe = new Subject();

  public cartItemLoading: boolean;
  public cartItemLoaded: boolean;

  public cartBadgeItemsInCart: number;

  public disableALlButtons: boolean;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';
  public productPlaceholder = './assets/images/logo/product-placeholder.png';

  public clickOnRemove(cartItem: CartItemArrayInterface): void {
    const reqData: DeleteProductInCartInputInterface = {
      UserId: this.Userdata.UserID,
      storeProductId: cartItem.store_product_id,
      ProductUnitID: cartItem.punitid,
      storeId: cartItem.store_id,
    };

    this.store.dispatch(DELETE_PRODUCT_FROM_CART({ reqData: reqData }));
  }

  public clickOnCheckOut(): void {
    // check for order price is greater the min order
    if (this.totalAmount >= this.minOrderValue) {
      this.router.navigate(['/etc/place-order']);
    } else {
      this.toastr.error(
        `Your Total order is less than Min Order: ₹${this.minOrderValue}., please add some more product before continue.`,
        'Error',
        {
          timeOut: 5000,
          closeButton: true,
        }
      );
      return;
    }
  }

  public clickOnPlusIcon(
    cartItem: CartItemArrayInterface,
    index: number
  ): void {
    this.cartQtyModel[index] = this.cartQtyModel[index] + 1;

    const reqData: AddProductInCartInputInterface = {
      UserId: this.Userdata.UserID,
      storeProductId: cartItem.store_product_id,
      ProductUnitID: cartItem.punitid,
      qty: this.cartQtyModel[index] - cartItem.qty,
      storeId: cartItem.store_id,
    };

    this.store.dispatch(ADD_PRODUCT_IN_CART({ reqData: reqData }));
  }

  public clickOnMinusIcon(
    cartItem: CartItemArrayInterface,
    index: number
  ): void {
    this.cartQtyModel[index] = this.cartQtyModel[index] - 1;

    const reqData: AddProductInCartInputInterface = {
      UserId: this.Userdata.UserID,
      storeProductId: cartItem.store_product_id,
      ProductUnitID: cartItem.punitid,
      qty: this.cartQtyModel[index] - cartItem.qty,
      storeId: cartItem.store_id,
    };

    this.store.dispatch(ADD_PRODUCT_IN_CART({ reqData: reqData }));
  }

  public changeQuantity(cartItem: CartItemArrayInterface, index: number): void {
    let qty: number;
    if (this.cartQtyModel[index] === cartItem.qty) {
      return;
    }
    if (this.cartQtyModel[index] > cartItem.qty) {
      qty = this.cartQtyModel[index] - cartItem.qty;
    }
    if (this.cartQtyModel[index] < cartItem.qty) {
      qty = this.cartQtyModel[index] - cartItem.qty;
    }

    const reqData: AddProductInCartInputInterface = {
      UserId: this.Userdata.UserID,
      storeProductId: cartItem.store_product_id,
      ProductUnitID: cartItem.punitid,
      qty: this.cartQtyModel[index] - cartItem.qty,
      storeId: cartItem.store_id,
    };

    this.store.dispatch(ADD_PRODUCT_IN_CART({ reqData: reqData }));
  }

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private metaAndTitle: UpdateMetaDataAndTitleService,
    private meta: Meta,
    private store: Store<{
      authState: AuthStateInterface;
      cart: CartStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.meta.updateTag({ name: 'description', content: 'Shopping Cart' });
    this.meta.updateTag({ name: 'keywords', content: 'Cart, Shopping' });
    this.metaAndTitle.updateTitle('Shopping Cart');

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.Userdata = data.user[0];
      });

    // subscribe to cart store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('cart'))
      .subscribe((data: CartStateInterface) => {
        this.cd.markForCheck();
        this.cartItemLoaded = data.CartState.loaded;
        this.cartItemLoading = data.CartState.loading;
        this.disableALlButtons =
          data.CartState.loading ||
          data.addProductInCartState.loading ||
          data.ChangeroductQntInCartState.loading ||
          data.deleteProductFromCartState.loading ||
          data.DeleteAllCartState.loading; // disable if anything is loading

        if (data.CartState.loaded) {
          this.cartItems = data.CartState.Cart.result;
          this.cartStoreDetail = data.CartState.Cart.store[0];
          this.cartBadgeItemsInCart = data.CartState.Cart.num_of_items;
          this.totalAmount = data.CartState.Cart.total_price;
          this.minOrderValue = data.CartState.Cart.store[0].min_amt;

          data.CartState.Cart.result.forEach((value, index) => {
            // set default qty for each cart item
            this.cartQtyModel[index] = value.qty;
          });
        }
        // else {
        //   this.cartItems = undefined;
        //   this.cartStoreDetail = undefined;
        //   this.cartBadgeItemsInCart = undefined;
        //   this.totalAmount = undefined;
        //   this.minOrderValue = undefined;
        //   this.cartQtyModel = {};
        // }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
