import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { GetOffersInterface } from '../../../interfaces/get-offers-interface';
import { takeUntil } from 'rxjs/operators';
import { UserInterface } from '../../../interfaces/user-interface';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_OFFERS } from '../../../stores/actions/application.action';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit, OnDestroy {
  public offers: GetOffersInterface;
  private ngUnsubscribe = new Subject();
  public offersLoading: boolean;
  public offersLodded: boolean;
  public Userdata: UserInterface;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';

  constructor(
    private store: Store<{
      applicationState: applicationState,
      authState: AuthStateInterface
    }>
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to applicationState store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('applicationState')
      )
      .subscribe( (data: applicationState) => {
        if (data.getOffersState && data.getOffersState.loaded) {
          this.offers = data.getOffersState.offer;
          this.offersLoading = data.getOffersState.loading;
          this.offersLodded = data.getOffersState.loaded;
        }
    });
  }

  ngOnInit() {
    this.store.dispatch(GET_OFFERS());
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
