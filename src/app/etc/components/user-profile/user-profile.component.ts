import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserInterface } from '../../../interfaces/user-interface';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ProfilePicUploderDialogComponent } from '../../../dialogs/profile-pic-uploder.dialog';
import { UpdateUserDataDialogComponent } from '../../../dialogs/update-profile-data.dialog';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { UserState } from '../../../stores/reducers/user.reducer';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserProfileComponent implements OnInit, OnDestroy {
  public UserData: UserInterface;
  public UserDataLoding: boolean;
  public UserDataLoaded: boolean;
  private ngUnsubscribe = new Subject();

  public profilePicSrc = './assets/images/profile-picture-female.png';

  openProfileImageUploadDialog(): void {
    const dialogRef = this.dialog.open(ProfilePicUploderDialogComponent, {
      width: '85%',
      height: '85%',
      data: { UserData: this.UserData }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.base64Image) {
        this.profilePicSrc = result.base64Image;
      }
      if (result.success) {

        // this.getUserDataFromServer();


        // // change display data
        // this.UserData = result.newUserData;
        // // change saved user data in local storage
        // this.saveUserData(result.newUserData);
      }
    });
  }

  openProfileDataUploadDialog(userData: UserInterface): void {
    const userDialogRef = this.dialog.open(UpdateUserDataDialogComponent, {
      width: '800px',
      height: '85%',
      data: userData
    });

    userDialogRef.afterClosed().subscribe(result => {
      if (result.success) {
        // change display data
        // this.UserData = result.newUserData;
        // change saved user data in local storage
      }
    });
  }

  // private getUserDataFromServer(UserName: string): void {
  //   this.store.dispatch(GET_END_USER_PROFILE({userName: UserName}));
  // }

  constructor(
    public dialog: MatDialog,
    private store: Store<{
      authState: AuthStateInterface,
      user: UserState
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.UserDataLoding = data.loading;
      this.UserDataLoaded = data.loaded;
      if (data.loaded) {
        this.cd.markForCheck();
      }
      if(data.user && data.user[0]) {
        this.UserData = data.user[0];
        if (data.user[0].userProfile) {
          this.profilePicSrc = data.user[0].userProfile;
        }
      }
    });

    // subscribe to user store state
    // store
    // .pipe(
    //   takeUntil(this.ngUnsubscribe),
    //   select('user')
    // )
    // .subscribe( (data: UserState) => {
    //   this.UserDataLoding = data.;
    //   this.UserDataLoaded = data;
    // });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
