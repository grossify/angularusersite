import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserInterface } from '../../../interfaces/user-interface';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';
import { Router } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { TOGGLE_SETTING_SIDENAV_BAR } from '../../../stores/actions/application.action';

@Component({
  selector: 'app-setting-address',
  templateUrl: './setting-address.component.html',
  styleUrls: ['./setting-address.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingAddressComponent implements OnInit, OnDestroy {
  public UserAddress: UserAddressInterface;
  public Userdata: UserInterface;
  private ngUnsubscribe = new Subject();

  public isUserAddressAvailable: boolean;

  public settingNavBarState: boolean;

  public toggelState() {
    this.store.dispatch(TOGGLE_SETTING_SIDENAV_BAR());
  }

  public clickOnChangeAddress(): void {
    this.router.navigate(['/etc/settings/address/edit-address']);
  }

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
      address: AddressStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        this.settingNavBarState = data.settingSideNavBarState;
      });

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to address store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('address'))
      .subscribe((data: AddressStateInterface) => {
        if (
          data.userAddressState &&
          data.userAddressState.UserAddress &&
          data.userAddressState.UserAddress.city
        ) {
          this.UserAddress = data.userAddressState.UserAddress;
          this.isUserAddressAvailable = data.userAddressState.loaded;
          this.cd.markForCheck();
        }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
