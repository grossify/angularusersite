import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserInterface } from '../../../interfaces/user-interface';
import { User } from '../../../classes/user';
import { UpdatePasswordDialogComponent } from '../../../dialogs/update-password.dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { applicationState } from '../../../stores/reducers/application.reducer';
import { Store, select } from '@ngrx/store';
import { TOGGLE_SETTING_SIDENAV_BAR } from '../../../stores/actions/application.action';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';

@Component({
  selector: 'app-setting-security',
  templateUrl: './setting-security.component.html',
  styleUrls: ['./setting-security.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingSecurityComponent implements OnDestroy {
  public Userdata: UserInterface;

  public model = new User();

  private ngUnsubscribe = new Subject();

  public settingNavBarState: boolean;

  public toggelState() {
    this.store.dispatch(TOGGLE_SETTING_SIDENAV_BAR());
  }

  public clickOnChangePassword(): void {
    const dialogRef = this.dialog.open(UpdatePasswordDialogComponent, {
      width: '380px',
      height: '400px',
      data: { Userdata: this.Userdata },
    });

    dialogRef.afterClosed().subscribe((result: { success: Boolean }) => {
      if (result.success) {
        this.getUserDataFromServer(this.Userdata.UserName);
      }
    });
  }

  private getUserDataFromServer(UserName: string): void {
    // this.store.dispatch(GET_END_USER_PROFILE({userName: UserName}));
  }

  constructor(
    public dialog: MatDialog,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        this.settingNavBarState = data.settingSideNavBarState;
      });

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.cd.markForCheck();
          this.Userdata = data.user[0];
          if (data.user && data.user[0].UserName) {
            this.getUserDataFromServer(data.user[0].UserName);
          }
        }
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
