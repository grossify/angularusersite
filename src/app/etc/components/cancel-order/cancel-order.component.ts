import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { GetOrdersInputInterface, OrderItemInterface } from '../../../interfaces/get-orders-interface';
import { UserInterface } from '../../../interfaces/user-interface';
import { OrderStatus } from '../../../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { OrderState } from '../../../stores/reducers/order.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_ALL_ORDERS } from '../../../stores/actions/order.action';

@Component({
  selector: 'app-cancel-order',
  templateUrl: './cancel-order.component.html',
  styleUrls: ['./cancel-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CancelOrderComponent implements OnInit, OnDestroy {

  private ngUnsubscribe = new Subject();
  public Userdata: UserInterface;
  public allOrder: OrderItemInterface;

  private orderId: number;

  private getRouteParameter(): void {
    // const orderId = this.route.snapshot.paramMap.get('orderId');
    // this.orderId = parseInt(orderId, 10);
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.orderId = parseInt(params.get('orderId'), 10);
    });
  }

  private getAllOrdersFromServer(): void {
    const reqData: GetOrdersInputInterface = {
      userId: this.Userdata.UserID,
      limit: 0,
      offset: 20
    };

    this.store.dispatch(GET_ALL_ORDERS({reqData: reqData}));
  }

  constructor(
    private route: ActivatedRoute,
    private store: Store<{
      order: OrderState,
      authState: AuthStateInterface
    }>,
    private cd: ChangeDetectorRef
    ) {
      this.getRouteParameter();

      // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to applicationState store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('order')
      )
      .subscribe( (data: OrderState) => {
        if (data && data.getAllOrderState) {
          if (data.getAllOrderState.ordersRes) {
            this.allOrder = data.getAllOrderState.orders[this.orderId];
            this.cd.markForCheck();
          }
        }
    });
  }

  ngOnInit() {
    this.getAllOrdersFromServer()
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
