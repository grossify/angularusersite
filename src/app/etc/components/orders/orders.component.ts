import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  GetOrdersInputInterface,
  OrderItemInterface,
} from '../../../interfaces/get-orders-interface';
import { UserInterface } from '../../../interfaces/user-interface';
import { OrderStatus } from '../../../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { OrderState } from '../../../stores/reducers/order.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_ALL_ORDERS } from '../../../stores/actions/order.action';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersComponent implements OnInit, OnDestroy {
  public Userdata: UserInterface;
  public allOrders: Array<OrderItemInterface>;
  public orderStatus = OrderStatus;

  private ngUnsubscribe = new Subject();

  public orderItemLoding: boolean;
  public orderItemLoaded: boolean;

  private getAllOrdersFromServer(): void {
    const reqData: GetOrdersInputInterface = {
      userId: this.Userdata.UserID,
      limit: 0,
      offset: 20,
    };

    this.store.dispatch(GET_ALL_ORDERS({ reqData: reqData }));
  }

  public clickOnOrderDetailsButton(order: OrderItemInterface): void {
    const id = order.OrderID;
    this.router.navigate(['/etc/order-details', id]);
  }

  public clickOnTrackOrderButton(order: OrderItemInterface): void {
    const id = order.OrderID;
    this.router.navigate(['/etc/track-order', id]);
  }

  public clickOnCancelOrderButton(order: OrderItemInterface): void {
    const id = order.OrderID;
    this.router.navigate(['/etc/cancel-order', id]);
  }

  public clickOnContactToSellerButton(order: OrderItemInterface): void {
    const id = order.store_id;
    this.router.navigate(['/etc/store-detail', id]);
  }

  constructor(
    private router: Router,
    private store: Store<{
      order: OrderState;
      authState: AuthStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.Userdata = data.user[0];
      });

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('order'))
      .subscribe((data: OrderState) => {
        if (data && data.getAllOrderState) {
          this.orderItemLoding = data.getAllOrderState.loading;
          this.orderItemLoaded = data.getAllOrderState.loaded;
          this.cd.markForCheck();
          if (data.getAllOrderState.ordersRes) {
            this.allOrders = data.getAllOrderState.ordersRes.result;
          }
        }
      });
  }

  ngOnInit() {
    this.getAllOrdersFromServer();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
