import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors
} from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { LOGIN } from '../../../stores/actions/auth.actions';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {

  private ngUnsubscribe = new Subject();
  public submitButtonValid: boolean;
  public showPassword = false;

  public loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(128),
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(128),
      Validators.pattern('^[^ ]+$')
    ])
  });

  public UserNameMatcher = new MyErrorStateMatcher();
  public UserPasswordMatcher = new MyErrorStateMatcher();

  constructor(
    private toastr: ToastrService,
    private store: Store<{authState: AuthStateInterface}>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.submitButtonValid = data.loading;
      this.cd.markForCheck();
    });
  }

  private showErrors(element: string, keyError: any): void {
    let text: string;
    switch (keyError) {
      case 'required':
        text = `${element} is required!`;
        break;
      case 'pattern':
        text = `${element} has spaces, No spacea are allowed!`;
        break;
      case 'minlength':
        text = `${element} is too short!`;
        break;
      case 'maxlength':
        text = `${element} is very long!`;
        break;
      case 'email':
        text = `${element} is not valid email Id!`;
        break;
    }
    this.toastr.error(text, `Error in ${element}!`, {
      timeOut: 5000,
      closeButton: true
    });
  }

  private getFormValidationErrors() {
    Object.keys(this.loginForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.loginForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          // console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          switch (key) {
            case 'username':
              this.showErrors('User Name', keyError);
              break;
            case 'password':
              this.showErrors('Password', keyError);
              break;
          }
        });
      }
    });
  }

  public onSubmitUserDetails(value): boolean {
    if (!this.loginForm.valid) {
      this.getFormValidationErrors();
      return false;
    }
    this.toastr.info('Login is in progress please wait', 'Info', {
      timeOut: 3000,
      closeButton: true
    });

    this.store.dispatch(LOGIN({LoginInputData: value}));
  }
}
