import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import { GetProductDetailsInterface } from '../../../interfaces/get-product-details-interface';
import { takeUntil } from 'rxjs/operators';
import { UserInterface } from '../../../interfaces/user-interface';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_STORE_PRODUCT_DETAIL } from '../../../stores/actions/store.action';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';

@Component({
  selector: 'app-store-product',
  templateUrl: './store-product.component.html',
  styleUrls: ['./store-product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoreProductComponent implements OnInit, OnDestroy {
  public storeProduct: GetProductDetailsInterface;
  public storeProductId: number;
  public selectedImageSrc: string;
  public selectedImageIndex: number;

  public Userdata: UserInterface;
  private ngUnsubscribe = new Subject();
  public noStoreProduct: boolean;
  public storeProductLoading: boolean;
  public storeProductLoaded: boolean;
  public productPlaceholder = './assets/images/logo/product-placeholder.png';

  private getStoreProductFromServer(): void {
    this.store.dispatch(
      GET_STORE_PRODUCT_DETAIL({ storeProductId: this.storeProductId })
    );
  }

  public clickOnProductImage(selectedimages: string): void {
    this.selectedImageSrc = selectedimages;
    this.cd.markForCheck();
  }

  private getRouteParameter(): void {
    this.route.paramMap
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((params: ParamMap) => {
        this.storeProductId = parseInt(params.get('id'), 10);
      });
  }

  constructor(
    private route: ActivatedRoute,
    private store: Store<{
      authState: AuthStateInterface;
      store: StoreStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getRouteParameter();

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.Userdata = data.user[0];
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.storeProductLoading = data.getProductDetailsState.loading;
        this.storeProductLoaded = data.getProductDetailsState.loaded;

        if (data.getProductDetailsState && data.getProductDetailsState.loaded) {
          this.storeProduct = data.getProductDetailsState.productDetail;
          if (!data.getProductDetailsState.productDetail.images[0]) {
            // if not images array
            this.selectedImageSrc =
              data.getProductDetailsState.productDetail.prod_images;
          } else {
            this.selectedImageSrc =
              data.getProductDetailsState.productDetail.images[0];
          }
          this.cd.markForCheck();
        }
      });
  }

  ngOnInit() {
    this.getStoreProductFromServer();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
