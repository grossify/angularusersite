import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { REGISTRATION } from '../../../stores/actions/auth.actions';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { SignUpInterface } from '../../../interfaces/sign-up-interface';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public Userdata: SignUpInterface;
  public submitButtonValid: boolean;

  public registrationForm = new FormGroup({
    first_name: new FormControl('', [
      Validators.required,
      Validators.maxLength(128),
    ]),
    last_name: new FormControl('', [
      Validators.required,
      Validators.maxLength(128),
    ]),
    UserName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(128),
      Validators.pattern('^[^ ]+$'),
    ]),
    UserEmail: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.minLength(4),
      Validators.maxLength(128),
    ]),
    UserPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(128),
      Validators.pattern('^[^ ]+$'),
    ]),
    confirm_UserPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(128),
      Validators.pattern('^[^ ]+$'),
    ]),
    UserPhone: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(15),
      Validators.min(5000000000),
      Validators.max(999999999999999),
      Validators.pattern('^[^ ]+$'),
    ]),
  });

  public firstNameMatcher = new MyErrorStateMatcher();
  public lastNameMatcher = new MyErrorStateMatcher();
  // public UserNameMatcher = new MyErrorStateMatcher();
  public UserEmailMatcher = new MyErrorStateMatcher();
  public UserPasswordMatcher = new MyErrorStateMatcher();
  public confirmUserPasswordMatcher = new MyErrorStateMatcher();
  public UserPhoneMatcher = new MyErrorStateMatcher();

  private showErrors(element: string, keyError: any): void {
    let text: string;
    switch (keyError) {
      case 'required':
        text = `${element} is required!`;
        break;
      case 'pattern':
        text = `${element} has spaces, No spaces are allowed!`;
        break;
      case 'email':
        text = `${element} has wrong email format!`;
        break;
      case 'minlength':
        text = `${element} is too short!`;
        break;
      case 'maxlength':
        text = `${element} is very long!`;
        break;
      case 'min':
        text = `${element} is too short, should be of 10 digits!`;
        break;
      case 'max':
        text = `${element} is very long!`;
        break;
    }
    this.toastr.error(text, `Error in ${element}!`, {
      timeOut: 5000,
      closeButton: true,
    });
  }

  private getFormValidationErrors() {
    Object.keys(this.registrationForm.controls).forEach((key) => {
      const controlErrors: ValidationErrors = this.registrationForm.get(key)
        .errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          // console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          switch (key) {
            case 'first_name':
              this.showErrors('First Name', keyError);
              break;
            case 'last_name':
              this.showErrors('Last Name', keyError);
              break;
            // case 'UserName':
            //   this.showErrors('User Name', keyError);
            //   break;
            case 'UserEmail':
              this.showErrors('Email Id', keyError);
              break;
            case 'UserPassword':
              this.showErrors('Password', keyError);
              break;
            case 'confirm_UserPassword':
              this.showErrors('Confirm Password', keyError);
              break;
            case 'UserPhone':
              this.showErrors('Mobile Number', keyError);
              break;
          }
        });
      }
    });
  }

  private randomString(): string {
    // random string genrator
    const length = 10;
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  public onSubmitUserDetails(value: any): boolean {
    // randome username
    let newUserName = this.randomString();
    this.registrationForm.patchValue({ UserName: newUserName });

    if (!this.registrationForm.valid) {
      this.getFormValidationErrors();
      return false;
    }
    // test other things
    if (
      this.registrationForm.get('UserPassword').value !==
      this.registrationForm.get('confirm_UserPassword').value
    ) {
      this.toastr.error(
        'Password and Confirm Password must be same!',
        'Error in Confirm Password',
        {
          timeOut: 5000,
          closeButton: true,
        }
      );
      return false;
    }

    this.toastr.info('Login is in progress please wait', 'Info', {
      timeOut: 3000,
      closeButton: true,
    });

    // remove  from value object (because it is causing problem in registration)
    // delete value.checkboxTermsAndPolicyFormControl;

    // dispach action
    this.store.dispatch(
      REGISTRATION({ registrationIputData: this.registrationForm.value })
    );
  }

  constructor(
    private toastr: ToastrService,
    private store: Store<{ authState: AuthStateInterface }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.submitButtonValid = data.loading;
        this.cd.markForCheck();
      });
  }

  ngOnInit() {}
}
