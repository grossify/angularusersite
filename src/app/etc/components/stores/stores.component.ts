import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { AddProductInCartInputInterface } from '../../../interfaces/add-product-in-cart-interface';
import {
  GetCartByUserIdInterface,
  CartItemArrayInterface,
} from '../../../interfaces/get-cart-by-user-id-interface';
import {
  GetStoreProductsItemInterface,
  PricesInterface,
  StoreProductsReqInterface,
  GetStoreProductsInterface,
} from '../../../interfaces/get-store-products-interface';
import { ProductCategoriesOfStoresInterface } from '../../../interfaces/product-categories-of-stores-interface';
import {
  StoreDetailInterface,
  StoreDetailReqInterface,
} from '../../../interfaces/store-detail-interface';
import { GetNearByFavouriteStoreItemInterface } from '../../../interfaces/fevourite-stores-interface';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserInterface } from '../../../interfaces/user-interface';
import { AddFavouriteStoreReqInterface } from '../../../interfaces/add-favourite-store-interface';
import { RemoveFavouriteStoreReqInterface } from '../../../interfaces/fevourite-stores-interface';
import { SameStoreItemsDailogComponent } from '../../../dialogs/app-same-store-items.dialog';
import { MatDialog } from '@angular/material/dialog';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { CartStateInterface } from '../../../stores/reducers/cart.reducer';
import { ADD_PRODUCT_IN_CART } from '../../../stores/actions/cart.action';
import {
  GET_STORE_DETAIL,
  GET_PRODUCT_CATEGORIES_OF_STORES,
  GET_STORES_PRODUCTS,
  ADD_FAVOURITE_STORE,
  REMOVE_FAVOURITE_STORE,
  SET_STORES_PRODUCTS_LIMIT_OFFSET,
} from '../../../stores/actions/store.action';
import {
  OPEN_MAIN_SIDENAV_BAR,
  CLOSE_MAIN_SIDENAV_BAR,
} from '../../../stores/actions/application.action';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoresComponent implements OnInit, OnDestroy {
  private storeId: number;

  public storeProducts: GetStoreProductsInterface;
  public storeDetail: StoreDetailInterface;
  public productCategoriesOfStores: ProductCategoriesOfStoresInterface[];
  public selectedPoductCategory: ProductCategoriesOfStoresInterface;
  public selectedQuantity: {} = {};
  public selectedIndex: number;
  private ngUnsubscribe = new Subject();

  public Userdata: UserInterface;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';
  public productPlaceholder = './assets/images/logo/product-placeholder.png';

  public storeDetailLoading: boolean;
  public storeDetailLoaded: boolean;
  public storeProductsLoading: boolean;
  public storeProductsLoaded: boolean;
  public productCategoriesLoading: boolean;
  public productCategoriesLoaded: boolean;

  public addOrRemoveFavStoreIcon: boolean; // true for add fav store, false for remove fav store
  private nearByFavStore: {
    [storeId: number]: GetNearByFavouriteStoreItemInterface;
  };

  private limit: number = 20;
  private offset: number = 0;

  private cart: GetCartByUserIdInterface | null;

  private getRouteParameter(): void {
    this.route.paramMap
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((params: ParamMap) => {
        this.storeId = parseInt(params.get('id'), 10);
      });
  }

  private getStoreProductsFromServer(
    StoreProductsReq: StoreProductsReqInterface
  ): void {
    this.store.dispatch(
      GET_STORES_PRODUCTS({ storeProductReq: StoreProductsReq })
    );
  }

  private getStoreDetailFromServer(): void {
    const reqData: StoreDetailReqInterface = {
      UserId: this.Userdata.UserID,
      StoreId: this.storeId,
    };
    this.store.dispatch(GET_STORE_DETAIL({ getStoreDetail: reqData }));
  }

  public clickOnViewStoreDetailsButton(
    storeDetail: StoreDetailInterface
  ): void {
    const id = storeDetail.store_id;
    this.router.navigate(['/etc/store-detail', id]);
  }

  public clickOnStoreProduct(
    storeProduct: GetStoreProductsItemInterface
  ): void {
    const id = storeProduct.store_product_id;
    this.router.navigate(['/etc/store-product', id]);
  }

  public clickOnQuantityMenuItem(
    i: number,
    j: number,
    price: PricesInterface,
    product: GetStoreProductsItemInterface
  ): void {
    this.selectedQuantity[i] = price;
  }

  private clickOnAddToFavoriteStore(
    reqData: AddFavouriteStoreReqInterface
  ): void {
    this.store.dispatch(ADD_FAVOURITE_STORE({ addFavouriteReq: reqData }));
  }

  private clickOnRemoveFavoriteStore(
    reqData: AddFavouriteStoreReqInterface
  ): void {
    this.store.dispatch(
      REMOVE_FAVOURITE_STORE({ removeFavouriteReq: reqData })
    );
    this.router.navigate(['/']);
  }

  public clickOnAddOrDeleteToFavoriteStore(
    storeDetail: StoreDetailInterface
  ): void {
    // check store is in favourate store list

    const reqData: AddFavouriteStoreReqInterface = {
      end_user_id: this.Userdata.UserID,
      store_id: storeDetail.store_id,
    };
    if (this.nearByFavStore[storeDetail.store_id]) {
      this.clickOnRemoveFavoriteStore(reqData); // delete store it if not in fav store list
    } else {
      this.clickOnAddToFavoriteStore(reqData); // add store if it not in fav store list
    }
  }

  public clickOnAddToCart(
    defaultQuantity: PricesInterface,
    selectedQuantity: PricesInterface,
    product: GetStoreProductsItemInterface
  ): void {
    // add product in cart if valid (from same store as cart)
    const storeProductId: number = product.store_product_id;
    let PunitId: number;
    if (selectedQuantity) {
      PunitId = selectedQuantity.punitid;
    } else {
      PunitId = defaultQuantity.punitid;
    }
    const qty = 1;

    const reqData: AddProductInCartInputInterface = {
      UserId: this.Userdata.UserID,
      storeProductId: storeProductId,
      ProductUnitID: PunitId,
      qty: qty,
      storeId: this.storeId,
    };

    // check for item, is from same store as in cart product?
    // if cart != undefined (this.cart exist) check for same storeId, if same add product if not show prompt
    // if cart == undefined cart does not exist add any store product
    if (
      (this.cart &&
        this.cart.store &&
        this.storeId === this.cart.store[0].store_id) ||
      this.cart == undefined
    ) {
      // check for already exist product

      if (this.cart != undefined && this.cart.result) {
        const checkForSameProduct = (currentValue: CartItemArrayInterface) => {
          if (
            reqData.ProductUnitID == currentValue.punitid &&
            reqData.storeId == currentValue.store_id &&
            reqData.storeProductId == currentValue.store_product_id
          ) {
            return true;
          }
        };

        const check = this.cart.result.every(checkForSameProduct);
        if (check) {
          this.toastr.error(
            'Product varient is already present in your cart.',
            'Error',
            {
              timeOut: 5000,
              closeButton: true,
            }
          );
        } else {
          this.store.dispatch(ADD_PRODUCT_IN_CART({ reqData: reqData }));
        }
      } else {
        this.store.dispatch(ADD_PRODUCT_IN_CART({ reqData: reqData }));
      }
    } else {
      // show error and prompt user for action.
      // show dialog

      const dialogRef = this.dialog.open(SameStoreItemsDailogComponent, {
        width: '480px',
        data: { product: product, cart: this.cart, reqData: reqData },
      });

      dialogRef.afterClosed().subscribe((result) => {});
    }
  }

  private getProductCategoriesOfStoresFromServer(): void {
    this.store.dispatch(
      GET_PRODUCT_CATEGORIES_OF_STORES({ storeId: this.storeId })
    );
  }

  public clickOnPoductCategory(
    productCategory: ProductCategoriesOfStoresInterface,
    i: number
  ): void {
    this.selectedIndex = i;
    this.selectedPoductCategory = productCategory;

    const reqData: StoreProductsReqInterface = {
      CatId: productCategory.cat_id,
      EndUserId: this.Userdata.UserID,
      StoreId: productCategory.store_id,
      limit: this.limit,
      offset: this.offset,
    };
    this.getStoreProductsFromServer(reqData);
  }

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private store: Store<{
      authState: AuthStateInterface;
      cart: CartStateInterface;
      store: StoreStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getRouteParameter();

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to cart store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('cart'))
      .subscribe((data: CartStateInterface) => {
        if (data.CartState.loaded) {
          this.cart = data.CartState.Cart;
        }
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.productCategoriesLoading = data.getProductCategoriesState.loading;
        this.productCategoriesLoaded = data.getProductCategoriesState.loaded;
        this.productCategoriesOfStores =
          data.getProductCategoriesState.productCategories;

        this.storeProductsLoading = data.getStoreProductState.loading;
        this.storeProductsLoaded = data.getStoreProductState.loaded;
        this.storeProducts = data.getStoreProductState.storeProduct;

        this.storeDetailLoading = data.getStoreDetalsState.loading;
        this.storeDetailLoaded = data.getStoreDetalsState.loaded;
        this.storeDetail = data.getStoreDetalsState.storeDetails;

        if (
          data.getNearbyFavouriteStoresState &&
          data.getNearbyFavouriteStoresState.loaded
        ) {
          this.nearByFavStore =
            data.getNearbyFavouriteStoresState.NearbyFavouriteStores;
          if (
            data.getStoreDetalsState &&
            data.getStoreDetalsState.loaded &&
            data.getStoreDetalsState.storeDetails.store_id
          ) {
            this.addOrRemoveFavStoreIcon = !!data.getNearbyFavouriteStoresState
              .NearbyFavouriteStores[
              data.getStoreDetalsState.storeDetails.store_id
            ];
          }
        }

        // run this function one time only
        if (
          data.getProductCategoriesState &&
          data.getProductCategoriesState.loaded &&
          !data.getStoreProductState.loaded &&
          !data.getStoreProductState.loading
        ) {
          this.selectedPoductCategory =
            data.getProductCategoriesState.productCategories[0];
          this.selectedIndex = 0;

          // const reqData: StoreProductsReqInterface = {
          //   CatId: data.getProductCategoriesState.productCategories[0].cat_id,
          //   EndUserId: this.Userdata.UserID,
          //   StoreId: data.getProductCategoriesState.productCategories[0].store_id,
          //   limit: this.limit,
          //   offset: this.offset
          // }
          // this.getStoreProductsFromServer(reqData);
        }

        if (
          data.getProductCategoriesState &&
          data.getProductCategoriesState.error
        ) {
          this.productCategoriesLoading = false;
          this.productCategoriesLoaded = false;
          this.productCategoriesOfStores = null;

          this.storeProductsLoading = false;
          this.storeProductsLoaded = false;
          this.storeProducts = null;

          // this.toastr.error('Product Categories not available for this product.', 'Error', {
          //   timeOut: 5000,
          //   closeButton: true
          // });
        }

        if (data.getStoreProductState && data.getStoreProductState.error) {
          this.storeProductsLoading = false;
          this.storeProductsLoaded = false;
          this.storeProducts = null;

          // this.toastr.error('Products not available for this category.', 'Error', {
          //   timeOut: 5000,
          //   closeButton: true
          // });
        }

        this.cd.markForCheck();
      });
  }

  ngOnInit() {
    const limitAndOffset: { limit: number; offset: number; userId: number } = {
      limit: this.limit,
      offset: this.offset,
      userId: this.Userdata.UserID,
    };
    this.store.dispatch(
      SET_STORES_PRODUCTS_LIMIT_OFFSET({ limitAndOffset: limitAndOffset })
    );

    this.getStoreDetailFromServer();
    this.getProductCategoriesOfStoresFromServer();
    this.store.dispatch(CLOSE_MAIN_SIDENAV_BAR());
  }

  ngOnDestroy() {
    this.store.dispatch(OPEN_MAIN_SIDENAV_BAR());
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
