
import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserInterface } from '../../../interfaces/user-interface';
import {
    CityStoresInterface,
    GetCityStoresItemInterface,
    CityStoresReqInterface
} from '../../../interfaces/city-stores-interface';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { GET_STORE_BY_CITY } from '../../../stores//actions/store.action';


@Component({
  selector: 'app-city-stores',
  templateUrl: './city-stores.component.html',
  styleUrls: ['./city-stores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityStoresComponent implements OnInit, OnDestroy {
  public storesByCity: CityStoresInterface;


  public storesByCityLoading: boolean;
  public storesByCityLoaded: boolean;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';

  private ngUnsubscribe = new Subject();

  public UserAddress: UserAddressInterface;
  public Userdata: UserInterface;

  private getStoreByCity(reqDataCity: CityStoresReqInterface){
    this.store.dispatch(GET_STORE_BY_CITY({reqData: reqDataCity}));
  }

  public clickOnCityStoresItem(cityStore: GetCityStoresItemInterface): void {
    const id = cityStore.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  constructor(
    private router: Router,
    private store: Store<{
      authState: AuthStateInterface,
      store: StoreStateInterface,
      address: AddressStateInterface
    }>,
    private cd: ChangeDetectorRef
    ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to address store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('address')
      )
      .subscribe( (data: AddressStateInterface) => {
        if (
          data.userAddressState &&
           data.userAddressState.loaded &&
           data.userAddressState.UserAddress &&
           data.userAddressState.UserAddress.city
        ) {

          this.UserAddress = data.userAddressState.UserAddress;
          const reqDataCity: CityStoresReqInterface = {
            userId: this.Userdata.UserID,
            city: this.UserAddress.city
          }
          this.getStoreByCity(reqDataCity);
        }
    });

    // subscribe to store's store state
    store
      .pipe(
      takeUntil(this.ngUnsubscribe),
      select('store')
      )
      .subscribe( (data: StoreStateInterface) => {
        this.storesByCityLoading = data.getCityStoresState.loading;
        this.storesByCityLoaded = data.getCityStoresState.loaded;
        this.storesByCity = data.getCityStoresState.StoresRes;
        this.cd.markForCheck();
    });
  }


  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
