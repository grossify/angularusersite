import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityStoresComponent } from './city-stores.component';

describe('CityStoresComponent', () => {
  let component: CityStoresComponent;
  let fixture: ComponentFixture<CityStoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityStoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
