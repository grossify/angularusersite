import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserInterface } from '../../../interfaces/user-interface';
import {
  GetNearByFevouriteStoresReqInterface,
  FevouriteStoresInterface,
  GetNearByFavouriteStoreItemInterface
} from '../../../interfaces/fevourite-stores-interface';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { GET_NEARBY_FAVOURITE_STORES } from '../../../stores//actions/store.action';


@Component({
  selector: 'app-favourite-stores',
  templateUrl: './favourite-stores.component.html',
  styleUrls: ['./favourite-stores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavouriteStoresComponent implements OnInit, OnDestroy {
  public fevouriteStores: Array<GetNearByFavouriteStoreItemInterface>;

  public fevouriteStoresLoading: boolean;
  public fevouriteStoresLoaded: boolean;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';

  private ngUnsubscribe = new Subject();

  public UserAddress: UserAddressInterface;
  public Userdata: UserInterface;

  private getNearByFavoriteStoresFromServer(reqData: GetNearByFevouriteStoresReqInterface): void {
    this.store.dispatch(GET_NEARBY_FAVOURITE_STORES({nearbyFavouriteStoreData: reqData}));
  }

  public clickOnFavouriteStoresItem(store: GetNearByFavouriteStoreItemInterface): void {
    const id = store.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  constructor(
    private router: Router,
    private store: Store<{
      authState: AuthStateInterface,
      store: StoreStateInterface,
      address: AddressStateInterface
    }>,
    private cd: ChangeDetectorRef
    ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to address store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('address')
      )
      .subscribe( (data: AddressStateInterface) => {
        if (
          data.userAddressState &&
           data.userAddressState.loaded &&
           data.userAddressState.UserAddress &&
           data.userAddressState.UserAddress.city
        ) {

          this.UserAddress = data.userAddressState.UserAddress;
          const reqDataFav: GetNearByFevouriteStoresReqInterface = {
            userId: this.Userdata.UserID,
            latitude: this.UserAddress.latitude,
            longitude: this.UserAddress.longitude,
            pincode: this.UserAddress.postal_code,
            limit: 20,
            offset: 0,
            city: this.UserAddress.city
          };

          this.getNearByFavoriteStoresFromServer(reqDataFav);
        }
    });

    // subscribe to store's store state
    store
      .pipe(
      takeUntil(this.ngUnsubscribe),
      select('store')
      )
      .subscribe( (data: StoreStateInterface) => {
        this.fevouriteStoresLoading = data.getNearbyFavouriteStoresState.loading;
        this.fevouriteStoresLoaded = data.getNearbyFavouriteStoresState.loaded;
        if (data.getNearbyFavouriteStoresState && data.getNearbyFavouriteStoresState.NearbyFavouriteStoresRes) {
          this.fevouriteStores = data.getNearbyFavouriteStoresState.NearbyFavouriteStoresRes.result;
        }
        this.cd.markForCheck();
    });
  }


  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
