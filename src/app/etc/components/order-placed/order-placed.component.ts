import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { OrderState } from '../../../stores/reducers/order.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { GET_CART } from '../../../stores/actions/cart.action';

@Component({
  selector: 'app-order-placed',
  templateUrl: './order-placed.component.html',
  styleUrls: ['./order-placed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrderPlacedComponent implements OnInit, OnDestroy {

  private ngUnsubscribe = new Subject();

  public OrderID: number;
  public OrderNo: number;
  public storeName: string;



  constructor(
    private store: Store<{
      authState: AuthStateInterface,
      order: OrderState
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to applicationState store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('order')
      )
      .subscribe( (data: OrderState) => {
        if (data && data.postOrderState.loaded) {
          this.OrderID = data.postOrderState.postOrder[0].OrderID;
          this.OrderNo = data.postOrderState.postOrder[0].OrderNo;
          this.storeName = data.postOrderState.postOrder[0].store_name;
          this.cd.markForCheck();
        }
    });

    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      if (data.user && data.user[0]) {  // for clearing cart badge number
        this.store.dispatch(GET_CART({userId: data.user[0].UserID}));
        this.cd.markForCheck();
      }

    });

  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
