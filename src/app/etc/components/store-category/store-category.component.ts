import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {
  GetStoreByCategoriesInterface,
  GetStoreByCategoriesItemInterface
} from '../../../interfaces/get-store-by-categories-interface';
import { UserInterface } from '../../../interfaces/user-interface';
import { takeUntil } from 'rxjs/operators';
import { StoreByCategoriesReqInterface } from '../../../interfaces/get-store-by-categories-interface';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';
import { GET_STORE_BY_CATEGORIES } from '../../../stores/actions/store.action';


@Component({
  selector: 'app-store-category',
  templateUrl: './store-category.component.html',
  styleUrls: ['./store-category.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoreCategoryComponent implements OnInit, OnDestroy {
  public stores: GetStoreByCategoriesInterface;
  public catId: number;
  private Userdata: UserInterface;
  private ngUnsubscribe = new Subject();
  public storeNearbyInSelectedCategoryLoaded: boolean;
  public storeNearbyInSelectedCategoryLoading: boolean;
  private UserAddress: UserAddressInterface;

  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';

  private getStoreByCategoryFormServer(): void {
    const storeByCategoriesData: StoreByCategoriesReqInterface = {
      userId: this.Userdata.UserID,
      catId: this.catId,
      latitude: this.UserAddress.latitude,
      longitude: this.UserAddress.longitude,
      city: this.UserAddress.city
    }

    this.store.dispatch(GET_STORE_BY_CATEGORIES({getStoreByCatData: storeByCategoriesData}));
  }

  public clickOnStore(store: GetStoreByCategoriesItemInterface): void {
    const id = store.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  private getRouteParameter(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.catId = parseInt(params.get('id'), 10);
    });
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<{
      authState: AuthStateInterface,
      address: AddressStateInterface,
      store: StoreStateInterface
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getRouteParameter();
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      this.Userdata = data.user[0];
    });

    // subscribe to address store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('address')
      )
      .subscribe( (data: AddressStateInterface) => {
        if (data.userAddressState && data.userAddressState.UserAddress && data.userAddressState.UserAddress.city) {
          this.UserAddress = data.userAddressState.UserAddress;

          if (this.Userdata && this.Userdata.UserID) {
            this.getStoreByCategoryFormServer();
          }
        }
    });

    // subscribe to store's store state
    store
      .pipe(
      takeUntil(this.ngUnsubscribe),
      select('store')
      )
      .subscribe( (data: StoreStateInterface) => {
        this.storeNearbyInSelectedCategoryLoaded = data.getStoreByCategoriesState.loaded;
        this.storeNearbyInSelectedCategoryLoading = data.getStoreByCategoriesState.loading;
        this.stores = data.getStoreByCategoriesState.stores;
        this.cd.markForCheck();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
