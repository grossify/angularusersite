import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { StoreCategoriesInterface } from '../../../interfaces/store-categories-interface';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserInterface } from '../../../interfaces/user-interface';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { StoreStateInterface } from '../../../stores/reducers/store.reducer';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesComponent implements OnDestroy {
  public storeCategories: { [catId: number]: StoreCategoriesInterface };
  public noCategoriesStores: boolean;

  public categoryPlaceholderImage =
    './assets/images/logo/category-placeholder.png';

  private ngUnsubscribe = new Subject();

  public Userdata: UserInterface;

  public clickOnStoreCategoryItem(
    storeCategory: StoreCategoriesInterface
  ): void {
    const id = storeCategory.cat_id;
    this.router.navigate(['/etc/store-category', id]);
  }

  constructor(
    private router: Router,
    private store: Store<{
      authState: AuthStateInterface;
      store: StoreStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.cd.markForCheck();
        this.storeCategories = data.StoreCatState.storeCategories;
        this.noCategoriesStores = data.StoreCatState.loaded;
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
