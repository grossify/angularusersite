import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  NgZone,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { UserInterface } from '../../../interfaces/user-interface';
import { Address } from '../../../classes/address';
import { ToastrService } from 'ngx-toastr';
import { GoogleAPIService } from '../../../services/google-api.service';
import { FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';
import {
  IndianStateAndUnionTerritoriesData,
  CountriesData,
} from '../../../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserAddressInterface } from '../../../interfaces/user-address-interface';
import {
  MouseEvent,
  GoogleMapsAPIWrapper,
  AgmMarker,
  LatLngLiteral,
  MapsAPILoader
} from '@agm/core';
import { GoogleGeolocationAPIResponseDataInterface } from '../../../interfaces/google-geolocation-api-response-data-interface';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../../../stores/reducers/address.reducer';
import * as addressRed from '../../../stores/reducers/latLngCity.reducer';
import { GET_GOOGLE_GEOLOCATION } from '../../../stores/actions/application.action';
import { POST_USER_ADDRESS } from '../../../stores/actions/address.action';
import { CLOSE_SETTING_SIDENAV_BAR } from '../../../stores/actions/application.action';

// declare var google: any;

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditAddressComponent implements OnInit, OnDestroy, AfterViewInit {
  public UserAddress: UserAddressInterface;
  private Userdata: UserInterface;

  public stateAndUnion = IndianStateAndUnionTerritoriesData;
  public CountriesData = CountriesData;

  public lat: number;
  public lng: number;
  public zoom: number;
  public defaultZoom: number;

  private geolocationLat: number;
  private geolocationLng: number;

  public googleGeolocation: GoogleGeolocationAPIResponseDataInterface;

  // for storing selected lat lng on map
  public selectedLat: number;
  public selectedLng: number;

  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  public addressList: google.maps.places.AutocompletePrediction[];
  @ViewChild('map', { static: true }) public mapElementRef: ElementRef;

  public addressLine1FormControl = new FormControl('', [
    Validators.minLength(3),
    Validators.maxLength(256),
  ]);

  public addressLine2FormControl = new FormControl('', [
    Validators.minLength(3),
    Validators.maxLength(128),
  ]);

  public cityFormControl = new FormControl('', [
    Validators.minLength(3),
    Validators.maxLength(128),
  ]);

  public stateFormControl = new FormControl('', [
    Validators.minLength(3),
    Validators.maxLength(128),
  ]);

  public countryFormControl = new FormControl('', [
    Validators.minLength(3),
    Validators.maxLength(128),
  ]);

  public postalCodeFormControl = new FormControl('', [
    Validators.minLength(6),
    Validators.maxLength(15),
  ]);

  public addressLine1Matcher = new MyErrorStateMatcher();
  public addressLine2Matcher = new MyErrorStateMatcher();
  public cityMatcher = new MyErrorStateMatcher();
  public stateMatcher = new MyErrorStateMatcher();
  public countryMatcher = new MyErrorStateMatcher();
  public postalCodeMatcher = new MyErrorStateMatcher();

  private ngUnsubscribe = new Subject();

  public mapClicked($event: MouseEvent) {
    // returns lat lng of clicked location from map
    const mouseLat = $event.coords.lat;
    const mouseLng = $event.coords.lng;
    this.lat = mouseLat;
    this.lng = mouseLng;
    this.googleReverseGeocode();
  }

  public centerChange(event: LatLngLiteral): void {
    this.selectedLat = event.lat;
    this.selectedLng = event.lng;
  }

  public markerClicked($event: AgmMarker): void {}

  private getLatLongFromPlaceId(googlePlaceId: string) {
    let placeService = new google.maps.places.PlacesService(
      document.createElement('div')
    );
    placeService.getDetails(
      {
        placeId: googlePlaceId,
      },
      (
        result: google.maps.places.PlaceResult,
        status: google.maps.places.PlacesServiceStatus
      ) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          // console.log(result.geometry.location.lat());
          // console.log(result.geometry.location.lng());
          // move map center to selected address
          this.lat = result.geometry.location.lat();
          this.lng = result.geometry.location.lng();
          this.zoom = this.defaultZoom + 0.5;
          this.getAddressComponentsPlaceResult(result);
        }
      }
    );
  }

  private getAddressComponentsGeocoderResult(
    results: google.maps.GeocoderResult[]
  ): void {
    let country: string;
    let state: string;
    let city: string;
    let postalCode: string;

    if (results[0] && results[0].address_components) {
      results.forEach((firstValue) => {
        firstValue.address_components.forEach((secondValue) => {
          secondValue.types.forEach((element) => {
            if (element === 'country') {
              country = secondValue.long_name;
            }
            if (element === 'administrative_area_level_1') {
              state = secondValue.long_name;
            }
            if (element === 'administrative_area_level_2') {
              city = secondValue.long_name;
            }
            if (!city && element === 'locality') {
              city = secondValue.long_name;
            }
            if (element === 'postal_code') {
              postalCode = secondValue.long_name;
            }
          });
        });
      });
    }

    if (country) {
      this.countryFormControl.setValue(country);
    }
    if (state) {
      this.stateFormControl.setValue(state);
    }
    if (city) {
      this.cityFormControl.setValue(city);
    }
    if (postalCode) {
      this.postalCodeFormControl.setValue(postalCode);
    }
    if (results[0].formatted_address) {
      this.addressLine1FormControl.setValue(results[0].formatted_address);
    }
  }

  private getAddressComponentsPlaceResult(
    results: google.maps.places.PlaceResult
  ): void {
    let country: string;
    let state: string;
    let city: string;
    let postalCode: string;

    if (results.address_components) {
      results.address_components.forEach((firstValue) => {
        firstValue.types.forEach((element) => {
          if (element === 'country') {
            country = firstValue.long_name;
          }
          if (element === 'administrative_area_level_1') {
            state = firstValue.long_name;
          }
          if (element === 'administrative_area_level_2') {
            city = firstValue.long_name;
          }
          if (!city && element === 'locality') {
            city = firstValue.long_name;
          }
          if (element === 'postal_code') {
            postalCode = firstValue.long_name;
          }
        });
      });
    }

    if (country) {
      this.countryFormControl.setValue(country);
    }
    if (state) {
      this.stateFormControl.setValue(state);
    }
    if (city) {
      this.cityFormControl.setValue(city);
    }
    if (postalCode) {
      this.postalCodeFormControl.setValue(postalCode);
    }
    if (results.formatted_address) {
      this.addressLine1FormControl.setValue(results.formatted_address);
    }
  }

  private googleReverseGeocode(): void {
    // get lat lng to address.
    const latlng: google.maps.LatLngLiteral = { lat: this.lat, lng: this.lng };

    this.googleapiservice.geocode(latlng).subscribe((res) => {
      this.getAddressComponentsGeocoderResult(res);
    });
  }

  private getPlaceAutocomplete(searchText: string): void {
    const autocomplete: google.maps.places.Autocomplete = new google.maps.places.Autocomplete(
      this.searchElementRef.nativeElement,
      {
        componentRestrictions: { country: 'in' },
        types: [
          'locality',
          'sublocality',
          'postal_code',
          'country',
          'administrative_area_level_1',
          'administrative_area_level_2',
          'administrative_area_level_3',
          'establishment',
          'geocode',
          'address',
          '(regions)',
          '(cities)',
        ],
      }
    );
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      // set address
      // console.log(place);
      this.getAddressComponentsPlaceResult(place);
    });
  }

  public submitAutocomplete(): boolean {
    // fired on pressing enter in address input 1 form field
    const searchText: string = this.addressLine1FormControl.value;

    if (searchText.length > 4) {
      setTimeout(() => {
        if (searchText === this.addressLine1FormControl.value) {
          // means user stops typing
          this.getPlaceAutocomplete(searchText);
        }
      }, 400); // delay for checking user is typing or not
      return true;
    }
    return false;
  }

  public autocompletekeyUpHandler(): void {
    this.submitAutocomplete();
  }

  public selectedAddress(
    address: google.maps.places.AutocompletePrediction
  ): void {
    // // change addressLine1FormControl value to selected
    // this.addressLine1FormControl.setValue(address.description);

    // // convert placeId to lat long for showing on map
    // this.getLatLongFromPlaceId(address.place_id);

    // // close the predection pannel
    // this.addressList = [];
  }

  public markerDragEnd(lat: number, lng: number, $event: MouseEvent): void {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    this.googleReverseGeocode();
  }

  public onNoClick(): void {
    // this.dialogRef.close({ success: false });
  }

  public onSaveClick(): void {
    const model = new Address(
      this.Userdata.UserID,
      this.addressLine1FormControl.value,
      this.addressLine2FormControl.value,
      this.cityFormControl.value,
      this.postalCodeFormControl.value,
      this.lat,
      this.lng,
      this.stateFormControl.value,
      this.countryFormControl.value
    );
    if (
      this.addressLine1FormControl.valid &&
      this.addressLine2FormControl.valid &&
      this.cityFormControl.valid &&
      this.stateFormControl.valid &&
      this.countryFormControl.valid &&
      this.postalCodeFormControl.valid
    ) {
      if (
        this.addressLine1FormControl.value ||
        this.addressLine2FormControl.value ||
        this.cityFormControl.value ||
        this.stateFormControl.value ||
        this.countryFormControl.value ||
        this.postalCodeFormControl.value
      ) {
        this.toastr.info('Please Wait !', 'info', {
          timeOut: 5000,
          closeButton: true,
        });
        // console.log(model);
        this.store.dispatch(POST_USER_ADDRESS({ userAddressInputData: model }));
        // this.dialogRef.close({ success: true });
      } else {
        this.toastr.error('Please select before submitting !', 'error', {
          timeOut: 5000,
          closeButton: true,
        });
      }
    } else {
      this.toastr.error('Please select before submitting !', 'error', {
        timeOut: 5000,
        closeButton: true,
      });
    }
  }

  public clickOnCrosshair(): void {
    // move map center to selected address
    this.lat = this.geolocationLat;
    this.lng = this.geolocationLng;
    this.zoom = this.defaultZoom + 0.5;
  }

  private findAdress(){
    this.mapsAPILoader.load().then(() => {
         let autocomplete = new google.maps.places.Autocomplete(
          this.searchElementRef.nativeElement,
          {
            componentRestrictions: { country: 'in' },
            types: [
              'locality',
              'sublocality',
              'postal_code',
              'country',
              'administrative_area_level_1',
              'administrative_area_level_2',
              'administrative_area_level_3',
              'establishment',
              'geocode',
              'address',
              '(regions)',
              '(cities)',
            ],
          }
         );
         autocomplete.addListener("place_changed", () => {
           this.ngZone.run(() => {
             // some details
             let place: google.maps.places.PlaceResult = autocomplete.getPlace();
             this.getAddressComponentsPlaceResult(place);
             //set latitude, longitude and zoom
             this.lat = place.geometry.location.lat();
             this.lng = place.geometry.location.lng();
             this.zoom = this.defaultZoom;
           });
         });
       });
   }

  constructor(
    private toastr: ToastrService,
    public gMaps: GoogleMapsAPIWrapper,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
      address: AddressStateInterface;
      latLngCity: addressRed.LatLngCityStateInterface;
    }>,
    private cd: ChangeDetectorRef,
    private googleapiservice: GoogleAPIService
  ) {
    this.store.dispatch(CLOSE_SETTING_SIDENAV_BAR());
    // set default values
    this.defaultZoom = 16;
    this.zoom = this.defaultZoom;

    // subscribe to latLngCity store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('latLngCity'))
      .subscribe((data: addressRed.LatLngCityStateInterface) => {
        if (data.latitude && data.longitude) {
          // set lat lng
          this.lat = data.latitude;
          this.lng = data.longitude;
          this.geolocationLat = data.latitude;
          this.geolocationLng = data.longitude;
          // if (
          //   data.reverseGoogleGeocodeState &&
          //   data.reverseGoogleGeocodeState.googleGeolocation
          // ) {
          //   this.gRAddress = data.reverseGoogleGeocodeState.googleGeolocation;
          // }
        } else {
          this.lat = data.DefaultLatitude;
          this.lng = data.DefaultLongitude;
        }
      });

    // subscribe to store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        if (data.geolocationState.loaded) {
          this.lat = data.geolocationState.geolocation.coords.latitude;
          this.lng = data.geolocationState.geolocation.coords.longitude;
        } else if (data.googleGeolocationState.loaded) {
          this.lat = data.googleGeolocationState.googleGeolocation.location.lat;
          this.lng = data.googleGeolocationState.googleGeolocation.location.lng;
        } else if (data.geolocationState.error) {
          this.store.dispatch(GET_GOOGLE_GEOLOCATION());
        }
      });

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to address store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('address'))
      .subscribe((data: AddressStateInterface) => {
        if (
          data.userAddressState &&
          data.userAddressState.UserAddress &&
          data.userAddressState.UserAddress.city
        ) {
          this.UserAddress = data.userAddressState.UserAddress;
          this.cd.markForCheck();
        }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngAfterViewInit(){
    this.findAdress();
  }
}
