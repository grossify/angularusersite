import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { AppRoutingModule, appRoutingExport } from './app-routing.module';

import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from './stores/index';
import { ApplicationEffects } from './stores/effects/application.effects';
import { AuthEffects } from './stores/effects/auth.effects';
import { StoreEffects } from './stores/effects/store.effects';
import { UserEffects } from './stores/effects/user.effects';
import { AddressEffects } from './stores/effects/address.effects';
import { CartEffects } from './stores/effects/cart.effects';
import { OrderEffects } from './stores/effects/order.effect';
import { LatLngCityEffects } from './stores/effects/latLngCity.effects';
import { extModules } from './build-specifics';
import { localStorageSync } from 'ngrx-store-localstorage';

import { SimpleCrypt } from 'ngx-simple-crypt';
import { UserDataKey, GoogleAPIKey } from './globels';

import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';

function encrypt(state: string) {
  return SimpleCrypt.encodeDefault(UserDataKey, state);
}

function decrypt(state: string) {
  return SimpleCrypt.decodeDefault(UserDataKey, state);
}

export function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return localStorageSync({
    keys: [
      {
        authState: {
          encrypt: (state: string) => encrypt(state), // ENTER YOUR ENCRYPTION FUNCTION HERE
          decrypt: (state: string) => decrypt(state), // ENTER YOUR DECRYPTION FUNCTION HERE
        },
      },
      {
        address: {
          encrypt: (state: string) => encrypt(state),
          decrypt: (state: string) => decrypt(state),
        },
      },
      {
        latLngCity: {
          encrypt: (state: string) => encrypt(state),
          decrypt: (state: string) => decrypt(state),
        },
      },
    ],
    rehydrate: true,
    removeOnUndefined: true,
    checkStorageAvailability: false, // for server side rendering / Universal.
  })(reducer);
}
const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
  declarations: [AppComponent, ...appRoutingExport],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right', // positionClass: 'toast-bottom-center',
      newestOnTop: true,
      preventDuplicates: true,
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatMenuModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),
    extModules, // Instrumentation must be imported after importing StoreModule
    EffectsModule.forRoot([
      ApplicationEffects,
      AuthEffects,
      StoreEffects,
      UserEffects,
      AddressEffects,
      CartEffects,
      OrderEffects,
      LatLngCityEffects,
    ]),
    AgmCoreModule.forRoot({
      apiKey: GoogleAPIKey,
      language: 'en',
      region: 'IN',
      libraries: ['places'],
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
