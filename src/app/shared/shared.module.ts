import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHTMLPipe } from '../pipes/safe-html.pipe';
import { DecodeHTMLPipe } from '../pipes/decode-html.pipe';

@NgModule({
  declarations: [SafeHTMLPipe, DecodeHTMLPipe],
  imports: [CommonModule],
  exports:[SafeHTMLPipe, DecodeHTMLPipe]
})
export class SharedModule {}
