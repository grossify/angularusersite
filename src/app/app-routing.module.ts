import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeMainContentComponent } from './components/home-main-content/home-main-content.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'home',
    component: HomeMainContentComponent,
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'etc',
    loadChildren: () => import('./etc/etc.module').then((m) => m.EtcModule),
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const appRoutingExport = [
  HomeMainContentComponent,
  FooterComponent,
  NavbarComponent,
  PageNotFoundComponent,
];
