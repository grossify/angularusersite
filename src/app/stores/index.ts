import { ActionReducerMap } from '@ngrx/store';
import * as application from './reducers/application.reducer';
import * as auth from './reducers/auth.reducers';
import * as store from './reducers/store.reducer';
import * as user from './reducers/user.reducer';
import * as address from './reducers/address.reducer';
import * as cart from './reducers/cart.reducer';
import * as order from './reducers/order.reducer';
import * as latLngCity from './reducers/latLngCity.reducer';

export interface AppState {
  authState: auth.AuthStateInterface;
  applicationState: application.applicationState;
  store: store.StoreStateInterface;
  user: user.UserState;
  address: address.AddressStateInterface;
  cart: cart.CartStateInterface;
  order: order.OrderState;
  latLngCity: latLngCity.LatLngCityStateInterface;
}

export const reducers: ActionReducerMap<AppState> = {
  authState: auth.authReducer,
  applicationState: application.applicationReducer,
  store: store.storeReducer,
  user: user.userReducer,
  address: address.addressReducer,
  cart: cart.cartReducer,
  order: order.orderReducer,
  latLngCity: latLngCity.LatLngCityReducer,
};
