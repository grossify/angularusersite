import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as store from '../../stores/reducers/store.reducer';

export const getAllStoreCatgoriesState = createFeatureSelector<store.StoreCatStateInterface>('store');

export const getAllStoreCatgories  = createSelector(
    getAllStoreCatgoriesState,
    (entities) => {
        return Object.keys(entities).map(
            catId => {entities[parseInt(catId, 10)]}
        );
    }
);

export const selectLimitAndOffsetFeature = (state: store.StoreStateInterface) => state.limitAndOffsetState;

export const selectLimitAndOffset = createSelector(
    selectLimitAndOffsetFeature,
    (state) => { state }
);
