import { createAction, props } from '@ngrx/store';
import { StoreCategoriesInterface  } from '../../interfaces/store-categories-interface';
import { AddReferStoreInterface } from '../../interfaces/add-refer-store-interface';
import { AddReferStoreClass } from '../../classes/add-refer-store-class';
import {
    GetNearByStoreReqInterface,
    GetNearByStoreInterface
} from '../../interfaces/get-near-by-store';
import {
    FevouriteStoresInterface,
    GetNearByFevouriteStoresReqInterface
} from '../../interfaces/fevourite-stores-interface';
import { StoreDetailInterface, StoreDetailReqInterface } from '../../interfaces/store-detail-interface';
import { ProductCategoriesOfStoresInterface } from '../../interfaces/product-categories-of-stores-interface';
import {
    GetStoreProductsInterface,
    StoreProductsReqInterface
} from '../../interfaces/get-store-products-interface';
import {
    AddFavouriteStoreInterface,
    AddFavouriteStoreReqInterface
} from '../../interfaces/add-favourite-store-interface';
import {
    RemoveFavouriteStoreReqInterface,
    RemoveFavouriteStoreResInterface
} from '../../interfaces/fevourite-stores-interface';
import { GetProductDetailsInterface } from '../../interfaces/get-product-details-interface';
import {
    StoreByCategoriesReqInterface,
    GetStoreByCategoriesInterface
} from '../../interfaces/get-store-by-categories-interface';
import {
    CityStoresInterface,
    CityStoresReqInterface
} from '../../interfaces/city-stores-interface';



export const GET_STORE_CATEGORIES = createAction(
    '[store] get Store Categories from server',
    props<{ userId: number }>()
);

export const GET_STORE_CATEGORIES_FAIL = createAction(
    '[store] Failed get Store Categories from server',
    props<{ error: any }>()
);

export const GET_STORE_CATEGORIES_SUCCESS = createAction(
    '[store] Success get Store Categories from server',
    props<{ storeCategoriesData: Array<StoreCategoriesInterface> }>()
);



export const POST_REFER_STORE = createAction(
    '[store] Add Refer Store',
    props<{ AddReferStoreData: AddReferStoreClass }>()
);

export const POST_REFER_STORE_FAIL = createAction(
    '[store] Failed Add Refer Store',
    props<{ error: any }>()
);

export const POST_REFER_STORE_SUCCESS = createAction(
    '[store] Success Add Refer Store',
    props<{ ReferStoreRes: Array<AddReferStoreInterface> }>()
);



export const GET_NEARBY_STORES = createAction(
    '[store] get nearby stores',
    props<{ nearbyStoreData: GetNearByStoreReqInterface }>()
);

export const GET_NEARBY_STORES_FAIL = createAction(
    '[store] Failed get nearby stores',
    props<{ error: any }>()
);

export const GET_NEARBY_STORES_SUCCESS = createAction(
    '[store] Success get nearby stores',
    props<{ nearbyStoreRes: GetNearByStoreInterface }>()
);



export const GET_NEARBY_FAVOURITE_STORES = createAction(
    '[store] get nearby favourite stores',
    props<{ nearbyFavouriteStoreData: GetNearByFevouriteStoresReqInterface }>()
);

export const GET_NEARBY_FAVOURITE_STORES_FAIL = createAction(
    '[store] Failed get nearby favourite stores',
    props<{ error: any }>()
);

export const GET_NEARBY_FAVOURITE_STORES_SUCCESS = createAction(
    '[store] Success get nearby favourite stores',
    props<{ nearbyFavouriteStoreRes: FevouriteStoresInterface }>()
);



export const GET_STORE_DETAIL = createAction(
    '[store] get store detail',
    props<{ getStoreDetail: StoreDetailReqInterface }>()
);

export const GET_STORE_DETAIL_FAIL = createAction(
    '[store] Failed get store detail',
    props<{ error: any }>()
);

export const GET_STORE_DETAIL_SUCCESS = createAction(
    '[store] Success get store detail',
    props<{ getStoreDetailRes: Array<StoreDetailInterface> }>()
);



export const GET_PRODUCT_CATEGORIES_OF_STORES = createAction(
    '[store] get product categories of stores',
    props<{ storeId: number }>()
);

export const GET_PRODUCT_CATEGORIES_OF_STORES_FAIL = createAction(
    '[store] Failed get product categories of stores',
    props<{ error: any }>()
);

export const GET_PRODUCT_CATEGORIES_OF_STORES_SUCCESS = createAction(
    '[store] Success get product categories of stores',
    props<{  getProductCategoriesRes: Array<ProductCategoriesOfStoresInterface> }>()
);




export const SET_STORES_PRODUCTS_LIMIT_OFFSET = createAction(
    '[store] set limit and offset for stors products',
    props<{ limitAndOffset: {limit: number, offset: number, userId: number} }>()
);




export const GET_STORES_PRODUCTS = createAction(
    '[store] get stores products',
    props<{ storeProductReq: StoreProductsReqInterface }>()
);

export const GET_STORES_PRODUCTS_FAIL = createAction(
    '[store] Failed get stores products',
    props<{ error: any }>()
);

export const GET_STORES_PRODUCTS_SUCCESS = createAction(
    '[store] Success get stores products',
    props<{ getStoreProductRes: GetStoreProductsInterface }>()
);



export const ADD_FAVOURITE_STORE = createAction(
    '[store] add favourite store',
    props<{ addFavouriteReq: AddFavouriteStoreReqInterface }>()
);

export const ADD_FAVOURITE_STORE_FAIL = createAction(
    '[store] Failed add favourite store',
    props<{ error: any }>()
);

export const ADD_FAVOURITE_STORE_SUCCESS = createAction(
    '[store] Success add favourite store',
    props<{ addFavouriteRes: Array<AddFavouriteStoreInterface> }>()
);




export const REMOVE_FAVOURITE_STORE = createAction(
    '[store] remove favourite store',
    props<{ removeFavouriteReq: RemoveFavouriteStoreReqInterface }>()
);

export const REMOVE_FAVOURITE_STORE_FAIL = createAction(
    '[store] Failed remove favourite store',
    props<{ error: any }>()
);

export const REMOVE_FAVOURITE_STORE_SUCCESS = createAction(
    '[store] Success remove favourite store',
    props<{ removeFavouriteRes: RemoveFavouriteStoreResInterface }>()
);




export const GET_STORE_PRODUCT_DETAIL = createAction(
    '[store] get store product detail',
    props<{ storeProductId: number }>()
);

export const GET_STORE_PRODUCT_DETAIL_FAIL = createAction(
    '[store] Failed get store product detail',
    props<{ error: any }>()
);

export const GET_STORE_PRODUCT_DETAIL_SUCCESS = createAction(
    '[store] Success get store product detail',
    props<{ getStoreProductRes: Array<GetProductDetailsInterface> }>()
);





export const GET_STORE_BY_CITY = createAction(
    '[store] get stores by city',
    props<{ reqData: CityStoresReqInterface }>()
);

export const GET_STORE_BY_CITY_FAIL = createAction(
    '[store] Failed get stores by city',
    props<{ error: any }>()
);

export const GET_STORE_BY_CITY_SUCCESS = createAction(
    '[store] Success get stores by city',
    props<{ getCityStoresRes: CityStoresInterface }>()
);





export const GET_STORE_BY_CATEGORIES = createAction(
    '[store] get nearby store by categories',
    props<{ getStoreByCatData: StoreByCategoriesReqInterface }>()
);

export const GET_STORE_BY_CATEGORIES_FAIL = createAction(
    '[store] Failed get nearby store by categories',
    props<{ error: any }>()
);

export const GET_STORE_BY_CATEGORIES_SUCCESS = createAction(
    '[store] Success get nearby store by categories',
    props<{ getStoreRes: GetStoreByCategoriesInterface }>()
);