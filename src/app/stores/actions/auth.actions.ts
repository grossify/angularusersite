import { createAction, props } from '@ngrx/store';
import { SignInInterface, LoginIputDataInterface } from '../../interfaces/sign-in-interface';
import { SignUpInterface, RegistrationIputDataInterface } from '../../interfaces/sign-up-interface';
import { UpdateEndUserProfileInterface } from '../../interfaces/update-end-user-profile-interface';
import { UserInterface } from '../../interfaces/user-interface';


export const LOGIN_FAIL = createAction(
    '[Auth] Login Failed',
    props<{ error: any }>()
);

export const LOGIN_SUCCESS = createAction(
    '[Auth] Login Success',
    props<{ userData: Array<SignInInterface> }>()
);

export const LOGIN = createAction(
    '[Auth] Login',
    props<{ LoginInputData: LoginIputDataInterface }>()
);



export const LOGIN_DATA_UPDATE = createAction(
    '[Auth update] Login data update Success',
    props<{ userData: Array<SignInInterface> }>()
);




export const LOGIN_PROFILE_PIC_UPDATE = createAction(
    '[Auth profile pic] Login profile pic data update Success',
    props<{ updateEndUserProfileUpdateRes: UpdateEndUserProfileInterface }>()
);



export const REGISTRATION_FAIL = createAction(
    '[Auth] Registration Failed',
    props<{ error: any }>()
);

export const REGISTRATION_SUCCESS = createAction(
    '[Auth] Registration Success',
    props<{ registrationData: Array<SignUpInterface> }>()
);

export const REGISTRATION = createAction(
    '[Auth] Registration',
    props<{ registrationIputData: RegistrationIputDataInterface }>()
);

export const LOGOUT = createAction('[Auth] Logout');

export const GET_END_USER_PROFILE = createAction(
    '[User Profile] get user profile data',
    props<{userName: string}>()
);

export const GET_END_USER_PROFILE_FAIL = createAction(
    '[User Profile] Failed get user profile data',
    props<{ error: any }>()
);

export const GET_END_USER_PROFILE_SUCCESS = createAction(
    '[User Profile] Success get user profile data',
    props<{ GetUserProfileRes: Array<UserInterface> }>()
);