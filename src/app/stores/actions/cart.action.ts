import { createAction, props } from '@ngrx/store';
import { GetCartByUserIdInterface } from '../../interfaces/get-cart-by-user-id-interface';
import { AddProductInCartInputInterface } from '../../interfaces/add-product-in-cart-interface';
import { DeleteProductInCartInputInterface } from '../../interfaces/delete-product-from-cart-interface';
import { ChangeProductFromCartInputInterface } from '../../interfaces/change-product-quantity-in-cart-interface';
import { DeleteAllCartInterface } from '../../interfaces/delete-all-cart-interface';

export const GET_CART = createAction(
    '[Cart] get cart from server',
    props<{ userId: number }>()
);

export const GET_CART_FAIL = createAction(
    '[Cart] Failed get cart from server',
    props<{ error: any }>()
);

export const GET_CART_SUCCESS = createAction(
    '[Cart] Success get cart from server',
    props<{ GetCartRes: GetCartByUserIdInterface }>()
);



export const DELETE_PRODUCT_FROM_CART = createAction(
    '[Cart] delete cart product',
    props<{ reqData: DeleteProductInCartInputInterface }>()
);

export const DELETE_PRODUCT_FROM_CART_FAIL = createAction(
    '[Cart] Failed delete cart product',
    props<{ error: any }>()
);

export const DELETE_PRODUCT_FROM_CART_SUCCESS = createAction(
    '[Cart] Success delete cart product',
    props<{ GetCartRes: GetCartByUserIdInterface }>()
);



export const ADD_PRODUCT_IN_CART = createAction(
    '[Cart] add cart in server',
    props<{ reqData: AddProductInCartInputInterface }>()
);

export const ADD_PRODUCT_IN_CART_FAIL = createAction(
    '[Cart] Failed add cart in server',
    props<{ error: any }>()
);

export const ADD_PRODUCT_IN_CART_SUCCESS = createAction(
    '[Cart] Success add cart in server',
    props<{ GetCartRes: GetCartByUserIdInterface }>()
);


export const CHANGE_PRODUCT_QUANTITY_IN_CART = createAction(
    '[Cart] change product quantity in cart',
    props<{ reqData: ChangeProductFromCartInputInterface }>()
);

export const CHANGE_PRODUCT_QUANTITY_IN_CART_FAIL = createAction(
    '[Cart] Failed change product quantity in cart',
    props<{ error: any }>()
);

export const CHANGE_PRODUCT_QUANTITY_IN_CART_SUCCESS = createAction(
    '[Cart] Success change product quantity in cart',
    props<{ ChangeCartRes: GetCartByUserIdInterface }>()
);



export const DELETE_ALL_CART = createAction(
    '[Cart] delete all cart',
    props<{ userId: number }>()
);

export const DELETE_ALL_CART_FAIL = createAction(
    '[Cart] Failed delete all cart',
    props<{ error: any }>()
);

export const DELETE_ALL_CART_SUCCESS = createAction(
    '[Cart] Success delete all cart',
    props<{ DeleteCartRes: DeleteAllCartInterface }>()
);
