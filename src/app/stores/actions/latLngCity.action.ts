import { createAction, props } from '@ngrx/store';
import { GoogleGeolocationAPIResponseDataInterface } from '../../interfaces/google-geolocation-api-response-data-interface';
import { AddressStateInterface } from '../reducers/latLngCity.reducer';

// declare var google: any;

export const GET_LAT_LNG_CITY = createAction(
  '[LatLngCity] get latitude longitude and city'
);

export const FAIL_GET_LAT_LNG_CITY = createAction(
  '[LatLngCity] fail get latitude longitude and city',
  props<{ error: any }>()
);

export const SUCCESS_GET_LAT_LNG_CITY = createAction(
  '[LatLngCity] success get latitude longitude and city',
  props<{
    latitude: number;
    longitude: number;
    city: string;
  }>()
);

export const UPDATE_LAT_LNG_CITY = createAction(
  '[LatLngCity] update latitude longitude and city',
  props<{
    latitude: number;
    longitude: number;
    city: string;
  }>()
);

export const SET_DEFAULT_LAT_LNG_CITY = createAction(
  '[LatLngCity] update latitude longitude and city'
);

export const UPDATE_REVERSE_GEOCODE_ADDRESS = createAction(
  '[LatLngCity] update reverse Geocode address',
  props<{ reqData: AddressStateInterface }>()
);

export const GET_GOOGLE_GEOLOCATION = createAction(
  '[Google Geolocation] get Google Geolocation'
);

export const GET_GOOGLE_GEOLOCATION_FAIL = createAction(
  '[Google Geolocation] Failed get Google Geolocation',
  props<{ error: any }>()
);

export const GET_GOOGLE_GEOLOCATION_SUCCESS = createAction(
  '[Google Geolocation] Success get Google Geolocation',
  props<{ GoogleGeolocationRes: GoogleGeolocationAPIResponseDataInterface }>()
);

// export const GET_REVERSE_GEOCODE_ADDRESS = createAction(
//   '[Google Reverse Geocode] get reverse Geocode address',
//   props<{ latLng: google.maps.LatLng }>()
// );

export const GET_REVERSE_GEOCODE_ADDRESS = createAction(
  '[Google Reverse Geocode] get reverse Geocode address',
  props<{ latLng: google.maps.LatLngLiteral }>()
);

export const FAIL_REVERSE_GEOCODE_ADDRESS = createAction(
  '[Google Reverse Geocode] fail get reverse Geocode address',
  props<{ error: any }>()
);

export const SUCCESS_REVERSE_GEOCODE_ADDRESS = createAction(
  '[Google Reverse Geocode] success get reverse Geocode address',
  props<{ results: google.maps.GeocoderResult[] }>()
);
