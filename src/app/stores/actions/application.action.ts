import { createAction, props } from '@ngrx/store';
import { GetBannerInterface } from '../../interfaces/get-banner-interface';
import { GeolocationPositionInterface } from '../../interfaces/geolocation-interface';
import {
  GoogleGeolocationAPIResponseDataInterface
} from '../../interfaces/google-geolocation-api-response-data-interface';
import { GetOffersInterface } from '../../interfaces/get-offers-interface';

export const OPEN_MAIN_SIDENAV_BAR = createAction('[main dashboard] open sidenav');

export const CLOSE_MAIN_SIDENAV_BAR = createAction('[main dashboard] close sidenav');

export const TOGGLE_MAIN_SIDENAV_BAR = createAction('[main dashboard] toggle sidenav');

export const CHANGE_SELECTED_MAIN_SIDENAV_BAR_ITEM = createAction(
    '[main dashboard] change sidenav',
    (payload: string) => ({payload})
);

export const SET_SCREEN = createAction(
    '[main dashboard] set screen type, mobile, tablet or desktop',
    (width: number) => ({width})
);

export const OPEN_SETTING_SIDENAV_BAR = createAction('[setting dashboard] open sidenav');

export const CLOSE_SETTING_SIDENAV_BAR = createAction('[setting dashboard] close sidenav');

export const TOGGLE_SETTING_SIDENAV_BAR = createAction('[setting dashboard] toggle sidenav');

export const CHANGE_SELECTED_SETTING_SIDENAV_BAR_ITEM = createAction(
    '[setting dashboard] change sidenav',
    (navBarSelect: string) => ({navBarSelect})
);

export const GET_BANNER = createAction(
    '[Banner] get banner'
);

export const GET_BANNER_FAIL = createAction(
    '[Banner] Failed get banner',
    props<{ error: any }>()
);

export const GET_BANNER_SUCCESS = createAction(
    '[Banner] Success get banner',
    props<{ GetBannerRes: Array<GetBannerInterface> }>()
);

export const GET_GEOLOCATION = createAction(
    '[Geolocation] get Geolocation'
);

export const GET_GEOLOCATION_FAIL = createAction(
    '[Geolocation] Failed get Geolocation',
    props<{ error: any }>()
);

export const GET_GEOLOCATION_SUCCESS = createAction(
    '[Geolocation] Success get Geolocation',
    props<{ GeolocationRes: GeolocationPositionInterface }>()
);

export const GET_GOOGLE_GEOLOCATION = createAction(
    '[Google Geolocation] get Google Geolocation'
);

export const GET_GOOGLE_GEOLOCATION_FAIL = createAction(
    '[Google Geolocation] Failed get Google Geolocation',
    props<{ error: any }>()
);

export const GET_GOOGLE_GEOLOCATION_SUCCESS = createAction(
    '[Google Geolocation] Success get Google Geolocation',
    props<{ GoogleGeolocationRes: GoogleGeolocationAPIResponseDataInterface }>()
);



export const GET_OFFERS = createAction(
    '[Offers] get Offers'
);

export const GET_OFFERS_FAIL = createAction(
    '[Offers] Failed get Offers',
    props<{ error: any }>()
);

export const GET_OFFERS_SUCCESS = createAction(
    '[Offers] Success get Offers',
    props<{ getOffersRes: GetOffersInterface }>()
);
