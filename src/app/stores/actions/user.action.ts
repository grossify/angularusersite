import { createAction, props } from '@ngrx/store';
import {
    ChangePasswordInputInterface,
    ChangePasswordResponseSuccessInterface
} from '../../interfaces/change-password-interface';
import { UpdateEndUserProfileInterface } from '../../interfaces/update-end-user-profile-interface';
import { UpdateEndUserInterface } from '../../interfaces/update-end-user-interface';
import { User } from '../../classes/user';



export const CHANGE_PASSWORD = createAction(
    '[User Password] change user password',
    props<{ passData: ChangePasswordInputInterface }>()
);

export const CHANGE_PASSWORD_FAIL = createAction(
    '[User Password] Failed change user password',
    props<{ error: any }>()
);

export const CHANGE_PASSWORD_SUCCESS = createAction(
    '[User Password] Success change user password',
    props<{ changePassRes: ChangePasswordResponseSuccessInterface }>()
);



export const UPDATE_USER_DATA = createAction(
    '[User Profile] update user profile data',
    props<{ reqData: User }>()
);

export const UPDATE_USER_DATA_FAIL = createAction(
    '[User Profile] Failed update user profile data',
    props<{ error: any }>()
);

export const UPDATE_USER_DATA_SUCCESS = createAction(
    '[User Profile] Success update user profile data',
    props<{ updateEndUserDataRes: Array<UpdateEndUserInterface> }>()
);




export const UPDATE_USER_PROFILE_PIC = createAction(
    '[User Profile pic] update user profile pic',
    props<{ reqData: {userId: number, croppedFile: Blob} }>()
);

export const UPDATE_USER_PROFILE_PIC_FAIL = createAction(
    '[User Profile pic] Failed update user profile pic',
    props<{ error: any }>()
);

export const UPDATE_USER_PROFILE_PIC_SUCCESS = createAction(
    '[User Profile pic] Success update user profile pic',
    props<{ updateEndUserProfileUpdateRes: UpdateEndUserProfileInterface }>()
);
