import { createAction, props } from '@ngrx/store';
import { GetOrdersInterface, GetOrdersInputInterface } from '../../interfaces/get-orders-interface';
import { GetorderdetailsInterface } from '../../interfaces/getorderdetails-interface';
import { PostOrdersReqInterface, PostOrderFromCartInterface } from '../../interfaces/post-order-from-cart-interface';

export const GET_ALL_ORDERS = createAction(
    '[Orders] get Orders from server',
    props<{ reqData: GetOrdersInputInterface }>()
);

export const GET_ALL_ORDERS_FAIL = createAction(
    '[Orders] Failed get Orders from server',
    props<{ error: any }>()
);

export const GET_ALL_ORDERS_SUCCESS = createAction(
    '[Orders] Success get Orders from server',
    props<{ GetOrdersRes: GetOrdersInterface }>()
);



export const GET_ORDER_DETALS = createAction(
    '[Orders] get Orders deatails from server',
    props<{ orderId: number }>()
);

export const GET_ORDER_DETALS_FAIL = createAction(
    '[Orders] Failed get Orders deatails from server',
    props<{ error: any }>()
);

export const GET_ORDER_DETALS_SUCCESS = createAction(
    '[Orders] Success get Orders deatails from server',
    props<{ GetOrderDetailRes: Array<GetorderdetailsInterface> }>()
);



export const POST_ORDER_FROM_CART = createAction(
    '[Orders] post order from cart',
    props<{ orderReq: PostOrdersReqInterface }>()
);

export const POST_ORDER_FROM_CART_FAIL = createAction(
    '[Orders] Failed post order from cart',
    props<{ error: any }>()
);

export const POST_ORDER_FROM_CART_SUCCESS = createAction(
    '[Orders] Success post order from cart',
    props<{ postOrderRes: Array<PostOrderFromCartInterface> }>()
);
