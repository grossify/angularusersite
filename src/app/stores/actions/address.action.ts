import { createAction, props } from '@ngrx/store';
import { UserAddressInterface, UserAddressInputInterface } from '../../interfaces/user-address-interface';

export const GET_USER_ADDRESS = createAction(
    '[Address] get user address',
    props<{ userId: number }>()
);

export const GET_USER_ADDRESS_FAIL = createAction(
    '[Address] Failed get user address',
    props<{ error: any }>()
);

export const GET_USER_ADDRESS_SUCCESS = createAction(
    '[Address] Success get user address',
    props<{ GetUserAddressRes: Array<UserAddressInterface> }>()
);

export const POST_USER_ADDRESS = createAction(
    '[Address] post user address',
    props<{ userAddressInputData: UserAddressInputInterface }>()
);

export const POST_USER_ADDRESS_FAIL = createAction(
    '[Address] Failed post user address',
    props<{ error: any }>()
);

export const POST_USER_ADDRESS_SUCCESS = createAction(
    '[Address] Success post user address',
    props<{ PostUserAddressRes: Array<UserAddressInterface> }>()
);

export const DELETE_USER_ADDRESS = createAction('[Address] delete user address after logout');
