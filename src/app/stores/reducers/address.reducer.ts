import { createReducer, on } from '@ngrx/store';
import * as AddressActions from '../actions/address.action';
import {
  UserAddressInterface,
  UserAddressInputInterface,
} from '../../interfaces/user-address-interface';

export interface GetUserAddressStateInterface {
  UserAddress: UserAddressInterface | null;
  error: string | null;
  loaded: boolean;
  loading: boolean;
}

export interface AddressStateInterface {
  userAddressState: GetUserAddressStateInterface;
  postUserAddressState: GetUserAddressStateInterface;
}

export const initialState: AddressStateInterface = {
  userAddressState: {
    UserAddress: null,
    error: null,
    loaded: null,
    loading: null,
  },
  postUserAddressState: {
    UserAddress: null,
    error: null,
    loaded: null,
    loading: null,
  },
};

const _addressReducer = createReducer(
  initialState,
  on(AddressActions.GET_USER_ADDRESS, (state: AddressStateInterface) => {
    const newState: AddressStateInterface = {
      ...state,
      userAddressState: {
        UserAddress: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AddressActions.GET_USER_ADDRESS_FAIL,
    (state: AddressStateInterface, { error }) => {
      const newState: AddressStateInterface = {
        ...state,
        userAddressState: {
          UserAddress: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    AddressActions.GET_USER_ADDRESS_SUCCESS,
    (state: AddressStateInterface, { GetUserAddressRes }) => {
      const newState: AddressStateInterface = {
        ...state,
        userAddressState: {
          UserAddress: GetUserAddressRes[0],
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(AddressActions.POST_USER_ADDRESS, (state: AddressStateInterface) => {
    const newState: AddressStateInterface = {
      ...state,
      postUserAddressState: {
        UserAddress: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AddressActions.POST_USER_ADDRESS_FAIL,
    (state: AddressStateInterface, { error }) => {
      const newState: AddressStateInterface = {
        ...state,
        postUserAddressState: {
          UserAddress: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    AddressActions.POST_USER_ADDRESS_SUCCESS,
    (state: AddressStateInterface, { PostUserAddressRes }) => {
      const newState: AddressStateInterface = {
        ...state,
        userAddressState: {
          UserAddress: PostUserAddressRes[0],
          error: null,
          loading: false,
          loaded: true,
        },
        postUserAddressState: {
          UserAddress: PostUserAddressRes[0],
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(AddressActions.DELETE_USER_ADDRESS, (state: AddressStateInterface) => {
    const newState: AddressStateInterface = {
      ...state,
      userAddressState: {
        UserAddress: null,
        error: null,
        loading: false,
        loaded: true,
      },
    };
    return newState;
  })
);

export function addressReducer(
  state = initialState,
  action: any
): AddressStateInterface {
  return _addressReducer(state, action);
}
