import { createReducer, on } from '@ngrx/store';
import * as cartActions from '../actions/cart.action';
import { GetCartByUserIdInterface } from '../../interfaces/get-cart-by-user-id-interface';

export interface GetCartStateInterface {
    Cart: GetCartByUserIdInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface DeleteProductFromStateInterface {
    Cart: GetCartByUserIdInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface AddProductInCartStateInterface {
    Cart: GetCartByUserIdInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface ChangeroductQntInCartStateInterface {
    Cart: GetCartByUserIdInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface DeleteAllCartStateInterface {
    Cart: any;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}


export interface CartStateInterface {
    CartState: GetCartStateInterface;
    deleteProductFromCartState: DeleteProductFromStateInterface;
    addProductInCartState: AddProductInCartStateInterface;
    ChangeroductQntInCartState: ChangeroductQntInCartStateInterface;
    DeleteAllCartState: DeleteAllCartStateInterface;
}

export const initialState: CartStateInterface = {
    CartState: {
        Cart: null,
        error: null,
        loaded: null,
        loading: null
    },
    deleteProductFromCartState: {
        Cart: null,
        error: null,
        loaded: null,
        loading: null
    },
    addProductInCartState: {
        Cart: null,
        error: null,
        loaded: null,
        loading: null
    },
    ChangeroductQntInCartState: {
        Cart: null,
        error: null,
        loaded: null,
        loading: null
    },
    DeleteAllCartState: {
        Cart: null,
        error: null,
        loaded: null,
        loading: null
    }
}

const _cartReducer = createReducer(
    initialState,
    on(cartActions.GET_CART, (state: CartStateInterface) => {
        const newState: CartStateInterface = {
            ...state,
            CartState : {
                Cart: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.GET_CART_FAIL, (state: CartStateInterface, { error }) => {
        const newState: CartStateInterface = {
            ...state,
            CartState : {
                Cart: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.GET_CART_SUCCESS, (state: CartStateInterface, { GetCartRes }) => {
        // check for empty cart response
        // {
        //     "status": true,
        //     "message": "Empty cart"
        // }
        if(GetCartRes['status'] && GetCartRes['message'] === 'Empty cart') {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: null, // no item in cart
                    error: null,
                    loading: false,
                    loaded: false
                }
            }
            return newState;
        } else {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: GetCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                }
            }
            return newState;
        }
    }),
    on(cartActions.ADD_PRODUCT_IN_CART, (state: CartStateInterface) => {
        const newState: CartStateInterface = {
            ...state,
            addProductInCartState : {
                Cart: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.ADD_PRODUCT_IN_CART_FAIL, (state: CartStateInterface, { error }) => {
        const newState: CartStateInterface = {
            ...state,
            addProductInCartState : {
                Cart: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.ADD_PRODUCT_IN_CART_SUCCESS, (state: CartStateInterface, { GetCartRes }) => {
        if(GetCartRes['status'] && GetCartRes['message'] === 'Empty cart') {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                },
                addProductInCartState : {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                }
            }
            return newState;
        } else {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: GetCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                },
                addProductInCartState : {
                    Cart: GetCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                }
            }
            return newState;
        }
    }),
    on(cartActions.DELETE_PRODUCT_FROM_CART, (state: CartStateInterface) => {
        const newState: CartStateInterface = {
            ...state,
            deleteProductFromCartState: {
                Cart: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.DELETE_PRODUCT_FROM_CART_FAIL, (state: CartStateInterface, { error }) => {
        const newState: CartStateInterface = {
            ...state,
            deleteProductFromCartState: {
                Cart: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.DELETE_PRODUCT_FROM_CART_SUCCESS, (state: CartStateInterface, { GetCartRes }) => {
        // console.log(GetCartRes);
        if(GetCartRes['status'] && GetCartRes['message'] === 'Empty cart') {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                },
                deleteProductFromCartState: {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                }
            }
            return newState;
        } else {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: GetCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                },
                deleteProductFromCartState: {
                    Cart: GetCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                }
            }
            return newState;
        }
    }),
    on(cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART, (state: CartStateInterface) => {
        const newState: CartStateInterface = {
            ...state,
            ChangeroductQntInCartState : {
                Cart: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART_FAIL, (state: CartStateInterface, { error }) => {
        const newState: CartStateInterface = {
            ...state,
            ChangeroductQntInCartState : {
                Cart: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART_SUCCESS, (state: CartStateInterface, { ChangeCartRes }) => {
        if(ChangeCartRes['status'] && ChangeCartRes['message'] === 'Empty cart') {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                },
                ChangeroductQntInCartState : {
                    Cart: null,
                    error: null,
                    loading: false,
                    loaded: false
                }
            }
            return newState;
        } else {
            const newState: CartStateInterface = {
                ...state,
                CartState : {
                    Cart: ChangeCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                },
                ChangeroductQntInCartState : {
                    Cart: ChangeCartRes,
                    error: null,
                    loading: false,
                    loaded: true
                }
            }
            return newState;
        }
    }),
    on(cartActions.DELETE_ALL_CART, (state: CartStateInterface) => {
        const newState: CartStateInterface = {
            ...state,
            DeleteAllCartState: {
                Cart: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.DELETE_ALL_CART_FAIL, (state: CartStateInterface, { error }) => {
        const newState: CartStateInterface = {
            ...state,DeleteAllCartState: {
                Cart: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(cartActions.DELETE_ALL_CART_SUCCESS, (state: CartStateInterface, { DeleteCartRes }) => {
        const newState: CartStateInterface = {
            ...state,
            CartState: {
                Cart: null,
                error: null,
                loading: false,
                loaded: false
            },
            DeleteAllCartState: {
                Cart: DeleteCartRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    })
);

export function cartReducer(
    state = initialState,
    action: any
): CartStateInterface {
    return _cartReducer(state, action);
}
