import { createReducer, on } from '@ngrx/store';
import * as OrderActions from '../actions/order.action';
import { GetOrdersInterface, OrderItemInterface } from '../../interfaces/get-orders-interface';
import { GetorderdetailsInterface } from '../../interfaces/getorderdetails-interface';
import { PostOrdersReqInterface, PostOrderFromCartInterface } from '../../interfaces/post-order-from-cart-interface';

export interface getALlOrderStateInterface {
    orders: {[orderId: number]: OrderItemInterface};
    ordersRes: GetOrdersInterface | null;
    error: any;
    loaded: boolean;
    loading: boolean;
}

export interface getOrderDetailsStateInterface {
    orderDetail: Array<GetorderdetailsInterface> | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface postOrderStateInterface {
    postOrder: Array<PostOrderFromCartInterface> | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface OrderState {
    getAllOrderState: getALlOrderStateInterface;
    getOrderDetailsState: getOrderDetailsStateInterface;
    postOrderState: postOrderStateInterface;
}

export const initialState: OrderState = {
    getAllOrderState: {
        orders: {},
        ordersRes: null,
        error: null,
        loaded: null,
        loading: null,
    },
    getOrderDetailsState: {
        orderDetail: null,
        error: null,
        loaded: null,
        loading: null,
    },
    postOrderState: {
        postOrder: null,
        error: null,
        loaded: null,
        loading: null,
    }
}

const _orderReducer = createReducer(
    initialState,
    on(OrderActions.GET_ALL_ORDERS, (state: OrderState) => {
        const newState: OrderState = {
            ...state,
            getAllOrderState : {
                orders: null,
                ordersRes: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.GET_ALL_ORDERS_FAIL, (state: OrderState, { error }) => {
        const newState: OrderState = {
            ...state,
            getAllOrderState : {
                orders: null,
                ordersRes: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.GET_ALL_ORDERS_SUCCESS, (state: OrderState, { GetOrdersRes }) => {
        const entities = GetOrdersRes.result.reduce(
            (entities: {[orderId: number]: OrderItemInterface}, GetOrdersRes: OrderItemInterface) => {
            return {
                ...entities,
                [GetOrdersRes.OrderID] : GetOrdersRes
            };
        }, {
            ...state.getAllOrderState.orders
        });

        const newState: OrderState = {
            ...state,
            getAllOrderState : {
                orders: entities,
                ordersRes: GetOrdersRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(OrderActions.GET_ORDER_DETALS, (state: OrderState) => {
        const newState: OrderState = {
            ...state,
            getOrderDetailsState: {
                orderDetail: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.GET_ORDER_DETALS_FAIL, (state: OrderState, { error }) => {
        const newState: OrderState = {
            ...state,
            getOrderDetailsState: {
                orderDetail: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.GET_ORDER_DETALS_SUCCESS, (state: OrderState, { GetOrderDetailRes }) => {
        const newState: OrderState = {
            ...state,
            getOrderDetailsState: {
                orderDetail: GetOrderDetailRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(OrderActions.POST_ORDER_FROM_CART, (state: OrderState) => {
        const newState: OrderState = {
            ...state,
            postOrderState: {
                postOrder: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.POST_ORDER_FROM_CART_FAIL, (state: OrderState, { error }) => {
        const newState: OrderState = {
            ...state,
            postOrderState: {
                postOrder: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(OrderActions.POST_ORDER_FROM_CART_SUCCESS, (state: OrderState, { postOrderRes }) => {
        const newState: OrderState = {
            ...state,
            postOrderState: {
                postOrder: postOrderRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    })
);

export function orderReducer(
    state = initialState,
    action: any
): OrderState {
    return _orderReducer(state, action);
}
