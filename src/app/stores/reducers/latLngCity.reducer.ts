import { createReducer, on } from '@ngrx/store';
import * as LatLngCityAction from '../actions/latLngCity.action';
import {
  GoogleGeolocationAPIResponseDataInterface,
  GoogleGeolocationAPIErrorsInterface,
} from '../../interfaces/google-geolocation-api-response-data-interface';

// declare var google: any;

export interface GoogleGeolocationStateInterface {
  googleGeolocation: GoogleGeolocationAPIResponseDataInterface | null;
  error: GoogleGeolocationAPIErrorsInterface | any;
  loaded: boolean;
  loading: boolean;
}

export interface GoogleReverseGeoCodeStateInterface {
  addressRes: Array<google.maps.GeocoderResult> | null;
  error: any;
  loaded: boolean;
  loading: boolean;
}

export interface AddressStateInterface {
  country?: string;
  state?: string;
  city?: string;
  postalCode?: string;
  formatted_address?: string;
  // latitude: number;
  // longitude: number;
}

export interface ReverseGoogleGeocodeStateInterface {
  googleGeolocation: AddressStateInterface | null;
  error: any;
  loaded: boolean;
  loading: boolean;
}

export interface LatLngCityStateInterface {
  error: any;
  loading: boolean;
  loaded: boolean;
  latitude: number | null;
  longitude: number | null;
  city: string | null;
  postalCode: string | null;
  DefaultLatitude: number;
  DefaultLongitude: number;
  DefaultCity: string;
  googleGeolocationState: GoogleGeolocationStateInterface;
  reverseGoogleGeocodeState: ReverseGoogleGeocodeStateInterface;
  googleReverseGeoCodeState: GoogleReverseGeoCodeStateInterface;
}

export const initialState: LatLngCityStateInterface = {
  error: null,
  loading: false,
  loaded: false,
  latitude: null,
  longitude: null,
  city: null,
  postalCode: null,
  DefaultLatitude: 20.003677,
  DefaultLongitude: 73.777505,
  DefaultCity: 'nashik',
  googleGeolocationState: {
    googleGeolocation: null,
    error: null,
    loaded: false,
    loading: false,
  },
  reverseGoogleGeocodeState: {
    googleGeolocation: null,
    error: null,
    loaded: false,
    loading: false,
  },
  googleReverseGeoCodeState: {
    addressRes: null,
    error: null,
    loaded: false,
    loading: false,
  },
};

const _LatLngCityReducer = createReducer(
  initialState,
  on(LatLngCityAction.GET_LAT_LNG_CITY, (state: LatLngCityStateInterface) => {
    const newState: LatLngCityStateInterface = {
      ...state,
      error: null,
      loading: true,
      loaded: false,
    };
    return newState;
  }),
  on(
    LatLngCityAction.FAIL_GET_LAT_LNG_CITY,
    (state: LatLngCityStateInterface, { error }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        error: error,
        loading: false,
        loaded: false,
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.SUCCESS_GET_LAT_LNG_CITY,
    (state: LatLngCityStateInterface, { latitude, longitude, city }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        latitude: latitude,
        longitude: longitude,
        city: city,
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.UPDATE_LAT_LNG_CITY,
    (state: LatLngCityStateInterface, { latitude, longitude, city }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        latitude: latitude,
        longitude: longitude,
        city: city,
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.SET_DEFAULT_LAT_LNG_CITY,
    (state: LatLngCityStateInterface) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        latitude: state.DefaultLatitude,
        longitude: state.DefaultLongitude,
        city: state.DefaultCity,
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.UPDATE_REVERSE_GEOCODE_ADDRESS,
    (state: LatLngCityStateInterface, { reqData }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        city: reqData.city,
        postalCode: reqData.postalCode,
        reverseGoogleGeocodeState: {
          googleGeolocation: reqData,
          error: null,
          loaded: false,
          loading: false,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.GET_GOOGLE_GEOLOCATION,
    (state: LatLngCityStateInterface) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        googleGeolocationState: {
          googleGeolocation: null,
          error: null,
          loading: true,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.GET_GOOGLE_GEOLOCATION_FAIL,
    (state: LatLngCityStateInterface, { error }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        googleGeolocationState: {
          googleGeolocation: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.GET_GOOGLE_GEOLOCATION_SUCCESS,
    (state: LatLngCityStateInterface, { GoogleGeolocationRes }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        latitude: GoogleGeolocationRes.location.lat,
        longitude: GoogleGeolocationRes.location.lng,
        googleGeolocationState: {
          googleGeolocation: GoogleGeolocationRes,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.GET_REVERSE_GEOCODE_ADDRESS,
    (state: LatLngCityStateInterface) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        googleReverseGeoCodeState: {
          addressRes: null,
          error: null,
          loading: true,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.FAIL_REVERSE_GEOCODE_ADDRESS,
    (state: LatLngCityStateInterface, { error }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        googleReverseGeoCodeState: {
          addressRes: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    LatLngCityAction.SUCCESS_REVERSE_GEOCODE_ADDRESS,
    (state: LatLngCityStateInterface, { results }) => {
      const newState: LatLngCityStateInterface = {
        ...state,
        googleReverseGeoCodeState: {
          addressRes: results,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  )
);

export function LatLngCityReducer(
  state = initialState,
  action: any
): LatLngCityStateInterface {
  return _LatLngCityReducer(state, action);
}
