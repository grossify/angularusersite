import { createReducer, on } from '@ngrx/store';
import * as UserStateActions from '../actions/user.action';
import {
    ChangePasswordResponseSuccessInterface,
    ChangePasswordResponseErrorInterface
} from '../../interfaces/change-password-interface';
import { UpdateEndUserProfileInterface } from '../../interfaces/update-end-user-profile-interface';
import { UpdateEndUserInterface } from '../../interfaces/update-end-user-interface';

export interface changePasswordStateInterface {
    changePassword: ChangePasswordResponseSuccessInterface | null;
    error: ChangePasswordResponseErrorInterface | any;
    loaded: boolean;
    loading: boolean;
}

export interface UpdateUserProfileDataStateInterface {
    userData: UpdateEndUserInterface | null;
    error: any | any;
    loaded: boolean;
    loading: boolean;
}

export interface UpdateUserPicStateInterface {
    userPic: UpdateEndUserProfileInterface | null;
    error: any | any;
    loaded: boolean;
    loading: boolean;
}

export interface UserState {
    changePasswordState: changePasswordStateInterface;
    updateUserProfileDataState: UpdateUserProfileDataStateInterface;
    updateUserPicStateInterface: UpdateUserPicStateInterface;
}

export const initialState: UserState = {
    changePasswordState: {
        changePassword: null,
        error: null,
        loaded: null,
        loading: null
     },
     updateUserProfileDataState: {
        userData: null,
        error: null,
        loaded: null,
        loading: null
     },
     updateUserPicStateInterface: {
        userPic: null,
        error: null,
        loaded: null,
        loading: null
     }
}

const _userReducer = createReducer(
    initialState,
    on(UserStateActions.CHANGE_PASSWORD, (state: UserState) => {
        const newState: UserState = {
            ...state,
            changePasswordState : {
                changePassword: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.CHANGE_PASSWORD_FAIL, (state: UserState, { error }) => {
        const newState: UserState = {
            ...state,
            changePasswordState : {
                changePassword: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.CHANGE_PASSWORD_SUCCESS, (state: UserState, { changePassRes }) => {
        const newState: UserState = {
            ...state,
            changePasswordState : {
                changePassword: changePassRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_DATA, (state: UserState) => {
        const newState: UserState = {
            ...state,
            updateUserProfileDataState : {
                userData: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_DATA_FAIL, (state: UserState, { error }) => {
        const newState: UserState = {
            ...state,
            updateUserProfileDataState : {
                userData: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_DATA_SUCCESS, (state: UserState, { updateEndUserDataRes }) => {
        const newState: UserState = {
            ...state,
            updateUserProfileDataState : {
                userData: updateEndUserDataRes[0],
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_PROFILE_PIC, (state: UserState) => {
        const newState: UserState = {
            ...state,
            updateUserPicStateInterface : {
                userPic: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_PROFILE_PIC_FAIL, (state: UserState, { error }) => {
        const newState: UserState = {
            ...state,
            updateUserPicStateInterface : {
                userPic: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(UserStateActions.UPDATE_USER_PROFILE_PIC_SUCCESS, (state: UserState, { updateEndUserProfileUpdateRes }) => {
        const newState: UserState = {
            ...state,
            updateUserPicStateInterface : {
                userPic: updateEndUserProfileUpdateRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    })
);

export function userReducer(
    state = initialState,
    action: any
): UserState {
    return _userReducer(state, action);
}
