import { createReducer, on } from '@ngrx/store';
import * as AppsStateActions from '../actions/application.action';
import { GetBannerInterface } from '../../interfaces/get-banner-interface';
import { GeolocationPositionInterface } from '../../interfaces/geolocation-interface';
import {
  GoogleGeolocationAPIResponseDataInterface,
  GoogleGeolocationAPIErrorsInterface,
} from '../../interfaces/google-geolocation-api-response-data-interface';
import { GetOffersInterface } from '../../interfaces/get-offers-interface';

const MOBILE_MAX_WIDTH = 800;
const TABLET_MAX_WIDTH = 1024;

export interface ScreenStateInterface {
  mobile: boolean;
  tablet: boolean;
  desktop: boolean;
}

export interface GetBannerStateInterface {
  bannerData: GetBannerInterface[] | null;
  error: any | null;
  loaded: boolean;
  loading: boolean;
}

export interface GeolocationStateInterface {
  geolocation: GeolocationPositionInterface | null;
  error: any | null;
  loaded: boolean;
  loading: boolean;
}

export interface GoogleGeolocationStateInterface {
  googleGeolocation: GoogleGeolocationAPIResponseDataInterface | null;
  error: GoogleGeolocationAPIErrorsInterface | any;
  loaded: boolean;
  loading: boolean;
}

export interface GetOffersStateInterface {
  offer: GetOffersInterface | null;
  error: any | null;
  loaded: boolean;
  loading: boolean;
}

export interface applicationState {
  sideNavBarState: boolean;
  selectedSideNavBarItem: string;
  ScreenState: ScreenStateInterface;
  settingSideNavBarState: boolean;
  selectedSettingSideNavBarItem: string;
  bannerState: GetBannerStateInterface;
  geolocationState: GeolocationStateInterface;
  googleGeolocationState: GoogleGeolocationStateInterface;
  getOffersState: GetOffersStateInterface;
}

export const initialState: applicationState = {
  sideNavBarState: false,
  selectedSideNavBarItem: 'home',
  ScreenState: {
    mobile: false,
    tablet: false,
    desktop: false,
  },
  settingSideNavBarState: true,
  selectedSettingSideNavBarItem: 'security',
  bannerState: {
    bannerData: null,
    error: null,
    loaded: false,
    loading: false,
  },
  geolocationState: {
    geolocation: null,
    error: null,
    loaded: false,
    loading: false,
  },
  googleGeolocationState: {
    googleGeolocation: null,
    error: null,
    loaded: false,
    loading: false,
  },
  getOffersState: {
    offer: null,
    error: null,
    loaded: false,
    loading: false,
  },
};

const _applicationReducer = createReducer(
  initialState,
  on(AppsStateActions.CLOSE_MAIN_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: false,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(AppsStateActions.OPEN_MAIN_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: true,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(AppsStateActions.TOGGLE_MAIN_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: !state.sideNavBarState,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(
    AppsStateActions.CHANGE_SELECTED_MAIN_SIDENAV_BAR_ITEM,
    (state: applicationState, { payload }) => {
      const newState: applicationState = {
        ...state,
        sideNavBarState: state.sideNavBarState,
        selectedSideNavBarItem: payload,
        ScreenState: state.ScreenState,
        settingSideNavBarState: state.settingSideNavBarState,
        selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
      };
      return newState;
    }
  ),
  on(AppsStateActions.SET_SCREEN, (state: applicationState, { width }) => {
    const mobile = width <= MOBILE_MAX_WIDTH;
    const tablet = width <= TABLET_MAX_WIDTH && width > MOBILE_MAX_WIDTH;

    const newState: applicationState = {
      ...state,
      sideNavBarState: state.sideNavBarState,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: {
        mobile,
        tablet,
        desktop: !mobile && !tablet,
      },
      settingSideNavBarState: state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(AppsStateActions.CLOSE_SETTING_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: state.sideNavBarState,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: false,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(AppsStateActions.OPEN_SETTING_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: state.sideNavBarState,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: !state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(AppsStateActions.TOGGLE_SETTING_SIDENAV_BAR, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      sideNavBarState: state.sideNavBarState,
      selectedSideNavBarItem: state.selectedSideNavBarItem,
      ScreenState: state.ScreenState,
      settingSideNavBarState: !state.settingSideNavBarState,
      selectedSettingSideNavBarItem: state.selectedSettingSideNavBarItem,
    };
    return newState;
  }),
  on(
    AppsStateActions.CHANGE_SELECTED_SETTING_SIDENAV_BAR_ITEM,
    (state: applicationState, { navBarSelect }) => {
      const newState: applicationState = {
        ...state,
        sideNavBarState: state.sideNavBarState,
        selectedSideNavBarItem: state.selectedSideNavBarItem,
        ScreenState: state.ScreenState,
        settingSideNavBarState: state.settingSideNavBarState,
        selectedSettingSideNavBarItem: navBarSelect,
      };
      return newState;
    }
  ),
  on(AppsStateActions.GET_BANNER, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      bannerState: {
        bannerData: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(AppsStateActions.GET_BANNER_FAIL, (state: applicationState, { error }) => {
    const newState: applicationState = {
      ...state,
      bannerState: {
        bannerData: null,
        error: error,
        loading: false,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AppsStateActions.GET_BANNER_SUCCESS,
    (state: applicationState, { GetBannerRes }) => {
      const newState: applicationState = {
        ...state,
        bannerState: {
          bannerData: GetBannerRes,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(AppsStateActions.GET_GEOLOCATION, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      geolocationState: {
        geolocation: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AppsStateActions.GET_GEOLOCATION_FAIL,
    (state: applicationState, { error }) => {
      const newState: applicationState = {
        ...state,
        geolocationState: {
          geolocation: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    AppsStateActions.GET_GEOLOCATION_SUCCESS,
    (state: applicationState, { GeolocationRes }) => {
      const newState: applicationState = {
        ...state,
        geolocationState: {
          geolocation: GeolocationRes,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(AppsStateActions.GET_GOOGLE_GEOLOCATION, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      googleGeolocationState: {
        googleGeolocation: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AppsStateActions.GET_GOOGLE_GEOLOCATION_FAIL,
    (state: applicationState, { error }) => {
      const newState: applicationState = {
        ...state,
        googleGeolocationState: {
          googleGeolocation: null,
          error: error,
          loading: false,
          loaded: false,
        },
      };
      return newState;
    }
  ),
  on(
    AppsStateActions.GET_GOOGLE_GEOLOCATION_SUCCESS,
    (state: applicationState, { GoogleGeolocationRes }) => {
      const newState: applicationState = {
        ...state,
        googleGeolocationState: {
          googleGeolocation: GoogleGeolocationRes,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  ),
  on(AppsStateActions.GET_OFFERS, (state: applicationState) => {
    const newState: applicationState = {
      ...state,
      getOffersState: {
        offer: null,
        error: null,
        loading: true,
        loaded: false,
      },
    };
    return newState;
  }),
  on(AppsStateActions.GET_OFFERS_FAIL, (state: applicationState, { error }) => {
    const newState: applicationState = {
      ...state,
      getOffersState: {
        offer: null,
        error: error,
        loading: false,
        loaded: false,
      },
    };
    return newState;
  }),
  on(
    AppsStateActions.GET_OFFERS_SUCCESS,
    (state: applicationState, { getOffersRes }) => {
      const newState: applicationState = {
        ...state,
        getOffersState: {
          offer: getOffersRes,
          error: null,
          loading: false,
          loaded: true,
        },
      };
      return newState;
    }
  )
);

export function applicationReducer(
  state = initialState,
  action: any
): applicationState {
  return _applicationReducer(state, action);
}
