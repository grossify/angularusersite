import { createReducer, on } from '@ngrx/store';
import * as AuthStateActions from '../actions/auth.actions';
import { SignInInterface } from '../../interfaces/sign-in-interface';

export interface AuthStateInterface {
  // is a user authenticated?
  isAuthenticated: boolean;
  // if authenticated, there should be a user object
  user: SignInInterface[] | null;
  // error message
  error: string | null;
  loaded: boolean;
  loading: boolean;
}

export const initialState: AuthStateInterface = {
    isAuthenticated: false,
    user: null,
    error: null,
    loaded: false,
    loading: false
};

const _authReducer = createReducer(
    initialState,
    on(AuthStateActions.LOGIN, (state: AuthStateInterface) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: false,
            error: null,
            user: null,
            loading: true,
            loaded: false
        }
        return newState;
    }),
    on(AuthStateActions.LOGIN_FAIL, (state: AuthStateInterface, { error }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: false,
            error: error,
            user: null,
            loading: false,
            loaded: false
        }
        return newState;
    }),
    on(AuthStateActions.LOGIN_SUCCESS, (state: AuthStateInterface, { userData }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: true,
            error: null,
            user: userData,
            loading: false,
            loaded: true
        }
        return newState;
    }),
    on(AuthStateActions.LOGIN_DATA_UPDATE, (state: AuthStateInterface, { userData }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: true,
            error: null,
            user: userData,
            loading: false,
            loaded: true
        }
        return newState;
    }),
    on(AuthStateActions.LOGIN_PROFILE_PIC_UPDATE, (state: AuthStateInterface, { updateEndUserProfileUpdateRes }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: true,
            error: null,
            user: [
                {
                    ...state.user[0],
                    userProfile: updateEndUserProfileUpdateRes.profileImage
                }
            ],
            loading: false,
            loaded: true
        }
        return newState;
    }),
    on(AuthStateActions.LOGOUT, (state: AuthStateInterface) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: false,
            error: null,
            user: null,
            loading: false,
            loaded: false
        }
        return newState;
    }),
    on(AuthStateActions.REGISTRATION, (state: AuthStateInterface) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: false,
            error: null,
            user: null,
            loading: true,
            loaded: false
        }
        return newState;
    }),
    on(AuthStateActions.REGISTRATION_FAIL, (state: AuthStateInterface, { error }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: false,
            error: error,
            user: null,
            loading: false,
            loaded: false
        }
        return newState;
    }),
    on(AuthStateActions.REGISTRATION_SUCCESS, (state: AuthStateInterface, { registrationData }) => {
        const newState: AuthStateInterface = {
            ...state,
            isAuthenticated: true,
            error: null,
            user: registrationData,
            loading: false,
            loaded: true
        }
        return newState;
    }),
    on(AuthStateActions.GET_END_USER_PROFILE, (state: AuthStateInterface) => {
        const newState: AuthStateInterface = {
            ...state,
        }
        return newState;
    }),
    on(AuthStateActions.GET_END_USER_PROFILE_FAIL, (state: AuthStateInterface, { error }) => {
        const newState: AuthStateInterface = {
            ...state,
        }
        return newState;
    }),
    on(AuthStateActions.GET_END_USER_PROFILE_SUCCESS, (state: AuthStateInterface, { GetUserProfileRes }) => {
        const newState: AuthStateInterface = {
            ...state,
            user: GetUserProfileRes,
        }
        return newState;
    })
);

export function authReducer(
    state = initialState,
    action: any
): AuthStateInterface {
    return _authReducer(state, action);
}
