import { createReducer, on } from '@ngrx/store';
import * as StoreStateActions from '../actions/store.action';
import { StoreCategoriesInterface } from '../../interfaces/store-categories-interface';
import { AddReferStoreInterface } from '../../interfaces/add-refer-store-interface';
import {
    GetNearByStoreInterface,
    GetNearByStoreItemInterface
} from '../../interfaces/get-near-by-store';
import {
    FevouriteStoresInterface,
    GetNearByFavouriteStoreItemInterface,
} from '../../interfaces/fevourite-stores-interface';
import { StoreDetailInterface } from '../../interfaces/store-detail-interface';
import { ProductCategoriesOfStoresInterface } from '../../interfaces/product-categories-of-stores-interface';
import { GetStoreProductsInterface } from '../../interfaces/get-store-products-interface';
import { AddFavouriteStoreInterface } from '../../interfaces/add-favourite-store-interface';
import { RemoveFavouriteStoreResInterface } from '../../interfaces/fevourite-stores-interface';
import { GetProductDetailsInterface } from '../../interfaces/get-product-details-interface';
import { GetStoreByCategoriesInterface } from '../../interfaces/get-store-by-categories-interface';
import {
    CityStoresInterface,
    GetCityStoresItemInterface
} from '../../interfaces/city-stores-interface';


export interface StoreCatStateInterface {
  storeCategories: {[catId: number]: StoreCategoriesInterface} | null;
  error: any | null;
  loaded: boolean;
  loading: boolean;
}

export interface PostReferStoreInterface {
    ReferStoreData: AddReferStoreInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}


export interface GetNearbyStoresStateInterface {
    Nearbystores: {[storeId: number]: GetNearByStoreItemInterface} | null;
    NearbystoresRes: GetNearByStoreInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface GetNearbyFavouriteStoresStateInterface {
    NearbyFavouriteStores: {[storeId: number]: GetNearByFavouriteStoreItemInterface} | {};
    NearbyFavouriteStoresRes: FevouriteStoresInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface GetStoreDetalsStateInterface {
    storeDetails: StoreDetailInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface getProductCategoriesStateInterface {
    productCategories: Array<ProductCategoriesOfStoresInterface> | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface limitAndOffsetStateInterface {
    limitAndOffset: {
        limit: number,
        offset: number,
        userId: number | null
    };
}

export interface getStoreProductStateInterface {
    storeProduct: GetStoreProductsInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface AddFavouriteStoreStateInterface {
    addFavourite: AddFavouriteStoreInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface RemoveFavouriteStoreStateInterface {
    removeFavourite: RemoveFavouriteStoreResInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}


export interface GetProductDetailsStateInterface {
    productDetail: GetProductDetailsInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface GetCityStoresStateInterface {
    StoresIntity: {[storeId: number]: GetCityStoresItemInterface};
    StoresRes: CityStoresInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface GetStoreByCategoriesStateInterface {
    stores: GetStoreByCategoriesInterface | null;
    error: any | null;
    loaded: boolean;
    loading: boolean;
}

export interface StoreStateInterface {
    StoreCatState: StoreCatStateInterface;
    ReferStoreState: PostReferStoreInterface;
    getNearbyStoresState: GetNearbyStoresStateInterface;
    getNearbyFavouriteStoresState: GetNearbyFavouriteStoresStateInterface;
    getStoreDetalsState: GetStoreDetalsStateInterface;
    getProductCategoriesState: getProductCategoriesStateInterface;
    limitAndOffsetState: limitAndOffsetStateInterface;
    getStoreProductState: getStoreProductStateInterface;
    addFavouriteStoreState: AddFavouriteStoreStateInterface;
    removeFavouriteStoreState: RemoveFavouriteStoreStateInterface;
    getProductDetailsState: GetProductDetailsStateInterface;
    getCityStoresState: GetCityStoresStateInterface;
    getStoreByCategoriesState: GetStoreByCategoriesStateInterface;
}

export const initialState: StoreStateInterface = {
    StoreCatState: {
        storeCategories: {},
        error: null,
        loaded: false,
        loading: false
    },
    ReferStoreState: {
        ReferStoreData: {},
        error: null,
        loaded: false,
        loading: false
    },
    getNearbyStoresState: {
        Nearbystores: {},
        NearbystoresRes: null,
        error: null,
        loaded: false,
        loading: false
    },
    getNearbyFavouriteStoresState: {
        NearbyFavouriteStores: {},
        NearbyFavouriteStoresRes: null,
        error: null,
        loaded: false,
        loading: false
    },
    getStoreDetalsState: {
        storeDetails: null,
        error: null,
        loaded: false,
        loading: false
    },
    getProductCategoriesState: {
        productCategories: null,
        error: null,
        loaded: false,
        loading: false
    },
    limitAndOffsetState: {
        limitAndOffset: {
            limit: 20,
            offset: 0,
            userId: null
        }
    },
    getStoreProductState: {
        storeProduct: null,
        error: null,
        loaded: false,
        loading: false
    },
    addFavouriteStoreState: {
        addFavourite: null,
        error: null,
        loaded: false,
        loading: false
    },
    removeFavouriteStoreState: {
        removeFavourite: null,
        error: null,
        loaded: false,
        loading: false
    },
    getProductDetailsState: {
        productDetail: null,
        error: null,
        loaded: false,
        loading: false
    },
    getCityStoresState: {
        StoresIntity: {},
        StoresRes: null,
        error: null,
        loaded: false,
        loading: false
    },
    getStoreByCategoriesState: {
        stores: null,
        error: null,
        loaded: false,
        loading: false
    }
};

const _storeReducer = createReducer(
    initialState,
    on(StoreStateActions.GET_STORE_CATEGORIES, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            StoreCatState : {
                storeCategories: {},
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_CATEGORIES_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            StoreCatState : {
                storeCategories: {},
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_CATEGORIES_SUCCESS, (state: StoreStateInterface, { storeCategoriesData }) => {
        const entities: {[catId: number]: StoreCategoriesInterface} = storeCategoriesData.reduce(
            (
                entities: {[catId: number]: StoreCategoriesInterface},
                storeCategoriesData : StoreCategoriesInterface
            ) => {
            return {
                ...entities,
                [storeCategoriesData.cat_id] : storeCategoriesData
            };
        }, {
            ...state.StoreCatState.storeCategories
        });
        const newState: StoreStateInterface = {
            ...state,
            StoreCatState : {
                storeCategories: entities,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.POST_REFER_STORE, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            ReferStoreState : {
                ReferStoreData: {},
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.POST_REFER_STORE_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            ReferStoreState : {
                ReferStoreData: {},
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.POST_REFER_STORE_SUCCESS, (state: StoreStateInterface, { ReferStoreRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            ReferStoreState : {
                ReferStoreData: ReferStoreRes[0],
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_STORES, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getNearbyStoresState: {
                Nearbystores: {},
                NearbystoresRes: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_STORES_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getNearbyStoresState : {
                Nearbystores: {},
                NearbystoresRes: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_STORES_SUCCESS, (state: StoreStateInterface, { nearbyStoreRes }) => {
        const entities: {[storeId: number]: GetNearByStoreItemInterface} = nearbyStoreRes.result.reduce(
            (
                entities: {[storeId: number]: GetNearByStoreItemInterface},
                nearByStoreData: GetNearByStoreItemInterface
            ) => {
            return {
                ...entities,
                [nearByStoreData.store_id] : nearByStoreData
            };
        }, {
            ...state.getNearbyStoresState.Nearbystores
        });
        const newState: StoreStateInterface = {
            ...state,
            getNearbyStoresState : {
                Nearbystores: entities,
                NearbystoresRes: nearbyStoreRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_FAVOURITE_STORES, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getNearbyFavouriteStoresState : {
                NearbyFavouriteStores: {},
                NearbyFavouriteStoresRes: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_FAVOURITE_STORES_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getNearbyFavouriteStoresState : {
                NearbyFavouriteStores: {},
                NearbyFavouriteStoresRes: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_NEARBY_FAVOURITE_STORES_SUCCESS, (state: StoreStateInterface, { nearbyFavouriteStoreRes }) => {
        const entities: {[storeId: number]: GetNearByFavouriteStoreItemInterface} = nearbyFavouriteStoreRes.result.reduce(
            (
                entities: {[storeId: number]: GetNearByFavouriteStoreItemInterface},
                nearByFavouriteStoreData: GetNearByFavouriteStoreItemInterface
            ) => {
            return {
                ...entities,
                [nearByFavouriteStoreData.store_id] : nearByFavouriteStoreData
            };
        }, {
            ...state.getNearbyFavouriteStoresState.NearbyFavouriteStores
        });
        const newState: StoreStateInterface = {
            ...state,
            getNearbyFavouriteStoresState : {
                NearbyFavouriteStores: entities,
                NearbyFavouriteStoresRes: nearbyFavouriteStoreRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_DETAIL, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreDetalsState : {
                storeDetails: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_DETAIL_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreDetalsState : {
                storeDetails: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_DETAIL_SUCCESS, (state: StoreStateInterface, { getStoreDetailRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreDetalsState : {
                storeDetails: getStoreDetailRes[0],
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductCategoriesState : {
                productCategories: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductCategoriesState : {
                productCategories: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_SUCCESS, (state: StoreStateInterface, { getProductCategoriesRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductCategoriesState : {
                productCategories: getProductCategoriesRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.SET_STORES_PRODUCTS_LIMIT_OFFSET, (state: StoreStateInterface, { limitAndOffset }) => {
        const newState: StoreStateInterface = {
            ...state,
            limitAndOffsetState: {
                limitAndOffset: {
                    limit: limitAndOffset.limit,
                    offset: limitAndOffset.offset,
                    userId: limitAndOffset.userId
                }
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORES_PRODUCTS, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreProductState: {
                storeProduct: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORES_PRODUCTS_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreProductState: {
                storeProduct: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORES_PRODUCTS_SUCCESS, (state: StoreStateInterface, { getStoreProductRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreProductState: {
                storeProduct: getStoreProductRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.ADD_FAVOURITE_STORE, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            addFavouriteStoreState : {
                addFavourite: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.ADD_FAVOURITE_STORE_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            addFavouriteStoreState : {
                addFavourite: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.ADD_FAVOURITE_STORE_SUCCESS, (state: StoreStateInterface, { addFavouriteRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            addFavouriteStoreState : {
                addFavourite: addFavouriteRes[0],
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.REMOVE_FAVOURITE_STORE, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            removeFavouriteStoreState: {
                removeFavourite: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.REMOVE_FAVOURITE_STORE_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            removeFavouriteStoreState: {
                removeFavourite: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.REMOVE_FAVOURITE_STORE_SUCCESS, (state: StoreStateInterface, { removeFavouriteRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            removeFavouriteStoreState: {
                removeFavourite: removeFavouriteRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_PRODUCT_DETAIL, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductDetailsState: {
                productDetail: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_PRODUCT_DETAIL_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductDetailsState: {
                productDetail: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_PRODUCT_DETAIL_SUCCESS, (state: StoreStateInterface, { getStoreProductRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            getProductDetailsState: {
                productDetail: getStoreProductRes[0],
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CITY, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getCityStoresState: {
                StoresIntity: {},
                StoresRes: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CITY_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getCityStoresState : {
                StoresIntity: {},
                StoresRes: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CITY_SUCCESS, (state: StoreStateInterface, { getCityStoresRes }) => {
        const entities: {[storeId: number]: GetCityStoresItemInterface} = getCityStoresRes.result.reduce(
            (
                entities: {[storeId: number]: GetCityStoresItemInterface},
                getCityStoresResData : GetCityStoresItemInterface
            ) => {
            return {
                ...entities,
                [getCityStoresResData.store_id] : getCityStoresResData
            };
        }, {
            ...state.getCityStoresState.StoresRes
        });
        const newState: StoreStateInterface = {
            ...state,
            getCityStoresState : {
                StoresIntity: entities,
                StoresRes: getCityStoresRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CATEGORIES, (state: StoreStateInterface) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreByCategoriesState: {
                stores: null,
                error: null,
                loading: true,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CATEGORIES_FAIL, (state: StoreStateInterface, { error }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreByCategoriesState: {
                stores: null,
                error: error,
                loading: false,
                loaded: false
            }
        }
        return newState;
    }),
    on(StoreStateActions.GET_STORE_BY_CATEGORIES_SUCCESS, (state: StoreStateInterface, { getStoreRes }) => {
        const newState: StoreStateInterface = {
            ...state,
            getStoreByCategoriesState: {
                stores: getStoreRes,
                error: null,
                loading: false,
                loaded: true
            }
        }
        return newState;
    })
);

export function storeReducer(
    state = initialState,
    action: any
): StoreStateInterface {
    return _storeReducer(state, action);
}
