import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as UserStateActions from '../actions/user.action';
import * as AuthStateActions from '../actions/auth.actions';
import { UserService } from '../../services/user.service';
import {
    ChangePasswordResponseErrorInterface
} from '../../interfaces/change-password-interface';

import { Store } from '@ngrx/store';
import { AuthStateInterface } from '../../stores/reducers/auth.reducers';


@Injectable()
export class UserEffects {

  changePassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserStateActions.CHANGE_PASSWORD),
      exhaustMap((action) => this.userservice.changePasswordByUserId(action.passData)
        .pipe(
          map((changePassData) => UserStateActions.CHANGE_PASSWORD_SUCCESS({changePassRes: changePassData})
          ),
          catchError((error) => of(UserStateActions.CHANGE_PASSWORD_FAIL(error))),
          tap((errorOrData) => {
            console.log(errorOrData);
            if(errorOrData && errorOrData['changePassRes'] && errorOrData['changePassRes']['status']) {
              if(errorOrData['changePassRes']['message'].toLowerCase().indexOf('successful') > -1) {
                this.toastr.success(
                  'Password Change successfully',
                  'Success',
                  {
                    timeOut: 5000,
                    closeButton: true
                  }
                );
              }
            }

            if(errorOrData && errorOrData['error'] && errorOrData['error']['data']) {

              if(errorOrData['error']['data']['msg'].indexOf('password does not match') > -1) {
                this.toastr.error(
                  'Password is Incorrect',
                  'Error',
                  {
                    timeOut: 5000,
                    closeButton: true
                  }
                );
              }
            }
          })
        )
      )
    )
  );


  updateUserData2$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserStateActions.UPDATE_USER_DATA),
      exhaustMap((action) => this.userservice.UpdateEndUser(action.reqData)
        .pipe(
          map((updateData) => UserStateActions.UPDATE_USER_DATA_SUCCESS({updateEndUserDataRes: updateData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
            if (errorOrData.updateEndUserDataRes[0].UserID) {
              this.store.dispatch(AuthStateActions.LOGIN_DATA_UPDATE({userData: errorOrData.updateEndUserDataRes}));
              this.toastr.success('User Data Updated !', 'Success', {
                timeOut: 3000
              });
            } else {
              this.toastr.error('User Data Not Updated !', 'Error', {
                timeOut: 3000
              });
            }
          }),
          catchError((error) => of(UserStateActions.UPDATE_USER_DATA_FAIL(error)))
        )
      )
    )
  );


  // updateUserData$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(UserStateActions.UPDATE_USER_DATA),
  //     exhaustMap((action) => this.userservice.UpdateEndUser(action.reqData).pipe(
  //       exhaustMap((updateData) => [
  //         UserStateActions.UPDATE_USER_DATA_SUCCESS({updateEndUserDataRes: updateData}),
  //         AuthStateActions.LOGIN_DATA_UPDATE({userData: updateData})
  //     ]),
  //       catchError((error) => of(UserStateActions.UPDATE_USER_DATA_FAIL(error)))
  //     )),
  //   tap((errorOrData) => {
  //     console.log(errorOrData);
  //     if (errorOrData['updateEndUserDataRes'] && errorOrData['updateEndUserDataRes'][0] && errorOrData['updateEndUserDataRes'][0].UserID) {
  //       this.toastr.success('User Data Updated !', 'Success', {
  //         timeOut: 3000
  //       });
  //     } else {
  //       this.toastr.error('User Data Not Updated !', 'Error', {
  //         timeOut: 3000
  //       });
  //     }
  //   })
  //   )
  // );


  updateUserPic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserStateActions.UPDATE_USER_PROFILE_PIC),
      exhaustMap(action => this.userservice.UpdateEndUserProfile(action.reqData)
        .pipe(
          map(updateData => UserStateActions.UPDATE_USER_PROFILE_PIC_SUCCESS({updateEndUserProfileUpdateRes: updateData})),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (dataOrError.updateEndUserProfileUpdateRes && dataOrError.updateEndUserProfileUpdateRes.status) {
              this.store.dispatch(AuthStateActions.LOGIN_PROFILE_PIC_UPDATE({updateEndUserProfileUpdateRes: dataOrError.updateEndUserProfileUpdateRes}));
              // AuthStateActions.LOGIN_PROFILE_PIC_UPDATE({updateEndUserProfileUpdateRes: dataOrError.updateEndUserProfileUpdateRes})

              this.toastr.success('User Image Saved !', 'Success', {
                timeOut: 5000,
                closeButton: true
              });
            } else {
              this.toastr.error('User Image Not Saved !', 'Error', {
                timeOut: 3000,
                closeButton: true
              });
            }
          })
        )
      ),
      catchError(error => of(UserStateActions.UPDATE_USER_PROFILE_PIC_FAIL(error))),
      tap((error) => {
        // console.log(error['error']);
        if (error['error'] && error['error'] && !error['error']['status']) {
          this.toastr.error('User Image Not Saved !', 'Error', {
            timeOut: 3000,
            closeButton: true
          });
        } else {
          // console.log(error['error']);
        }
      })
    )
  );


  constructor(
    private toastr: ToastrService,
    private actions$: Actions,
    private userservice: UserService,
    private store: Store<{
      authState: AuthStateInterface
    }>
  ) {}

}
