import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as addressStateActions from '../actions/address.action';
import { UserService } from '../../services/user.service';

@Injectable()
export class AddressEffects {

  getUserAddress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addressStateActions.GET_USER_ADDRESS),
      exhaustMap((action) => this.userservice.getUserAddress(action.userId)
        .pipe(
          map(userAddressData => addressStateActions.GET_USER_ADDRESS_SUCCESS({GetUserAddressRes: userAddressData})
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            // if (dataOrError.PostUserAddressRes[0].address_id) {
            //   this.toastr.success('User Address Updated !', 'Success', {
            //     timeOut: 5000,
            //     closeButton: true
            //   });
            // } else {
            //   this.toastr.error('User Address Not Updated !', 'Error', {
            //     timeOut: 5000,
            //     closeButton: true
            //   });
            // }
          }),
          catchError(error => of(addressStateActions.GET_USER_ADDRESS_FAIL(error)))
        )
      )
    )
  );

  postUserAddress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addressStateActions.POST_USER_ADDRESS),
      exhaustMap((action) => this.userservice.userAddress(action.userAddressInputData)
        .pipe(
          map(userAddressData => addressStateActions.POST_USER_ADDRESS_SUCCESS({PostUserAddressRes: userAddressData})
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (dataOrError.PostUserAddressRes[0].address_id) {
              this.toastr.success('User Address Updated !', 'Success', {
                timeOut: 5000,
                closeButton: true
              });
            } else {
              this.toastr.error('User Address Not Updated !', 'Error', {
                timeOut: 5000,
                closeButton: true
              });
            }
          }),
          catchError(error => of(addressStateActions.POST_USER_ADDRESS_FAIL(error))),
          tap((dataOrError) => {
            // console.log(dataOrError);
            // if (dataOrError['PostUserAddressRes'][0].address_id) {
            //   this.toastr.success('User Data Updated !', 'Success', {
            //     timeOut: 5000,
            //     closeButton: true
            //   });
            // } else {
            //   this.toastr.error('User Data Not Updated !', 'Error', {
            //     timeOut: 5000,
            //     closeButton: true
            //   });
            // }
          })
        )
      )
    )
  );

  deletUserAddress$ = createEffect(() =>
      this.actions$.pipe(
      ofType(addressStateActions.DELETE_USER_ADDRESS),
      tap((data) => {})
    ),
    { dispatch: false }
  );

  constructor(
    private toastr: ToastrService,
    private actions$: Actions,
    private userservice: UserService
  ) {}

}
