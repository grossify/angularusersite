import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as LatLngCityActions from '../actions/latLngCity.action';
import { ApplicationService } from '../../services/application.service';
import { GoogleAPIService } from '../../services/google-api.service';
import { AddressStateInterface } from '../reducers/latLngCity.reducer';
import { Store } from '@ngrx/store';

// declare var google: any;

@Injectable()
export class LatLngCityEffects {
  public getgeoLoaction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LatLngCityActions.GET_LAT_LNG_CITY),
      exhaustMap((action) =>
        this.applicationService.getGeolocation().pipe(
          map((geoCodeData) =>
            LatLngCityActions.SUCCESS_GET_LAT_LNG_CITY({
              latitude: geoCodeData.coords.latitude,
              longitude: geoCodeData.coords.longitude,
              city: null,
            })
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (dataOrError.latitude && dataOrError.longitude) {
              // get city name by google reverse geocode

              const Latlng: google.maps.LatLngLiteral = {
                lat: dataOrError.latitude,
                lng: dataOrError.longitude,
              };
              this.store.dispatch(
                LatLngCityActions.GET_REVERSE_GEOCODE_ADDRESS({
                  latLng: Latlng,
                })
              );
            } else {
              // call google geocoding api for lat lang
              // this.store.dispatch(LatLngCityActions.GET_GOOGLE_GEOLOCATION());
            }
          }),
          catchError((error) =>
            of(LatLngCityActions.FAIL_GET_LAT_LNG_CITY(error))
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (dataOrError['latitude'] && dataOrError['longitude']) {
              // get city name by google reverse geocode
            } else {
              // call google geocoding api for lat lang
              this.store.dispatch(LatLngCityActions.GET_GOOGLE_GEOLOCATION());
            }
          })
        )
      )
    )
  );

  public googleGeolocation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LatLngCityActions.GET_GOOGLE_GEOLOCATION),
      exhaustMap(() =>
        this.googleapiservice.getGeolocationData().pipe(
          map((googleGeolocationData) =>
            LatLngCityActions.GET_GOOGLE_GEOLOCATION_SUCCESS({
              GoogleGeolocationRes: googleGeolocationData,
            })
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (
              dataOrError.GoogleGeolocationRes &&
              dataOrError.GoogleGeolocationRes.location &&
              dataOrError.GoogleGeolocationRes.location.lat &&
              dataOrError.GoogleGeolocationRes.location.lng
            ) {
              // get city name by google reverse geocode
              const Latlng: google.maps.LatLngLiteral = {
                lat: dataOrError.GoogleGeolocationRes.location.lat,
                lng: dataOrError.GoogleGeolocationRes.location.lng,
              };
              this.store.dispatch(
                LatLngCityActions.GET_REVERSE_GEOCODE_ADDRESS({
                  latLng: Latlng,
                })
              );
            } else {
              // call google geocoding api for lat lang
              // this.store.dispatch(LatLngCityActions.GET_GOOGLE_GEOLOCATION());
            }
          }),
          catchError((error) =>
            of(LatLngCityActions.GET_GOOGLE_GEOLOCATION_FAIL(error))
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (
              dataOrError['GoogleGeolocationRes'] &&
              dataOrError['GoogleGeolocationRes']['location'] &&
              dataOrError['GoogleGeolocationRes']['location']['lat'] &&
              dataOrError['GoogleGeolocationRes']['location']['lng']
            ) {
              // get city name by google reverse geocode
            } else {
              // set default lat lng and city
              this.store.dispatch(LatLngCityActions.SET_DEFAULT_LAT_LNG_CITY())
            }
          })
        )
      )
    )
  );

  public googleReverseGeocoding$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LatLngCityActions.GET_REVERSE_GEOCODE_ADDRESS),
      exhaustMap((action) =>
        this.googleapiservice.geocode(action.latLng).pipe(
          map((googleRevGeoCodeData) =>
            LatLngCityActions.SUCCESS_REVERSE_GEOCODE_ADDRESS({
              results: googleRevGeoCodeData,
            })
          ),
          tap((dataOrError) => {
            // console.log(dataOrError);
            if (dataOrError) {
              // get city name by google reverse geocode
              if (dataOrError.results) {
                this.getAddressComponents(dataOrError.results);
              }
            } else {
              // call google geocoding api for lat lang
              // this.store.dispatch(LatLngCityActions.GET_GOOGLE_GEOLOCATION());
            }
          }),
          catchError((error) =>
            of(LatLngCityActions.FAIL_REVERSE_GEOCODE_ADDRESS(error))
          ),
          tap((dataOrError) => {
            //console.log(dataOrError);
            if (dataOrError['results']) {
              // get city name by google reverse geocode
            } else {
              // call any fallback  geocoding api for lat lang
            }
          })
        )
      )
    )
  );

  private getAddressComponents(results: google.maps.GeocoderResult[]): void {
    let reqData: AddressStateInterface = {};
    reqData.formatted_address = results[0].formatted_address;

    // get country
    if (results[0] && results[0].address_components) {
      results.forEach((firstValue) => {
        firstValue.address_components.forEach((secondValue) => {
          secondValue.types.forEach((element) => {
            if (element === 'country') {
              reqData.country = secondValue.long_name;
            }
            if (element === 'administrative_area_level_1') {
              reqData.state = secondValue.long_name;
            }
            if (element === 'administrative_area_level_2') {
              reqData.city = secondValue.long_name;
            }
            if (!reqData.city && element === 'locality') {
              reqData.city = secondValue.long_name;
            }
            if (element === 'postal_code') {
              reqData.postalCode = secondValue.long_name;
            }
          });
        });
      });
    }

    this.store.dispatch(
      LatLngCityActions.UPDATE_REVERSE_GEOCODE_ADDRESS({ reqData: reqData })
    );
  }

  // private getAddressComponents(
  //   results: any,
  //   request: { location: { lat: any; lng: any } }
  // ): void {
  //   let reqData: AddressStateInterface;
  //   reqData.latitude = request.location.lat;
  //   reqData.longitude = request.location.lng;

  //   // get country
  //   if (results[0] && results[0].address_components) {
  //     results.forEach((firstValue) => {
  //       firstValue.address_components.forEach((secondValue) => {
  //         secondValue.types.forEach((element) => {
  //           if (element === 'country') {
  //             reqData.country = secondValue.long_name;
  //           }
  //           if (element === 'administrative_area_level_1') {
  //             reqData.state = secondValue.long_name;
  //           }
  //           if (element === 'administrative_area_level_2') {
  //             reqData.city = secondValue.long_name;
  //           }
  //           if (!reqData.city && element === 'locality') {
  //             reqData.city = secondValue.long_name;
  //           }
  //           if (element === 'postal_code') {
  //             reqData.postalCode = secondValue.long_name;
  //           }
  //         });
  //       });
  //     });
  //   }

  //   if (results.address_components) {
  //     results.address_components.forEach((firstValue) => {
  //       firstValue.types.forEach((element) => {
  //         if (element === 'country') {
  //           reqData.country = firstValue.long_name;
  //         }
  //         if (element === 'administrative_area_level_1') {
  //           reqData.state = firstValue.long_name;
  //         }
  //         if (element === 'administrative_area_level_2') {
  //           reqData.city = firstValue.long_name;
  //         }
  //         if (!reqData.city && element === 'locality') {
  //           reqData.city = firstValue.long_name;
  //         }
  //         if (element === 'postal_code') {
  //           reqData.postalCode = firstValue.long_name;
  //         }
  //       });
  //     });
  //   }

  //   if (results.formatted_address) {
  //     reqData.formatted_address = results.formatted_address;
  //   } else {
  //     reqData.formatted_address = results[0].formatted_address;
  //   }

  //   this.store.dispatch(
  //     LatLngCityActions.UPDATE_REVERSE_GEOCODE_ADDRESS({ reqData: reqData })
  //   );
  // }

  // private googleReverseGeocode(lat: number, lng: number): void {
  //   // get lat lng to address.
  //   const geocoder = new google.maps.Geocoder();
  //   const latlng = new google.maps.LatLng(lat, lng);
  //   const request = {
  //     location: latlng,
  //   };

  //   geocoder.geocode(request, (results, status) => {
  //     if (status.toLocaleLowerCase() === 'ok') {
  //       this.getAddressComponents(results, request);
  //     } else {
  //       console.log(
  //         'Geocode was not successful for the following reason: ' + status
  //       );
  //     }
  //   });
  // }

  constructor(
    private actions$: Actions,
    private applicationService: ApplicationService,
    private googleapiservice: GoogleAPIService,
    private store: Store<{}>
  ) {}
}
