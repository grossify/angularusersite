import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as AppsStateActions from '../actions/application.action';
import { ApplicationService } from '../../services/application.service';
import { GoogleAPIService } from '../../services/google-api.service';

@Injectable()
export class ApplicationEffects {

  getBanner$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsStateActions.GET_BANNER),
      exhaustMap(() => this.applicationservice.getBanner()
        .pipe(
          map(bannerData => AppsStateActions.GET_BANNER_SUCCESS({GetBannerRes: bannerData})
          ),
          catchError(error => of(AppsStateActions.GET_BANNER_FAIL(error)))
        )
      )
    )
  );

  getGeolocation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsStateActions.GET_GEOLOCATION),
      exhaustMap(() => this.applicationservice.getGeolocation()
        .pipe(
          map(geolocationData => AppsStateActions.GET_GEOLOCATION_SUCCESS({GeolocationRes: geolocationData})
          ),
          catchError(error => of(AppsStateActions.GET_GEOLOCATION_FAIL(error)))
        )
      )
    )
  );

  getGoogleGeolocation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsStateActions.GET_GOOGLE_GEOLOCATION),
      exhaustMap(() => this.googleapiservice.getGeolocationData()
        .pipe(
          map(googleGeolocationData => AppsStateActions.GET_GOOGLE_GEOLOCATION_SUCCESS({GoogleGeolocationRes: googleGeolocationData})
          ),
          catchError(error => of(AppsStateActions.GET_GOOGLE_GEOLOCATION_FAIL(error)))
        )
      )
    )
  );

  getOffers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppsStateActions.GET_OFFERS),
      exhaustMap(() => this.applicationservice.getOffers()
        .pipe(
          map(offerData => AppsStateActions.GET_OFFERS_SUCCESS({getOffersRes: offerData[0]})
          ),
          catchError(error => of(AppsStateActions.GET_OFFERS_FAIL(error)))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private applicationservice: ApplicationService,
    private googleapiservice: GoogleAPIService
  ) {}

}
