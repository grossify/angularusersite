import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StoreProductsReqInterface } from '../../interfaces/get-store-products-interface';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap, withLatestFrom, concatMap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as StoreStateActions from '../actions/store.action';
import { StoreService } from '../../services/store.service';
import { Store, select } from '@ngrx/store';
import * as StoreStateRed from '../../stores/reducers/store.reducer';

@Injectable()
export class StoreEffects {

  getCat$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_STORE_CATEGORIES),
      exhaustMap((action) => this.storeservice.getStoreCategories(action.userId)
        .pipe(
          map(storeCatData => StoreStateActions.GET_STORE_CATEGORIES_SUCCESS({storeCategoriesData: storeCatData})
          ),
          catchError(error => of(StoreStateActions.GET_STORE_CATEGORIES_FAIL(error)))
        )
      )
    )
  );


  // getCat$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(StoreStateActions.GET_STORE_CATEGORIES),
  //     concatMap(action =>  
  //       of(action).pipe(
  //         withLatestFrom(this.store.pipe(select('store')))
  //       )
  //     ),
  //     map(([action, store]) => {
  //       return {action: action, limitAndOffset: store.limitAndOffsetState}
  //     }),
  //     exhaustMap(action2 =>
  //       this.storeservice.getStoreCategories(action2.action.userId)
  //         .pipe(
  //           map(storeCatData => StoreStateActions.GET_STORE_CATEGORIES_SUCCESS({storeCategoriesData: storeCatData})
  //           ),
  //           catchError(error => of(StoreStateActions.GET_STORE_CATEGORIES_FAIL(error)))
  //         )
  //     )
  //   )
  // );


  // getCat$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(StoreStateActions.GET_STORE_CATEGORIES),
  //     exhaustMap(action =>
  //       this.storeservice.getStoreCategories(action.userId)
  //         .pipe(
  //           map(storeCatData => StoreStateActions.GET_STORE_CATEGORIES_SUCCESS({storeCategoriesData: storeCatData})
  //           ),
  //           catchError(error => of(StoreStateActions.GET_STORE_CATEGORIES_FAIL(error)))
  //         )
  //     ),
  //     concatMap(action =>
  //       of(action).pipe(
  //         withLatestFrom(this.store.pipe(select('store')))
  //       )
  //     ),
  //     map(([action, store]) => {
  //       return {action: action, limitAndOffset: store.limitAndOffsetState}
  //     }),
  //     exhaustMap(action =>
  //       this.storeservice.GetStoreProducts({
  //         StoreId: action.action['storeCategoriesData'][0].store_id,
  //         CatId: action.action['storeCategoriesData'][0].cat_id,
  //         EndUserId: action.limitAndOffset.limitAndOffset.userId,
  //         limit: action.limitAndOffset.limitAndOffset.limit,
  //         offset: action.limitAndOffset.limitAndOffset.offset,
  //       })
  //         .pipe(
  //           map(storeProductData => StoreStateActions.GET_STORES_PRODUCTS_SUCCESS({getStoreProductRes: storeProductData})
  //           ),
  //           catchError(error => of(StoreStateActions.GET_STORES_PRODUCTS_FAIL({error: error})))
  //         )
  //     )
  //   )
  // );


  addReferStore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.POST_REFER_STORE),
      exhaustMap((action) => this.storeservice.addReferStore(action.AddReferStoreData)
        .pipe(
          map(
            referStoreData => StoreStateActions.POST_REFER_STORE_SUCCESS({ReferStoreRes: referStoreData})
          ),
          tap((dataOrError) => {
            if (
              dataOrError.ReferStoreRes &&
              dataOrError.ReferStoreRes[0]  &&
              dataOrError.ReferStoreRes[0].ref_store_id
              ) {
              this.toastr.success(
                'Thanks for refering store, we will soon contact your referred store.',
                'Thank You!',
                {
                  timeOut: 5000,
                  closeButton: true
                }
              );
              this.router.navigate(['/']);
            } else {
              this.toastr.error('Please try again later.', 'Some Error occured!', {
                timeOut: 3000,
                closeButton: true
              });
            }
          }),
          catchError(error => of(StoreStateActions.POST_REFER_STORE_FAIL(error)))
        )
      )
    )
  );

  getNearbyStores$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_NEARBY_STORES),
      exhaustMap((action) => this.storeservice.getNearByStores(action.nearbyStoreData)
        .pipe(
          map(
            nearbyData => StoreStateActions.GET_NEARBY_STORES_SUCCESS({nearbyStoreRes: nearbyData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_NEARBY_STORES_FAIL({error: error})))
        )
      )
    )
  );


  getNearbyFavouriteStores$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_NEARBY_FAVOURITE_STORES),
      exhaustMap((action) => this.storeservice.getNearByFavoriteStores(action.nearbyFavouriteStoreData)
        .pipe(
          map(nearbyFavData => StoreStateActions.GET_NEARBY_FAVOURITE_STORES_SUCCESS({nearbyFavouriteStoreRes: nearbyFavData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_NEARBY_FAVOURITE_STORES_FAIL({error: error}))),
          tap((errorOrData) => {
            // console.log(errorOrData);
          })
        )
      )
    )
  );


  getStoreDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_STORE_DETAIL),
      exhaustMap((action) => this.storeservice.getStoreDetail(action.getStoreDetail)
        .pipe(
          map(storeDetailsData => StoreStateActions.GET_STORE_DETAIL_SUCCESS({getStoreDetailRes: storeDetailsData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_STORE_DETAIL_FAIL({error: error})))
        )
      )
    )
  );

  // getProductCatOfStore$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES),
  //     exhaustMap((action) => this.storeservice.getProductCategoriesOfStores(action.storeId)
  //       .pipe(
  //         map(productCatData => StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_SUCCESS({getProductCategoriesRes: productCatData})
  //         ),
  //         tap((errorOrData) => {
  //           // console.log(errorOrData);
  //         }),
  //         catchError(error => of(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_FAIL({error: error})))
  //       )
  //     )
  //   )
  // );


  // getProductCatOfStore$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES),
  //     exhaustMap(action =>
  //       this.storeservice.getProductCategoriesOfStores(action.storeId)
  //       .pipe(
  //         map(productCatData => StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_SUCCESS({getProductCategoriesRes: productCatData})
  //         ),
  //         tap((errorOrData) => {
  //           console.log(errorOrData);
  //         }),
  //         catchError(error => of(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_FAIL({error: error})))
  //       )
  //     ),
  //     concatMap(action =>
  //       of(action).pipe(
  //         withLatestFrom(this.store.pipe(select('store')))
  //       )
  //     ),
  //     map(([action, store]) => {
  //       // console.log(action['getProductCategoriesRes'][0]);
  //       return {action: action, limitAndOffset: store.limitAndOffsetState}
  //     }),
  //     exhaustMap(action =>
  //       this.storeservice.GetStoreProducts({
  //         StoreId: action.action['getProductCategoriesRes'][0].store_id,
  //         CatId: action.action['getProductCategoriesRes'][0].cat_id,
  //         EndUserId: action.limitAndOffset.limitAndOffset.userId,
  //         limit: action.limitAndOffset.limitAndOffset.limit,
  //         offset: action.limitAndOffset.limitAndOffset.offset,
  //       })
  //       .pipe(
  //         map(storeProductData => StoreStateActions.GET_STORES_PRODUCTS_SUCCESS({getStoreProductRes: storeProductData})
  //         ),
  //         tap((errorOrData) => {
  //           console.log(errorOrData);
  //         }),
  //         catchError(error => of(StoreStateActions.GET_STORES_PRODUCTS_FAIL({error: error})))
  //       )
  //     )
  //   )
  // );


  getProductCatOfStore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES),
      concatMap(action =>
        of(action).pipe(
          withLatestFrom(this.store.pipe(select('store')))
        )
      ),
      map(([action, store]) => {
        // console.log(action['getProductCategoriesRes'][0]);
        return {action: action, limitAndOffset: store.limitAndOffsetState}
      }),
      exhaustMap(action =>
        this.storeservice.getProductCategoriesOfStores(action.action.storeId)
        .pipe(
          map(productCatData => StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_SUCCESS({getProductCategoriesRes: productCatData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
            const reqData: StoreProductsReqInterface = {
              StoreId: errorOrData['getProductCategoriesRes'][0].store_id,
              CatId: errorOrData['getProductCategoriesRes'][0].cat_id,
              EndUserId: action.limitAndOffset.limitAndOffset.userId,
              limit: action.limitAndOffset.limitAndOffset.limit,
              offset: action.limitAndOffset.limitAndOffset.offset,
            }
            this.store.dispatch(StoreStateActions.GET_STORES_PRODUCTS({storeProductReq: reqData}))
          }),
          catchError(error => of(StoreStateActions.GET_PRODUCT_CATEGORIES_OF_STORES_FAIL({error: error})))
        )
      )
    )
  );




  getStoreProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_STORES_PRODUCTS),
      exhaustMap((action) => this.storeservice.GetStoreProducts(action.storeProductReq)
        .pipe(
          map(storeProductData => StoreStateActions.GET_STORES_PRODUCTS_SUCCESS({getStoreProductRes: storeProductData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_STORES_PRODUCTS_FAIL({error: error})))
        )
      )
    )
  );


  // getStoreProducts$  = createEffect(
  //   () =>
  //     this.actions$.pipe(
  //       ofType(StoreStateActions.GET_STORES_PRODUCTS),
  //       concatMap(action => of(action).pipe(
  //         withLatestFrom(this.store.pipe(select('store')))
  //       )),
  //       tap(([action, store]) => {
  //         this.storeservice.GetStoreProducts(action.storeProductReq)
  //         .pipe(
  //           map(storeProductData => StoreStateActions.GET_STORES_PRODUCTS_SUCCESS({getStoreProductRes: storeProductData})
  //           ),
  //           tap((errorOrData) => {
  //             // console.log(errorOrData);
  //           }),
  //           catchError(error => of(StoreStateActions.GET_STORES_PRODUCTS_FAIL({error: error}))),
  //           tap((errorOrData) => {
  //             // console.log(errorOrData);
  //           })
  //         )
      
  //       })
  //     ),
  //   { dispatch: false }
  // );



  removeFavStore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.REMOVE_FAVOURITE_STORE),
      exhaustMap((action) => this.storeservice.removeFavouriteStore(action.removeFavouriteReq)
        .pipe(
          map(removeFavData => StoreStateActions.REMOVE_FAVOURITE_STORE_SUCCESS({removeFavouriteRes: removeFavData})
          ),
          tap((errorOrData) => {
            console.log(errorOrData);
            if (errorOrData.removeFavouriteRes && errorOrData.removeFavouriteRes.status) {
              this.toastr.success(
                'Store removed from your favourate store list.',
                'Success',
                {
                  timeOut: 5000,
                  closeButton: true
                }
              );
            } else {
              this.toastr.error(
                'Store does Not removed from your favourate store list.',
                'Some Error Occoured',
                {
                  timeOut: 5000,
                  closeButton: true
                }
              );
            }
          }),
          catchError(error => of(StoreStateActions.REMOVE_FAVOURITE_STORE_FAIL({error: error})))
        )
      )
    )
  );


  addFavStore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.ADD_FAVOURITE_STORE),
      exhaustMap((action) => this.storeservice.addFavouriteStore(action.addFavouriteReq)
        .pipe(
          map(addFavData => StoreStateActions.ADD_FAVOURITE_STORE_SUCCESS({addFavouriteRes: addFavData})
          ),
          tap((errorOrData) => {
            console.log(errorOrData);
            if(errorOrData.addFavouriteRes && errorOrData.addFavouriteRes[0] && errorOrData.addFavouriteRes[0].store_id) {
              this.toastr.success(
                'Store added to your favourate store list.',
                'Success',
                {
                  timeOut: 5000,
                  closeButton: true
                }
              );
            } else {
              this.toastr.error(
                'Store does Not added to your favourate store list.',
                'Some Error Occoured',
                {
                  timeOut: 5000,
                  closeButton: true
                }
              );
            }
          }),
          catchError(error => of(StoreStateActions.ADD_FAVOURITE_STORE_FAIL({error: error})))
        )
      )
    )
  );


  getStoreProductDetail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_STORE_PRODUCT_DETAIL),
      exhaustMap((action) => this.storeservice.getProductDetails(action.storeProductId)
        .pipe(
          map(storeProductDetailData => StoreStateActions.GET_STORE_PRODUCT_DETAIL_SUCCESS({getStoreProductRes: storeProductDetailData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_STORE_PRODUCT_DETAIL_FAIL({error: error})))
        )
      )
    )
  );


  getStorebycity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StoreStateActions.GET_STORE_BY_CITY),
      exhaustMap((action) => this.storeservice.getStoreByCity(action.reqData)
        .pipe(
          map(storeByCityData => StoreStateActions.GET_STORE_BY_CITY_SUCCESS({getCityStoresRes: storeByCityData})
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError(error => of(StoreStateActions.GET_STORE_BY_CITY_FAIL({error: error})))
        )
      )
    )
  );


  getStoreCatBynearyStores$ = createEffect(() =>
  this.actions$.pipe(
    ofType(StoreStateActions.GET_STORE_BY_CATEGORIES),
    exhaustMap((action) => this.storeservice.getStoreByCategories(action.getStoreByCatData)
      .pipe(
        map(nearByCatStoreData => StoreStateActions.GET_STORE_BY_CATEGORIES_SUCCESS({getStoreRes: nearByCatStoreData})
        ),
        tap((errorOrData) => {
            // console.log(errorOrData);
        }),
        catchError(error => of(StoreStateActions.GET_STORE_BY_CATEGORIES_FAIL(error)))
      )
    )
  )
);


  constructor(
    private toastr: ToastrService,
    private router: Router,
    private actions$: Actions,
    private storeservice: StoreService,
    private store: Store<{
      store: StoreStateRed.StoreStateInterface
    }>
  ) {}

}
