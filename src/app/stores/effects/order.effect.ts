import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { of, Subject } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as orderActions from '../actions/order.action';
import { OrderService } from '../../services/order.service';
import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../../stores/reducers/auth.reducers';
import { DELETE_ALL_CART } from '../../stores/actions/cart.action';
import { UserInterface } from '../../interfaces/user-interface';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class OrderEffects {
  public Userdata: UserInterface;
  private ngUnsubscribe = new Subject();

  getOrders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(orderActions.GET_ALL_ORDERS),
      exhaustMap((action) =>
        this.orderservice.getOrders(action.reqData).pipe(
          map((orderData) =>
            orderActions.GET_ALL_ORDERS_SUCCESS({ GetOrdersRes: orderData })
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError((error) => of(orderActions.GET_ALL_ORDERS_FAIL(error)))
        )
      )
    )
  );

  getOrderDetais$ = createEffect(() =>
    this.actions$.pipe(
      ofType(orderActions.GET_ORDER_DETALS),
      exhaustMap((action) =>
        this.orderservice.getorderdetails(action.orderId).pipe(
          map((orderData) =>
            orderActions.GET_ORDER_DETALS_SUCCESS({
              GetOrderDetailRes: orderData,
            })
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
          }),
          catchError((error) => of(orderActions.GET_ORDER_DETALS_FAIL(error)))
        )
      )
    )
  );

  postOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(orderActions.POST_ORDER_FROM_CART),
      exhaustMap((action) =>
        this.orderservice.postOrderFromCart(action.orderReq).pipe(
          map((orderData) =>
            orderActions.POST_ORDER_FROM_CART_SUCCESS({
              postOrderRes: orderData,
            })
          ),
          tap((errorOrData) => {
            // console.log(errorOrData);
            if (
              errorOrData.postOrderRes &&
              errorOrData.postOrderRes[0].OrderID
            ) {
              this.toastr.success('Your Order has been placed', 'Thank You!', {
                timeOut: 5000,
                closeButton: true,
              });

              // if successfully order placed delete all cart
              this.store.dispatch(
                DELETE_ALL_CART({ userId: this.Userdata.UserID })
              );

              // rediret to successful order page
              this.router.navigate(['/etc/order-placed']);
            }
          }),
          catchError((error) =>
            of(orderActions.POST_ORDER_FROM_CART_FAIL(error))
          )
        )
      )
    )
  );

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private actions$: Actions,
    private orderservice: OrderService,
    private store: Store<{
      authState: AuthStateInterface;
    }>
  ) {
    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.loaded) {
          this.Userdata = data.user[0];
        }
      });
  }
}
