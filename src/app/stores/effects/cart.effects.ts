import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as cartActions from '../actions/cart.action';
import { CartService } from '../../services/cart.service';

@Injectable()
export class CartEffects {

  getCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cartActions.GET_CART),
      exhaustMap((action) => this.cartservice.GetCartByUserId(action.userId)
        .pipe(
          map(
            cartData => cartActions.GET_CART_SUCCESS({GetCartRes: cartData})
          ),
          catchError(error => of(cartActions.GET_CART_FAIL(error)))
        )
      )
    )
  );

  addProductInCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cartActions.ADD_PRODUCT_IN_CART),
      exhaustMap((action) => this.cartservice.addProductInCart(action.reqData)
        .pipe(
          map(cartData => cartActions.ADD_PRODUCT_IN_CART_SUCCESS({GetCartRes: cartData})
          ),
          tap((errorOrData) => {
              if (errorOrData.GetCartRes['status']) {
                this.toastr.success('Product added in cart', 'Success', {
                  timeOut: 5000,
                  closeButton: true
                });
              }
          }),
          catchError(error => of(cartActions.ADD_PRODUCT_IN_CART_FAIL(error)))
        )
      )
    )
  );

  deleteProductFromCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cartActions.DELETE_PRODUCT_FROM_CART),
      exhaustMap((action) => this.cartservice.deleteProductFromCart(action.reqData)
        .pipe(
          map(cartData => cartActions.DELETE_PRODUCT_FROM_CART_SUCCESS({GetCartRes: cartData})
          ),
          catchError(error => of(cartActions.DELETE_PRODUCT_FROM_CART_FAIL(error)))
        )
      )
    )
  );

  changeProductQtyInCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART),
      exhaustMap((action) => this.cartservice.changeProductQuantityInCart(action.reqData)
        .pipe(
          map(cartData => cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART_SUCCESS({ChangeCartRes: cartData})
          ),
          catchError(error => of(cartActions.CHANGE_PRODUCT_QUANTITY_IN_CART_FAIL(error)))
        )
      )
    )
  );

  deleteAllCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(cartActions.DELETE_ALL_CART),
      exhaustMap((action) => this.cartservice.deleteAllCart(action.userId)
        .pipe(
          map(cartData => cartActions.DELETE_ALL_CART_SUCCESS({DeleteCartRes: cartData})
          ),
          catchError(error => of(cartActions.DELETE_ALL_CART_FAIL(error)))
        )
      )
    )
  );

  constructor(
    private toastr: ToastrService,
    private actions$: Actions,
    private cartservice: CartService
  ) {}

}
