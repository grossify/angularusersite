import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as AuthStateActions from '../actions/auth.actions';
import { LoginService } from '../../services/login.service';
import { RegistrationService } from '../../services/registration.service';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { DELETE_USER_ADDRESS } from '../actions/address.action';
import { AddressStateInterface } from '../../stores/reducers/address.reducer';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthStateActions.LOGIN),
      exhaustMap((action) =>
        this.loginservice.postLoginForm(action.LoginInputData).pipe(
          map((user) => AuthStateActions.LOGIN_SUCCESS({ userData: user })),
          catchError((error) => of(AuthStateActions.LOGIN_FAIL(error))),
          tap((dataOrError) => {
            // console.log(dataOrError);
            // success
            if (
              dataOrError['userData'] &&
              dataOrError['userData'][0] &&
              dataOrError['userData'][0]['UserID']
            ) {
              this.toastr.success(
                'Login Successful, Enjoy shopping',
                'Success',
                {
                  timeOut: 5000,
                  closeButton: true,
                }
              );
              // check for isAddressAvilabe and redirect accordig to it
              if (dataOrError['userData'][0]['isAddressAvilabe']) {
                this.router.navigate(['/']);
              } else {
                this.toastr.info(
                  'Please update your adress before shopping',
                  'Info',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
                this.router.navigate(['/etc/setting']); // redirect to address setting page
              }
              return;
            }

            // handle error
            if (dataOrError['error'] && !dataOrError['error']['status']) {
              if (
                dataOrError['error']['message']
                  .toLowerCase()
                  .indexOf('invalid username') > -1 ||
                dataOrError['error']['message']
                  .toLowerCase()
                  .indexOf('invalid password') > -1
              ) {
                this.toastr.error('Invalid username or password', 'Error', {
                  timeOut: 5000,
                  closeButton: true,
                });
              }
            } else {
              this.toastr.error(
                'Some Server error occourd please try later',
                'Error',
                {
                  timeOut: 5000,
                  closeButton: true,
                }
              );
            }
          })
        )
      )
    )
  );

  registration$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthStateActions.REGISTRATION),
      exhaustMap((action) =>
        this.registrationservice
          .postRegistrationForm(action.registrationIputData)
          .pipe(
            map((user) =>
              AuthStateActions.REGISTRATION_SUCCESS({ registrationData: user })
            ),
            catchError((error) =>
              of(AuthStateActions.REGISTRATION_FAIL(error))
            ),
            tap((dataOrError) => {
              // console.log(dataOrError);
              // success
              if (
                dataOrError['registrationData'] &&
                dataOrError['registrationData'][0] &&
                dataOrError['registrationData'][0]['UserID']
              ) {
                this.toastr.success(
                  'Registration Successful, Enjoy shopping',
                  'Success',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
                // check for isAddressAvilabe and redirect accordig to it
                if (dataOrError['registrationData'][0]['isAddressAvilabe']) {
                  this.router.navigate(['/']);
                } else {
                  this.toastr.info(
                    'Please update your adress before shopping',
                    'Info',
                    {
                      timeOut: 5000,
                      closeButton: true,
                    }
                  );
                  this.router.navigate(['/etc/setting']); // redirect to address setting page
                }
                return;
              }

              // handle error
              if (dataOrError['error'] && !dataOrError['error']['status']) {
                if (
                  dataOrError['error']['message'].indexOf(
                    'Username is already exist'
                  ) > -1
                ) {
                  this.toastr.error('Username is already exist', 'Error', {
                    timeOut: 5000,
                    closeButton: true,
                  });
                }
                if (
                  dataOrError['error']['message'].indexOf(
                    'Phone is already exist'
                  ) > -1
                ) {
                  this.toastr.error('Phone is already exist', 'Error', {
                    timeOut: 5000,
                    closeButton: true,
                  });
                }
                if (
                  dataOrError['error']['message'].indexOf(
                    'Email is already exist'
                  ) > -1
                ) {
                  this.toastr.error('Email is already exist', 'Error', {
                    timeOut: 5000,
                    closeButton: true,
                  });
                }
              } else {
                this.toastr.error(
                  'Some Server error occourd please try later',
                  'Error',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
              }
            })
          )
      )
    )
  );

  LogOut$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthStateActions.LOGOUT),
        tap((user) => {
          this.store.dispatch(DELETE_USER_ADDRESS());
          localStorage.removeItem('authState');
          localStorage.removeItem('address');
          localStorage.removeItem('latLngCity');
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/']);
          window.location.reload();
        })
      ),
    { dispatch: false }
  );

  GetUserProfile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthStateActions.GET_END_USER_PROFILE),
      exhaustMap((action) =>
        this.userservice.GetEndUserProfile(action.userName).pipe(
          map((user) =>
            AuthStateActions.GET_END_USER_PROFILE_SUCCESS({
              GetUserProfileRes: user,
            })
          ),
          tap((dataOrError) => {}),
          catchError((error) =>
            of(AuthStateActions.GET_END_USER_PROFILE_FAIL(error))
          )
        )
      )
    )
  );

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private actions$: Actions,
    private loginservice: LoginService,
    private registrationservice: RegistrationService,
    private userservice: UserService,
    private store: Store<{ address: AddressStateInterface }>
  ) {}
}
