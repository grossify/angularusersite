import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { StoreInterface } from '../interfaces/store-interface';
import { GetStoreByCategoriesInterface, StoreByCategoriesReqInterface } from '../interfaces/get-store-by-categories-interface';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }


  public getStoreByCategories(
    storeByCategoriesData: StoreByCategoriesReqInterface
  ): Observable<GetStoreByCategoriesInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getStoreByCategories';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      }),
      params: {
        userId: storeByCategoriesData.userId
          ? storeByCategoriesData.userId.toString()
          : '',
        catId: storeByCategoriesData.catId
          ? storeByCategoriesData.catId.toString()
          : '',
        latitude: storeByCategoriesData.latitude
          ? storeByCategoriesData.latitude.toString()
          : '',
        longitude: storeByCategoriesData.longitude
          ? storeByCategoriesData.longitude.toString()
          : '',
        pincode: storeByCategoriesData.pincode
          ? storeByCategoriesData.pincode.toString()
          : '',
        city: storeByCategoriesData.city
          ? storeByCategoriesData.city.toString()
          : '',
        limit: storeByCategoriesData.limit
          ? storeByCategoriesData.limit.toString()
          : '',
        offset: storeByCategoriesData.offset
          ? storeByCategoriesData.offset.toString()
          : ''
      }
    };
    return this.http
      .get<GetStoreByCategoriesInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  constructor(private http: HttpClient) {}
}
