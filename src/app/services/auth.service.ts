import { Injectable } from '@angular/core';
import { UserInterface } from '../interfaces/user-interface';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../stores/reducers/auth.reducers';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private ngUnsubscribe = new Subject();
  public Userdata: UserInterface;

  public isAuthenticated(): boolean {
    // return true or false
    if (this.Userdata) {
      if (this.Userdata.UserID) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
  constructor(
    private store: Store<{
      authState: AuthStateInterface
    }>
  ) {
    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      if (data.loaded) {
        this.Userdata = data.user[0];
      }
    });
  }
}
