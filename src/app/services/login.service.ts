import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { SignInInterface } from '../interfaces/sign-in-interface';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public postLoginForm(body: any): Observable<SignInInterface[]> {
    const apiUrl = baseServerUrl + 'UserProfileApi/signIn';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.post<SignInInterface[]>(apiUrl, body, httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }
}
