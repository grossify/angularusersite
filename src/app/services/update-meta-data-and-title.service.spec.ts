import { TestBed } from '@angular/core/testing';

import { UpdateMetaDataAndTitleService } from './update-meta-data-and-title.service';

describe('UpdateMetaDataAndTitleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateMetaDataAndTitleService = TestBed.get(UpdateMetaDataAndTitleService);
    expect(service).toBeTruthy();
  });
});
