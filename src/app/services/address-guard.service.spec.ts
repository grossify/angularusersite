import { TestBed } from '@angular/core/testing';

import { AddressGuardService } from './address-guard.service';

describe('AddressGuardService', () => {
  let service: AddressGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddressGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
