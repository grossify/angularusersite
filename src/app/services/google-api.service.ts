import { Injectable } from '@angular/core';
import { GoogleAPIKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observer, Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { GoogleGeolocationAPIRequestDataInterface } from '../interfaces/google-geolocation-api-request-data-interface';
import { GoogleGeolocationAPIResponseDataInterface } from '../interfaces/google-geolocation-api-response-data-interface';
import { GooglePlaceAutocompleteResponseDataInterface } from '../interfaces/google-place-autocomplete-response-data-interface';
// import { google } from 'google-maps';
// import {} from '@types/googlemaps';

// declare var google: any;

@Injectable({
  providedIn: 'root',
})
export class GoogleAPIService {
  private SessionToken = this.getRandomeToken();

  // SessionToken = new google.maps.places.AutocompleteSessionToken();
  private geocoder: google.maps.Geocoder;

  private getRandomeToken(): string {
    // uuid genrator
    let d = new Date().getTime(); // Timestamp

    // Time in microseconds since page-load or 0 if unsupported
    let d2 = (performance && performance.now && performance.now() * 1000) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      let r = Math.random() * 16; // random number between 0 and 16
      if (d > 0) {
        // Use timestamp until depleted
        r = (d + r) % 16 | 0;
        d = Math.floor(d / 16);
      } else {
        // Use microseconds since page-load if supported
        r = (d2 + r) % 16 | 0;
        d2 = Math.floor(d2 / 16);
      }
      return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public getGeolocationData(): Observable<
    GoogleGeolocationAPIResponseDataInterface
  > {
    const apiUrl =
      'https://www.googleapis.com/geolocation/v1/geolocate?key=' + GoogleAPIKey;

    const body: GoogleGeolocationAPIRequestDataInterface = {};
    body.considerIp = true;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }),
    };
    return this.http
      .post<GoogleGeolocationAPIResponseDataInterface>(
        apiUrl,
        body,
        httpOptions
      )
      .pipe(retry(3), catchError(this.handleError));
  }

  public getPlaceAutocomplete(
    searchText: string
  ): Observable<GooglePlaceAutocompleteResponseDataInterface> {
    const apiUrl =
      'https://maps.googleapis.com/maps/api/place/autocomplete' +
      '/json?' +
      'input=' +
      searchText +
      '&types=address' +
      '&language=en' +
      '&components=country:in' +
      '&key=' +
      GoogleAPIKey +
      '&sessiontoken=' +
      this.SessionToken;

    // const apiUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }),
      // params: new HttpParams()
      // .set('input', searchText)
      // .set('types', 'address')
      // .set('language', 'en')
      // .set('components', 'country:in')
      // .set('key', GoogleAPIKey)
      // .set('sessiontoken', this.SessionToken)
    };
    return this.http
      .post<GooglePlaceAutocompleteResponseDataInterface>(apiUrl, httpOptions)
      .pipe(catchError(this.handleError));
  }

  /**
   * Reverse geocoding by location.
   *
   * Wraps the Google Maps API geocoding service into an observable.
   *
   * @param latLng Location
   * @return An observable of GeocoderResult
   */
  public geocode(
    latLng: google.maps.LatLngLiteral
  ): Observable<google.maps.GeocoderResult[]> {
    return Observable.create(
      (observer: Observer<google.maps.GeocoderResult[]>) => {
        // Invokes geocode method of Google Maps API geocoding.
        this.geocoder.geocode(
          { location: latLng },
          (
            results: google.maps.GeocoderResult[],
            status: google.maps.GeocoderStatus
          ) => {
            if (status === google.maps.GeocoderStatus.OK) {
              observer.next(results);
              observer.complete();
            } else {
              console.log(
                'Geocoding service: geocoder failed due to: ' + status
              );
              observer.error(status);
            }
          }
        );
      }
    );
  }

  /**
   * Geocoding service.
   *
   * Wraps the Google Maps API geocoding service into an observable.
   *
   * @param address The address to be searched
   * @return An observable of GeocoderResult
   */
  public codeAddress(
    address: string
  ): Observable<google.maps.GeocoderResult[]> {
    return Observable.create(
      (observer: Observer<google.maps.GeocoderResult[]>) => {
        // Invokes geocode method of Google Maps API geocoding.
        this.geocoder.geocode(
          { address: address },
          (
            results: google.maps.GeocoderResult[],
            status: google.maps.GeocoderStatus
          ) => {
            if (status === google.maps.GeocoderStatus.OK) {
              observer.next(results);
              observer.complete();
            } else {
              console.log(
                'Geocoding service: geocode was not successful for the following reason: ' +
                  status
              );
              observer.error(status);
            }
          }
        );
      }
    );
  }

  /**
   * Tries HTML5 geolocation.
   *
   * Wraps the Geolocation API into an observable.
   *
   * @return An observable of Position
   */
  getCurrentPosition(): Observable<Position> {
    return Observable.create((observer: Observer<Position>) => {
      // Invokes getCurrentPosition method of Geolocation API.
      navigator.geolocation.getCurrentPosition(
        (position: Position) => {
          observer.next(position);
          observer.complete();
        },
        (error: PositionError) => {
          console.log('Geolocation service: ' + error.message);
          observer.error(error);
        }
      );
    });
  }

  constructor(private http: HttpClient) {
    this.geocoder = new google.maps.Geocoder();
  }
}
