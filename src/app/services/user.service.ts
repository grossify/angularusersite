import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { UserAddressInterface, UserAddressInputInterface } from '../interfaces/user-address-interface';
import { UserInterface } from '../interfaces/user-interface';
import { UpdateEndUserInterface } from '../interfaces/update-end-user-interface';
import { UpdateEndUserProfileInterface } from '../interfaces/update-end-user-profile-interface';
import {
    ChangePasswordInputInterface,
    ChangePasswordResponseSuccessInterface
} from '../interfaces/change-password-interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public getUserAddress(UserId: number): Observable<UserAddressInterface[]> {
    const apiUrl = baseServerUrl + 'UserInfoApi/getUserAddress/' + UserId.toString();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<UserAddressInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public userAddress(UserData: UserAddressInputInterface): Observable<UserAddressInterface[]> {
    const apiUrl = baseServerUrl + 'UserInfoApi/userAddress';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<UserAddressInterface[]>(apiUrl, UserData, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public GetEndUserProfile(UserName: string): Observable<UserInterface[]> {
    const apiUrl = baseServerUrl + 'UserInfoApi/GetEndUserProfile/' + UserName;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<UserInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public UpdateEndUser(UserData: UserInterface): Observable<UpdateEndUserInterface[]> {
    const apiUrl = baseServerUrl + 'UserProfileApi/UpdateEndUser';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<UpdateEndUserInterface[]>(apiUrl, UserData, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public UpdateEndUserProfile(reqData: {userId: number, croppedFile: Blob}): Observable<UpdateEndUserProfileInterface> {
    const apiUrl = baseServerUrl + 'UserProfileApi/UpdateEndUserProfile';
    const fd: FormData = new FormData();
    fd.append('avatar', reqData.croppedFile, 'Profile-pic.jpeg');
    fd.append('UserId', reqData.userId.toString());
    // fd.forEach( (value, key) => {
    //   console.log(key);
    //   console.log(value);
    // })
    // const body = new URLSearchParams();
    // body.set('UserId', UserId.toString());
    // body.set('avatar', Profile.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/x-www-form-urlencoded',   // woring only without Content-Type header
        // 'Content-Type': 'multipart/form-data',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return (
      this.http
        .post<UpdateEndUserProfileInterface>(apiUrl, fd, httpOptions)
        .pipe(retry(3), catchError(this.handleError))
    );
  }

  public changePassword(
    userName: string,
    newPassword: string,
    oldPassword: string
  ): Observable<UserInterface[]> {
    const apiUrl = baseServerUrl + 'UserProfileApi/changePassword';

    const body = new URLSearchParams();
    // body.set('UserId', UserId.toString());
    body.set('userName', userName.toString());
    body.set('new_Password', newPassword.toString());
    body.set('old_Password', oldPassword.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<UserInterface[]>(apiUrl, body.toString(), httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public changePasswordByUserId(
    passData: ChangePasswordInputInterface
    ): Observable<ChangePasswordResponseSuccessInterface> {
    const apiUrl = baseServerUrl + 'UserProfileApi/ChangeUserPassword';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<ChangePasswordResponseSuccessInterface>(apiUrl, passData, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  constructor(private http: HttpClient) {}
}
