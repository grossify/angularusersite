import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { AddressGuardService } from './address-guard.service';
// import { MatDialog } from '@angular/material/dialog';
// import { UpdateAddressComponent } from '../dialogs/update-address-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { UserInterface } from '../interfaces/user-interface';
import { AuthStateInterface } from '../stores/reducers/auth.reducers';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  public Userdata: UserInterface;
  private ngUnsubscribe = new Subject();

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      // not logged in
      this.router.navigate(['/etc/login']);
      return false;
    } else {
      // check for address
      // if (!this.address.isAddressAvailable()) {
      //   // not saved address
      //   // this.router.navigate(['/etc/settings/address']);
      //   const dialogRef = this.dialog.open(UpdateAddressComponent, {
      //     width: '85%',
      //     height: '85%',
      //     data: { thisUser: this.Userdata, UserAddress: {} }
      //   });
      // } else {
      //   // not saved address
      //   // this.router.navigate(['/']);
      // }
      return true;
    }
  }


  constructor(
    // public dialog: MatDialog,
    private auth: AuthService,
    private router: Router,
    private address: AddressGuardService,
    private store: Store<{
      authState: AuthStateInterface
    }>
    ) {
      // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('authState')
    )
    .subscribe( (data: AuthStateInterface) => {
      if (data.user && data.user[0]) {
        this.Userdata = data.user[0];
      }

    });
  }
}
