import { Injectable } from '@angular/core';
import { UserAddressInterface } from '../interfaces/user-address-interface';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { AddressStateInterface } from '../stores/reducers/address.reducer';

@Injectable({
  providedIn: 'root'
})
export class AddressGuardService {
  private ngUnsubscribe = new Subject();
  public UserAddress: UserAddressInterface;

  public isAddressAvailable(): boolean {
    // return true or false
    if (this.UserAddress) {
      if (
        this.UserAddress.latitude &&
        this.UserAddress.longitude &&
        this.UserAddress.city
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  constructor(
    private store: Store<{
      address: AddressStateInterface
    }>
  ) {
    // subscribe to address store state
    store
      .pipe(
        takeUntil(this.ngUnsubscribe),
        select('address')
      )
      .subscribe( (data: AddressStateInterface) => {
        if (data.userAddressState && data.userAddressState.UserAddress && data.userAddressState.loaded) {
          this.UserAddress = data.userAddressState.UserAddress;
        }
    });
  }
}
