import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { DeleteAllCartInterface } from '../interfaces/delete-all-cart-interface';
import { GetCartByUserIdInterface } from '../interfaces/get-cart-by-user-id-interface';
import { AddProductInCartInputInterface } from '../interfaces/add-product-in-cart-interface';
import { DeleteProductInCartInputInterface } from '../interfaces/delete-product-from-cart-interface';
import { ChangeProductFromCartInputInterface } from '../interfaces/change-product-quantity-in-cart-interface';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public GetCartByUserId(
    UserId: number
  ): Observable<GetCartByUserIdInterface> {
    const apiUrl =
      baseServerUrl + 'UserCartApi/getCartByUserId/' + UserId.toString();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.get<GetCartByUserIdInterface>(apiUrl, httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public addProductInCart(
    reqData: AddProductInCartInputInterface
  ): Observable<GetCartByUserIdInterface> {
    const apiUrl = baseServerUrl + 'UserCartApi/addProductInCart';

    const body = new URLSearchParams();
    body.set('UserId', reqData.UserId.toString());
    body.set('storeProductId', reqData.storeProductId.toString());
    body.set('ProductUnitID', reqData.ProductUnitID.toString());
    body.set('qty', reqData.qty.toString());
    body.set('storeId', reqData.storeId.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<GetCartByUserIdInterface>(apiUrl, body.toString(), httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public changeProductQuantityInCart(
    reqData: ChangeProductFromCartInputInterface
  ): Observable<GetCartByUserIdInterface> {
    const apiUrl = baseServerUrl + 'UserCartApi/changeProductQuantityInCart';

    const body = new URLSearchParams();
    body.set('UserId', reqData.UserId.toString());
    body.set('storeProductId', reqData.storeProductId.toString());
    body.set('ProductUnitID', reqData.ProductUnitID.toString());
    body.set('qty', reqData.qty.toString());
    body.set('storeId', reqData.storeId.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<GetCartByUserIdInterface>(
        apiUrl,
        body.toString(),
        httpOptions
      )
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public deleteAllCart(UserId: number): Observable<DeleteAllCartInterface> {
    const apiUrl = baseServerUrl + 'UserCartApi/deleteAllCart';  // check for sepated api

    const body = new URLSearchParams();
    body.set('userId', UserId.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.post<DeleteAllCartInterface>(apiUrl, body.toString(), httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public deleteProductFromCart(
    reqData: DeleteProductInCartInputInterface
  ): Observable<GetCartByUserIdInterface> {
    const apiUrl = baseServerUrl + 'UserCartApi/deleteProductFromCart';

    const body = new URLSearchParams();
    body.set('UserId', reqData.UserId.toString());
    body.set('storeProductId', reqData.storeProductId.toString());
    body.set('ProductUnitID', reqData.ProductUnitID.toString());
    body.set('storeId', reqData.storeId.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<GetCartByUserIdInterface>(apiUrl, body.toString(), httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  constructor(private http: HttpClient) {}
}
