import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, Observer } from 'rxjs';
import { GetBannerInterface } from '../interfaces/get-banner-interface';
import { GetOffersInterface } from '../interfaces/get-offers-interface';
import { GeolocationPositionInterface } from '../interfaces/geolocation-interface';
import { ToastrService } from 'ngx-toastr';

import { Store } from '@ngrx/store';
import { applicationState } from '../stores/reducers/application.reducer';
import { SET_SCREEN } from '../stores/actions/application.action';

@Injectable({
  providedIn: 'root',
})
export class ApplicationService {
  public mobile$ = this.store.select(
    (state) => state.applicationState.ScreenState.mobile
  );
  public tablet$ = this.store.select(
    (state) => state.applicationState.ScreenState.tablet
  );
  public desktop$ = this.store.select(
    (state) => state.applicationState.ScreenState.desktop
  );

  public setWindowWidth(windowWidth: number): void {
    this.store.dispatch(SET_SCREEN(windowWidth));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public getBanner(): Observable<GetBannerInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getBanner';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json',
      }),
    };
    return this.http
      .get<GetBannerInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getOffers(): Observable<GetOffersInterface[]> {
    const apiUrl = baseServerUrl + 'getOffer';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json',
      }),
    };
    return this.http
      .get<GetOffersInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  private handleLocationError(error: any): void {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        this.toastr.error(
          +'This is highly recommended that you grant us location access. ' +
            'Please manually select location or allow location access from browser setting. ',
          'Oops You denied location access.',
          {
            timeOut: 5000,
            closeButton: true,
          }
        );
        break;
      case error.POSITION_UNAVAILABLE:
        this.toastr.info(
          'Please manually select location so we can give you better store results.',
          'Info',
          {
            timeOut: 5000,
            closeButton: true,
          }
        );
        break;
      case error.TIMEOUT:
        this.toastr.info(
          'Please manually select location so we can give you better store results.',
          'Info',
          {
            timeOut: 5000,
            closeButton: true,
          }
        );
        break;
      case error.UNKNOWN_ERROR:
        this.toastr.info(
          'Please manually select location so we can give you better store results.',
          'Info',
          {
            timeOut: 5000,
            closeButton: true,
          }
        );
        break;
    }
  }

  private getCurrentPosition(observer: Observer<any>): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          // console.log(position);
          observer.next(position);
          observer.complete();
        },
        (error) => {
          console.log(error);
          this.handleLocationError(error);
          observer.error(error);
        },
        { maximumAge: Infinity, timeout: 10000 }
      );
    } else {
      // console.log('No support for geolocation let user select the location');
      observer.error(
        new Error('No support for geolocation let user select the location')
      );
    }
  }

  public getGeolocation(): Observable<GeolocationPositionInterface> {
    const locationObservable = Observable.create((observer: Observer<any>) => {
      if (window.navigator.permissions) {
        navigator.permissions
          .query({ name: 'geolocation' })
          .then((result: PermissionStatus) => {
            // console.log('geolocation permission state is ', result.state);
            switch (result.state.toLocaleLowerCase()) {
              case 'granted':
                this.getCurrentPosition(observer);
                break;
              case 'prompt':
                this.toastr.info(
                  'Please allow location  access so we can serve you better stores.',
                  'Info',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
                this.getCurrentPosition(observer);
                break;
              case 'denied':
                this.toastr.error(
                  +'This is highly recommended that you grant us location access. ' +
                    'Please manually select location or allow location access from browser setting. ',
                  'Oops You denied location access.',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
                observer.error({ error: 'denied' });
                break;
            }

            result.onchange = (event: Event) => {
              // console.log(result.state);
              if (result.state.toLocaleLowerCase() === 'granted') {
                this.getCurrentPosition(observer);
              } else if (result.state.toLocaleLowerCase() === 'denied') {
                this.toastr.error(
                  +'This is highly recommended that you grant us location access. ' +
                    'Please manually select location or allow location access from browser setting. ',
                  'Oops You denied location access.',
                  {
                    timeOut: 5000,
                    closeButton: true,
                  }
                );
              }
            };
          });
      } else {
        // even navigator.permissions not exist maybe navigator.geolocation exist
        this.getCurrentPosition(observer);
      }
    });
    return locationObservable;
  }

  constructor(
    private toastr: ToastrService,
    private http: HttpClient,
    private store: Store<{ applicationState: applicationState }>
  ) {}
}
