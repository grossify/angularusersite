import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { GetOrdersInterface, GetOrdersInputInterface } from '../interfaces/get-orders-interface';
import { GetorderdetailsInterface } from '../interfaces/getorderdetails-interface';
import { PostOrdersReqInterface, PostOrderFromCartInterface } from '../interfaces/post-order-from-cart-interface';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      console.error(JSON.stringify(error.error));
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  public getOrders(reqData: GetOrdersInputInterface): Observable<GetOrdersInterface> {
    const apiUrl = baseServerUrl +
      'UserOrdersApi/getOrders/' +
      reqData.userId.toString() +
      '/' +
      reqData.limit.toString()
      '/' +
      reqData.offset.toString();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.get<GetOrdersInterface>(apiUrl, httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public getorderdetails(OrderId: number): Observable<GetorderdetailsInterface[]> {
    const apiUrl = baseServerUrl + 'UserOrdersApi/getOrderDetails/' + OrderId.toString();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.get<GetorderdetailsInterface[]>(apiUrl, httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public postOrderFromCart(OrderData: PostOrdersReqInterface): Observable<PostOrderFromCartInterface[]> {
    const apiUrl = baseServerUrl + 'UserCartApi/postOrderFromCart';

    const body = new URLSearchParams();
    body.set('UserID', OrderData.UserID.toString());
    body.set('storeId', OrderData.storeId.toString());
    body.set('OrderNote', OrderData.OrderNote);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http.post<PostOrderFromCartInterface[]>(apiUrl, body.toString(), httpOptions).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }
}
