import { Injectable } from '@angular/core';
import { baseServerUrl, AuthKey } from '../globels';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { StoreDetailInterface, StoreDetailReqInterface } from '../interfaces/store-detail-interface';
import { StoreCategoriesInterface } from '../interfaces/store-categories-interface';
import {
  AddFavouriteStoreInterface,
  AddFavouriteStoreReqInterface
} from '../interfaces/add-favourite-store-interface';
import { ProductCategoriesOfStoresInterface } from '../interfaces/product-categories-of-stores-interface';
import { GetStoreProductsInterface, StoreProductsReqInterface } from '../interfaces/get-store-products-interface';
import { FevouriteStoresInterface } from '../interfaces/fevourite-stores-interface';
import { GetAllStoreInterface } from '../interfaces/get-all-store';
import { GetProductDetailsInterface } from '../interfaces/get-product-details-interface';
import { AddReferStoreInterface } from '../interfaces/add-refer-store-interface';
import { AddReferStoreClass } from '../classes/add-refer-store-class';
import {
  GetNearByStoreReqInterface,
  GetNearByStoreInterface
} from '../interfaces/get-near-by-store';
import {
  RemoveFavouriteStoreReqInterface,
  RemoveFavouriteStoreResInterface
} from '../interfaces/fevourite-stores-interface';
import {
  StoreByCategoriesReqInterface,
  GetStoreByCategoriesInterface
} from '../interfaces/get-store-by-categories-interface';
import {
  CityStoresInterface,
  CityStoresReqInterface
} from '../interfaces/city-stores-interface';


@Injectable({
  providedIn: 'root'
})
export class StoreService {
  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  public getAllStores(UserId: number): Observable<GetAllStoreInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getAllStores/' + UserId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<GetAllStoreInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getNearByStores(
    NearbyData: GetNearByStoreReqInterface
  ): Observable<GetNearByStoreInterface> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getNearByStores';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      }),
      params: {
        userId: NearbyData.userId ? NearbyData.userId.toString(10) : '',
        latitude: NearbyData.latitude ? NearbyData.latitude.toString(10) : '',
        longitude: NearbyData.longitude ? NearbyData.longitude.toString(10) : '',
        pincode: NearbyData.pincode ? NearbyData.pincode : '',
        city: NearbyData.city ? NearbyData.city : '',
        limit: NearbyData.limit ? NearbyData.limit.toString(10) : '',
        offset: NearbyData.offset ? NearbyData.offset.toString(10) : '0'
      }
    };

    return this.http
      .get<GetNearByStoreInterface>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getStoreDetail(
    reqData: StoreDetailReqInterface
  ): Observable<StoreDetailInterface[]> {
    const apiUrl =
      baseServerUrl +
      'UserStoreApi/getStoreDetail/' +
      reqData.UserId.toString(10) +
      '/' +
      reqData.StoreId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<StoreDetailInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getStoreCategories(UserId: number): Observable<StoreCategoriesInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getStoreCategories/' + UserId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<StoreCategoriesInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getProductCategoriesOfStores(
    StoreId: number
  ): Observable<ProductCategoriesOfStoresInterface[]> {
    const apiUrl =
      baseServerUrl + 'UserStoreApi/getProductCategoriesOfStores/' + StoreId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<ProductCategoriesOfStoresInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getFevouriteStores(
    UserId: number
  ): Observable<FevouriteStoresInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getFevoriteStores/' + UserId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<FevouriteStoresInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getNearByFavoriteStores(
    NearbyData: GetNearByStoreReqInterface
  ): Observable<FevouriteStoresInterface> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getNearByFavoriteStores/';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      }),
      params: {
        userId: NearbyData.userId ? NearbyData.userId.toString(10) : '',
        latitude: NearbyData.latitude ? NearbyData.latitude.toString(10) : '',
        longitude: NearbyData.longitude ? NearbyData.longitude.toString(10) : '',
        pincode: NearbyData.pincode ? NearbyData.pincode : '',
        city: NearbyData.city ? NearbyData.city : '',
        limit: NearbyData.limit ? NearbyData.limit.toString(10) : '',
        offset: NearbyData.offset ? NearbyData.offset.toString(10) : '0'
      }
    };

    return this.http
      .get<FevouriteStoresInterface>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public GetStoreProducts(
    StoreProductsReq: StoreProductsReqInterface
  ): Observable<GetStoreProductsInterface> {
    const apiUrl =
      baseServerUrl +
      'UserProductApi/getStoreProducts/' +
      StoreProductsReq.StoreId.toString(10) +
      '/' +
      StoreProductsReq.CatId.toString(10) +
      '/' +
      StoreProductsReq.EndUserId.toString(10) +
      '/' +
      StoreProductsReq.limit.toString(10) +
      '/' +
      StoreProductsReq.offset.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<GetStoreProductsInterface>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public getProductDetails(
    storeProductId: number
  ): Observable<GetProductDetailsInterface[]> {
    const apiUrl = baseServerUrl + 'UserProductApi/getProductDetails/' + storeProductId.toString(10);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .get<GetProductDetailsInterface[]>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public addFavouriteStore(
    data: AddFavouriteStoreReqInterface
  ): Observable<AddFavouriteStoreInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/addFavouriteStore';

    // const body = new URLSearchParams();
    // body.set('store_id', userId.toString(10));
    // body.set('end_user_id', endUserId.toString(10));

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<AddFavouriteStoreInterface[]>(apiUrl, data, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  public removeFavouriteStore(
    data: RemoveFavouriteStoreReqInterface
  ): Observable<RemoveFavouriteStoreResInterface> {
    const apiUrl = baseServerUrl + 'UserStoreApi/removeFavouriteStore';

    // const body = new URLSearchParams();
    // body.set('store_id', userId.toString(10));
    // body.set('end_user_id', endUserId.toString(10));

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<RemoveFavouriteStoreResInterface>(
        apiUrl,
        data,
        // body.toString(10),
        httpOptions
      )
      .pipe(retry(3), catchError(this.handleError));
  }

  public addReferStore(
    data: AddReferStoreClass
  ): Observable<AddReferStoreInterface[]> {
    const apiUrl = baseServerUrl + 'UserStoreApi/addReferStore';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      })
    };
    return this.http
      .post<AddReferStoreInterface[]>(apiUrl, data, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }


  public getStoreByCity(
    ReqData: CityStoresReqInterface
  ): Observable<CityStoresInterface> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getStoreByCity';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      }),
      params: {
        userId: ReqData.userId.toString(10),
        city: ReqData.city
      }

    };
    return this.http
      .get<CityStoresInterface>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }


  public getStoreByCategories(
    storeCatReqData: StoreByCategoriesReqInterface
  ): Observable<GetStoreByCategoriesInterface> {
    const apiUrl = baseServerUrl + 'UserStoreApi/getStoreByCategories';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': AuthKey,
        Accept: 'application/json'
      }),
      params: {
        userId: storeCatReqData.userId.toString(10),
        catId: storeCatReqData.catId.toString(10),
        latitude: storeCatReqData.latitude.toString(10),
        longitude: storeCatReqData.longitude.toString(10),
        city: storeCatReqData.city,
        pincode: storeCatReqData.pincode ? storeCatReqData.pincode : '',
        limit: storeCatReqData.limit ? storeCatReqData.limit.toString(10) : '',
        offset: storeCatReqData.offset ? storeCatReqData.offset.toString(10) : '0'
      }

    };
    return this.http
      .get<GetStoreByCategoriesInterface>(apiUrl, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }
}
