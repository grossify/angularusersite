import { Injectable } from '@angular/core';
// import { AngularFireDatabase } from '@angular/fire/database';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireMessaging } from '@angular/fire/messaging';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

// import { AngularFireFunctions } from '@angular/fire/functions';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  token: string;

  constructor(
    // private angularFireDB: AngularFireDatabase,
    // private angularFireAuth: AngularFireAuth,
    // private fun: AngularFireFunctions,
    private toastr: ToastrService,
    // private angularFireMessaging: AngularFireMessaging
  ) {
    // Bind methods to fix temporary bug in AngularFire
    // this.angularFireMessaging.messaging.subscribe(messagingEv => {
    //   messagingEv.onMessage = messagingEv.onMessage.bind(messagingEv);
    //   messagingEv.onTokenRefresh = messagingEv.onTokenRefresh.bind(messagingEv);
    // });
  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */
  updateToken(userId: number, token: string) {
    // we can change this function to request our backend service
    // this.angularFireAuth.authState.pipe(take(1)).subscribe(() => {
    //   const data = {};
    //   data[userId] = token;
    //   this.angularFireDB.object('fcmTokens/').update(data);
    // });
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId: number) {
    // this.angularFireMessaging.requestToken.subscribe(
    //   token => {
    //     console.log(token);
    //     this.token = token;
    //     this.updateToken(userId, token);

    //     // subscribe to topic 'topic-User-' + userId
    //     this.subscribe('topic-user-' + userId);

    //     this.toastr.info(token, 'token', {
    //       timeOut: 5000,
    //       closeButton: true
    //     });
    //   },
    //   err => {
    //     console.error('Unable to get permission to notify.', err);
    //   }
    // );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    // this.angularFireMessaging.messages.subscribe(payload => {
    //   console.log('new message received. ', payload);
    //   this.currentMessage.next(payload);
    //   this.toastr.info(JSON.stringify(payload), 'Message Recieved', {
    //     timeOut: 5000,
    //     closeButton: true
    //   });
    // });
  }

  subscribe(topic: string) {
    // this.fun
    //   .httpsCallable('subscribeToTopic')({ topic, token: this.token })
    //   .pipe(
    //     tap(() => {
    //       this.toastr.info(`subscribed to ${topic}`, 'Subscribed ', {
    //         timeOut: 5000,
    //         closeButton: true
    //       });
    //     })
    //   )
    //   .subscribe();
  }

  unsubscribe(topic: string) {
    // this.fun
    //   .httpsCallable('unsubscribeFromTopic')({ topic, token: this.token })
    //   .pipe(
    //     tap(() => {
    //       this.toastr.info(`unsubscribed from ${topic}`, 'Unsubscribed ', {
    //         timeOut: 5000,
    //         closeButton: true
    //       });
    //     })
    //   )
    //   .subscribe();
  }
}
