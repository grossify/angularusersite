import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetCartByUserIdInterface } from '../interfaces/get-cart-by-user-id-interface';
import { GetStoreProductsItemInterface } from '../interfaces/get-store-products-interface';
import { AddProductInCartInputInterface } from '../interfaces/add-product-in-cart-interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { CartStateInterface } from '../stores/reducers/cart.reducer';
import { ADD_PRODUCT_IN_CART, DELETE_ALL_CART } from '../stores/actions/cart.action';

@Component({
    selector: 'app-same-store-items-dialog',
    templateUrl: '../dialogs/app-same-store-items.html',
    styleUrls: ['../dialogs/app-same-store-items.scss']
})
export class SameStoreItemsDailogComponent implements OnInit {
    private ngUnsubscribe = new Subject();
    private product: GetStoreProductsItemInterface;
    private cart: GetCartByUserIdInterface;
    private reqData: AddProductInCartInputInterface;
    public storeName: string;
    public storeid: number;

    public onDeleteCartClick() {
        // delete all cart
        this.store.dispatch(DELETE_ALL_CART({userId: this.reqData.UserId}));
    }

    constructor(
        public dialogRef: MatDialogRef<SameStoreItemsDailogComponent>,
        @Inject(MAT_DIALOG_DATA)
        private data: {
            product: GetStoreProductsItemInterface,
            cart: GetCartByUserIdInterface,
            reqData: AddProductInCartInputInterface
        },
        private store: Store<{
          cart: CartStateInterface
        }>
    ) {
        this.reqData = data.reqData;
        this.cart = data.cart;
        this.product = data.product;
        this.storeName = data.cart.store[0].store_name;
        this.storeid = data.cart.store[0].store_id;

        // subscribe to cart store state
        store
        .pipe(
        takeUntil(this.ngUnsubscribe),
        select('cart')
        )
        .subscribe( (data: CartStateInterface) => {
            if (data.DeleteAllCartState.loaded) {
                // add product in cart if valid (from same store as cart)
                this.store.dispatch(ADD_PRODUCT_IN_CART({reqData: this.reqData}));
                if (data.addProductInCartState.loaded) {
                    this.dialogRef.close();
                }
            }
        });
    }

    ngOnInit() {}

}
