import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  NgZone,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserInterface } from '../interfaces/user-interface';
import { Address } from '../classes/address';
import { ToastrService } from 'ngx-toastr';
import { FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';
import { IndianStateAndUnionTerritoriesData, CountriesData } from '../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserAddressInterface } from '../interfaces/user-address-interface';
import { MouseEvent, GoogleMapsAPIWrapper } from '@agm/core';
import { GoogleGeolocationAPIResponseDataInterface } from '../interfaces/google-geolocation-api-response-data-interface';

import {
  GooglePlaceAutocompleteResponseDataInterface,
  Predictions,
} from '../interfaces/google-place-autocomplete-response-data-interface';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../stores/reducers/application.reducer';
import { AuthStateInterface } from '../stores/reducers/auth.reducers';
import { AddressStateInterface } from '../stores/reducers/address.reducer';
import { GET_GOOGLE_GEOLOCATION } from '../stores/actions/application.action';
import { POST_USER_ADDRESS } from '../stores/actions/address.action';

declare var google: any;

@Component({
  selector: 'app-update-address-dialog',
  templateUrl: '../dialogs/update-address.html',
  styleUrls: ['../dialogs/update-address.scss'],
})
export class UpdateAddressComponent implements OnInit, OnDestroy {
  public stateAndUnion = IndianStateAndUnionTerritoriesData;
  public CountriesData = CountriesData;
  public addressLine1: string;
  public addressLine2: string;
  public city: string;
  public state: string;
  public country: string;
  public postalCode: string;

  public lat: number;
  public lng: number;
  public zoom: number;
  public defaultZoom: number;

  public geolocationLat: number;
  public geolocationLng: number;

  public googleGeolocation: GoogleGeolocationAPIResponseDataInterface;

  // for storing selected lat lng on map
  public selectedLat: number;
  public selectedLng: number;

  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  public addressList: Predictions[];
  @ViewChild('map', { static: true }) public mapElementRef: ElementRef;

  public addressLine1FormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.address_line1 : '',
    [Validators.minLength(3), Validators.maxLength(256)]
  );

  public addressLine2FormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.address_line2 : '',
    [Validators.minLength(3), Validators.maxLength(128)]
  );

  public cityFormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.city : '',
    [Validators.minLength(3), Validators.maxLength(128)]
  );

  public stateFormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.state : '',
    [Validators.minLength(3), Validators.maxLength(128)]
  );

  public countryFormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.country : '',
    [Validators.minLength(3), Validators.maxLength(128)]
  );

  public postalCodeFormControl = new FormControl(
    this.data.UserAddress ? this.data.UserAddress.postal_code : '',
    [Validators.minLength(6), Validators.maxLength(15)]
  );

  public addressLine1Matcher = new MyErrorStateMatcher();
  public addressLine2Matcher = new MyErrorStateMatcher();
  public cityMatcher = new MyErrorStateMatcher();
  public stateMatcher = new MyErrorStateMatcher();
  public countryMatcher = new MyErrorStateMatcher();
  public postalCodeMatcher = new MyErrorStateMatcher();

  private ngUnsubscribe = new Subject();

  public mapClicked($event: MouseEvent) {
    // returns lat lng of clicked location from map
    const mouseLat = $event.coords.lat;
    const mouseLng = $event.coords.lng;
    this.lat = mouseLat;
    this.lng = mouseLng;
    this.googleReverseGeocode();
  }

  public centerChange(event): void {
    this.selectedLat = event.lat;
    this.selectedLng = event.lng;
  }

  public markerClicked($event: any): void {}

  private getLatLongFromPlaceId(googlePlaceId: string) {
    const placeService = new google.maps.places.PlacesService(
      document.createElement('div')
    );
    placeService.getDetails(
      {
        placeId: googlePlaceId,
      },
      (result, status) => {
        if (status.toLocaleLowerCase() === 'ok') {
          // console.log(result.geometry.location.lat());
          // console.log(result.geometry.location.lng());
          // move map center to selected address
          this.lat = result.geometry.location.lat();
          this.lng = result.geometry.location.lng();
          this.zoom = this.defaultZoom + 0.5;
          this.getAddressComponents(result);
        }
      }
    );
  }

  private getAddressComponents(results: any): void {
    let country: string;
    let state: string;
    let city: string;
    let postalCode: number;

    // get country
    if (results[0] && results[0].address_components) {
      results.forEach((firstValue) => {
        firstValue.address_components.forEach((secondValue) => {
          secondValue.types.forEach((element) => {
            if (element === 'country') {
              country = secondValue.long_name;
            }
            if (element === 'administrative_area_level_1') {
              state = secondValue.long_name;
            }
            if (element === 'administrative_area_level_2') {
              city = secondValue.long_name;
            }
            if (!city && element === 'locality') {
              city = secondValue.long_name;
            }
            if (element === 'postal_code') {
              postalCode = secondValue.long_name;
            }
          });
        });
      });
    }

    if (results.address_components) {
      results.address_components.forEach((firstValue) => {
        firstValue.types.forEach((element) => {
          if (element === 'country') {
            country = firstValue.long_name;
          }
          if (element === 'administrative_area_level_1') {
            state = firstValue.long_name;
          }
          if (element === 'administrative_area_level_2') {
            city = firstValue.long_name;
          }
          if (!city && element === 'locality') {
            city = firstValue.long_name;
          }
          if (element === 'postal_code') {
            postalCode = firstValue.long_name;
          }
        });
      });
    }

    if (country) {
      this.countryFormControl.setValue(country);
    }
    if (state) {
      this.stateFormControl.setValue(state);
    }
    if (city) {
      this.cityFormControl.setValue(city);
    }
    if (postalCode) {
      this.postalCodeFormControl.setValue(postalCode);
    }
    if (results.formatted_address) {
      this.addressLine1FormControl.setValue(results.formatted_address);
    } else {
      this.addressLine1FormControl.setValue(results[0].formatted_address);
    }
    // console.log(country);
    // console.log(state);
    // console.log(city);
    // console.log(postalCode);
  }

  private googleReverseGeocode(): void {
    // get lat lng to address.
    const geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(this.lat, this.lng);
    const request = {
      location: latlng,
    };

    geocoder.geocode(request, (results, status) => {
      if (status.toLocaleLowerCase() === 'ok') {
        this.ngZone.run(() => {
          // move map center to selected address
          this.lat = request.location.lat();
          this.lng = request.location.lng();
          this.zoom = this.defaultZoom + 0.5;

          this.getAddressComponents(results);
        });
      } else {
        console.log(
          'Geocode was not successful for the following reason: ' + status
        );
      }
    });
  }

  private getPlaceAutocomplete(searchText: string) {
    const autocomplete = new google.maps.places.Autocomplete(
      this.searchElementRef.nativeElement,
      {
        componentRestrictions: { country: 'in' },
        types: ['address'], // 'establishment' / 'address' / 'geocode'
      }
    );
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      // set address
      // console.log(place);
      this.getAddressComponents(place);
    });
  }

  public submitAutocomplete(): boolean {
    // fired on pressing enter in address input 1 form field
    const searchText: string = this.addressLine1FormControl.value;

    if (searchText.length > 4) {
      setTimeout(() => {
        if (searchText === this.addressLine1FormControl.value) {
          // means user stops typing
          this.getPlaceAutocomplete(searchText);
        }
      }, 400); // delay for checking user is typing or not
      return true;
    }
    return false;
  }

  public autocompletekeyUpHandler(): void {
    this.submitAutocomplete();
  }

  public selectedAddress(address: Predictions): void {
    // change addressLine1FormControl value to selected
    this.addressLine1FormControl.setValue(address.description);

    // convert placeId to lat long for showing on map
    this.getLatLongFromPlaceId(address.place_id);

    // close the predection pannel
    this.addressList = [];
  }

  public markerDragEnd(lat: number, lng: number, $event: MouseEvent): void {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    this.googleReverseGeocode();
  }

  public onNoClick(): void {
    this.dialogRef.close({ success: false });
  }

  public onSaveClick(): void {
    const model = new Address(
      this.data.thisUser.UserID,
      this.addressLine1FormControl.value,
      this.addressLine2FormControl.value,
      this.cityFormControl.value,
      this.postalCodeFormControl.value,
      this.lat,
      this.lng,
      this.stateFormControl.value,
      this.countryFormControl.value
    );
    if (
      this.addressLine1FormControl.valid &&
      this.addressLine2FormControl.valid &&
      this.cityFormControl.valid &&
      this.stateFormControl.valid &&
      this.countryFormControl.valid &&
      this.postalCodeFormControl.valid
    ) {
      if (
        this.addressLine1FormControl.value ||
        this.addressLine2FormControl.value ||
        this.cityFormControl.value ||
        this.stateFormControl.value ||
        this.countryFormControl.value ||
        this.postalCodeFormControl.value
      ) {
        this.toastr.info('Please Wait !', 'info', {
          timeOut: 5000,
          closeButton: true,
        });
        // console.log(model);
        this.store.dispatch(POST_USER_ADDRESS({ userAddressInputData: model }));
        this.dialogRef.close({ success: true });
      } else {
        this.toastr.error('Please select before submitting !', 'error', {
          timeOut: 5000,
          closeButton: true,
        });
      }
    } else {
      this.toastr.error('Please select before submitting !', 'error', {
        timeOut: 5000,
        closeButton: true,
      });
    }
  }

  public clickOnCrosshair(): void {
    // move map center to selected address
    this.lat = this.geolocationLat;
    this.lng = this.geolocationLng;
    this.zoom = this.defaultZoom + 0.5;
  }

  constructor(
    public dialogRef: MatDialogRef<UpdateAddressComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { thisUser: UserInterface; UserAddress: UserAddressInterface },
    private toastr: ToastrService,
    public gMaps: GoogleMapsAPIWrapper,
    private ngZone: NgZone,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
      address: AddressStateInterface;
    }>
  ) {
    // set default values
    this.lat = 28.6448;
    this.lng = 77.216721;
    this.defaultZoom = 16;
    this.zoom = this.defaultZoom;

    // subscribe to store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        if (data.geolocationState.loaded) {
          this.lat = data.geolocationState.geolocation.coords.latitude;
          this.lng = data.geolocationState.geolocation.coords.longitude;
        } else if (data.googleGeolocationState.loaded) {
          this.lat = data.googleGeolocationState.googleGeolocation.location.lat;
          this.lng = data.googleGeolocationState.googleGeolocation.location.lng;
        } else if (data.geolocationState.error) {
          this.store.dispatch(GET_GOOGLE_GEOLOCATION());
        }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
