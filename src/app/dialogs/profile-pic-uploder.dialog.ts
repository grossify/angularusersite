import {
    Component,
    Inject,
    ViewChild,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef
  } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { UserInterface } from '../interfaces/user-interface';
import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { base64ToFile } from 'ngx-image-cropper/esm2015/lib/utils/blob.utils';

import { Store, select } from '@ngrx/store';
import { AuthStateInterface } from '../stores/reducers/auth.reducers';
import { UserState } from '../stores/reducers/user.reducer';
import { UPDATE_USER_PROFILE_PIC } from '../stores/actions/user.action';


@Component({
  selector: 'app-profile-pic-uploder-dialog',
  templateUrl: '../dialogs/profile-pic-uploder.html',
  styleUrls: ['../dialogs/profile-pic-uploder.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfilePicUploderDialogComponent implements OnDestroy {
  private ngUnsubscribe = new Subject();

  // @ViewChild(ImageCropperComponent, { static: false })
  // imageCropper: ImageCropperComponent;

  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public ifImageLoded = false;
  private croppedFile: Blob;

  public canvasRotation = 0;
  public profilePicLoading: boolean;

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.ifImageLoded = true;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.croppedFile = base64ToFile(event.base64);
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  onNoClick(): void {
    this.dialogRef.close({ success: false });
  }

  onCropAndSaveClick(): void {
    if (!this.croppedImage) {
      this.toastr.error('Please Select Image first !', 'Error', {
        timeOut: 3000
      });
      return;
    } else {
      this.toastr.info('Please Wait !', 'info', {
        timeOut: 3000
      });
      // return base64 image
      this.dialogRef.close({
        base64Image: this.croppedImage,
        success: true,
      //  resData: res
      });
    }

    const reqData: {userId: number, croppedFile: Blob} = {
        userId: this.data.UserData.UserID,
        croppedFile: this.croppedFile
    };
    
    this.store.dispatch(UPDATE_USER_PROFILE_PIC({reqData: reqData}));
  }

  onClickrotateLeft(): void {
    this.canvasRotation--;
  }

  onClickrotateRight(): void {
    this.canvasRotation++;
  }

  constructor(
    public dialogRef: MatDialogRef<ProfilePicUploderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { UserData: UserInterface },
    private toastr: ToastrService,
    private store: Store<{
      authState: AuthStateInterface,
      user: UserState
    }>,
    private cd: ChangeDetectorRef
  ) {
      // subscribe to applicationState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('user')
    )
    .subscribe( (user: UserState) => {
      this.profilePicLoading = user.updateUserPicStateInterface.loading;
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
