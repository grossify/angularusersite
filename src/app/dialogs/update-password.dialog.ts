import {
    Component,
    Inject,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserInterface } from '../interfaces/user-interface';
import { UserPassword } from '../classes/userPassword';
import { ToastrService } from 'ngx-toastr';
import {
    FormGroup,
    FormControl,
    Validators,
    ValidationErrors
} from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { CHANGE_PASSWORD } from '../stores/actions/user.action';
import { UserState } from '../stores/reducers/user.reducer';

interface ChangePassFormGroupInterface {
    oldPasswordFormControl: string;
    passwordFormControl: string;
    cPasswordFormControl: string;
}


@Component({
    selector: 'app-update-password-dialog',
    templateUrl: 'update-Password.html',
    styleUrls: ['update-Password.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdatePasswordDialogComponent {
public Userdata: UserInterface;
public password: string;
public changePasswordLoading: boolean;


public passwordMatcher = new MyErrorStateMatcher();
public oldPasswordMatcher = new MyErrorStateMatcher();

private ngUnsubscribe = new Subject();

public changePasswordForm = new FormGroup({
    
    oldPasswordFormControl: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(128),
        Validators.pattern('^[^ ]+$')
    ]),
    passwordFormControl: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(128),
        Validators.pattern('^[^ ]+$')
    ]),
    cPasswordFormControl: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(128),
        Validators.pattern('^[^ ]+$')
    ])
  });

  private showErrors(element: string, keyError: any): void {
    let text: string;
    switch (keyError) {
      case 'required':
        text = `${element} is required!`;
        break;
      case 'pattern':
        text = `${element} has spaces, No spaces are allowed!`;
        break;
      case 'minlength':
        text = `${element} is too short!`;
        break;
      case 'maxlength':
        text = `${element} is very long!`;
        break;
    }
    this.toastr.error(text, `Error in ${element}!`, {
      timeOut: 5000,
      closeButton: true
    });
  }

  private getFormValidationErrors() {
    Object.keys(this.changePasswordForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.changePasswordForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          // console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          switch (key) {
            case 'oldPasswordFormControl':
              this.showErrors('Old Password', keyError);
              break;
            case 'passwordFormControl':
              this.showErrors('Password', keyError);
              break;
            case 'cPasswordFormControl':
              this.showErrors('Confirm Password', keyError);
              break;
          }
        });
      }
    });
  }


  public onSubmitChangePassDetails(value: ChangePassFormGroupInterface): boolean {
        if (!this.changePasswordForm.valid) {
            this.getFormValidationErrors();
            return false;
        }
        if (value.passwordFormControl !== value.cPasswordFormControl) {
            this.toastr.error('Password and confirm password is not same', 'error', {
                timeOut: 3000,
                closeButton: true
            });
            return false;
        }
        if (value.oldPasswordFormControl === value.passwordFormControl) {
            this.toastr.info('Please use new password.', 'info', {
                timeOut: 3000,
                closeButton: true
            });
            return false;
        }
        this.toastr.info('Please wait', 'Info', {
            timeOut: 3000,
            closeButton: true
        });

        const model = new UserPassword(
            value.passwordFormControl,
            value.oldPasswordFormControl,
            this.data.Userdata.UserID
        );

        this.store.dispatch(CHANGE_PASSWORD({passData: model}));
        this.dialogRef.close({ success: true });
        return true;
    }


    constructor(
        public dialogRef: MatDialogRef<UpdatePasswordDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { Userdata: UserInterface },
        private toastr: ToastrService,
        private store: Store<{
        user: UserState,
        }>,
        private cd: ChangeDetectorRef
    ) {
        // subscribe to applicationState store state
        store
        .pipe(
            takeUntil(this.ngUnsubscribe),
            select('user')
        )
        .subscribe( (user: UserState) => {
            this.changePasswordLoading = user.changePasswordState.loading;
        });
    }

    onNoClick(): void {
        this.dialogRef.close({ success: false });
    }

// onSaveClick(): void {

//     if (
//     this.passwordFormControl.valid &&
//     this.cPasswordFormControl.valid &&
//     this.passwordFormControl.value !== this.cPasswordFormControl.value
//     ) {
//     this.toastr.error(
//         'Password Confirm Password does not match',
//         'Error',
//         {
//         timeOut: 3000,
//         closeButton: true
//         }
//     );
//     this.oldPasswordFormControl.reset();
//     this.passwordFormControl.reset();
//     this.cPasswordFormControl.reset();
//     return;
//     } else if (
//     this.passwordFormControl.valid &&
//     this.oldPasswordFormControl.valid
//     ) {
//     const model = new UserPassword(
//         this.passwordFormControl.value,
//         this.oldPasswordFormControl.value,
//         this.data.Userdata.UserID
//     );

//     this.store.dispatch(CHANGE_PASSWORD({passData: model}));
//     this.dialogRef.close({ success: true });
//     }
//     else {
//     this.toastr.error(
//     'Please Fill form before submitting',
//     'Error',
//     {
//         timeOut: 3000,
//         closeButton: true
//     }
//     );
//     }
// }
}
