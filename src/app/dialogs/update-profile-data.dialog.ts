import {
    Component,
    Inject,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef
  } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { UserInterface } from '../interfaces/user-interface';
import { User } from '../classes/user';
import { ToastrService } from 'ngx-toastr';
import { FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from './MyErrorStateMatcher';
import {
  IndianStateAndUnionTerritoriesData,
  CountriesData
} from '../globels';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { UserState } from '../stores/reducers/user.reducer';
import { UPDATE_USER_DATA } from '../stores/actions/user.action';


@Component({
  selector: 'app-update-profile-data-dialog',
  templateUrl: '../dialogs/update-profile-data-dialog.html',
  styleUrls: ['../dialogs/update-profile-data-dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateUserDataDialogComponent implements OnDestroy {
  private ngUnsubscribe = new Subject();
  public profileDataLoading: boolean;

  public stateAndUnion = IndianStateAndUnionTerritoriesData;
  public CountriesData = CountriesData;
  public FirstName: string;
  public LastName: string;
  public Gender: string;
  public DOB: string;
  public Altername_Number: string;
  public UserCity: string;
  public UserState: string;
  public UserCountry: string;
  public UserZip: string;

  public FirstNameFormControl = new FormControl(this.data.FirstName, [
    Validators.minLength(3),
    Validators.maxLength(256)
  ]);

  public LastNameFormControl = new FormControl(this.data.LastName, [
    Validators.minLength(3),
    Validators.maxLength(256)
  ]);

  public GenderFormControl = new FormControl(this.data.Gender, [
    Validators.minLength(3),
    Validators.maxLength(128)
  ]);

  public DOBFormControl = new FormControl(this.data.DOB, [
    Validators.minLength(3),
    Validators.maxLength(128)
  ]);

  public alternameNumberFormControl = new FormControl(
    this.data.Altername_Number,
    [Validators.minLength(10), Validators.maxLength(15)]
  );

  public UserCityFormControl = new FormControl(this.data.UserCity, [
    Validators.minLength(3),
    Validators.maxLength(128)
  ]);

  public UserStateFormControl = new FormControl(this.data.UserState, [
    Validators.minLength(3),
    Validators.maxLength(128)
  ]);

  public UserCountryFormControl = new FormControl(this.data.UserCountry, [
    Validators.minLength(3),
    Validators.maxLength(128)
  ]);

  public UserZipFormControl = new FormControl(this.data.UserZip, [
    Validators.minLength(6),
    Validators.maxLength(15)
  ]);

  public FirstNameMatcher = new MyErrorStateMatcher();
  public LastNameMatcher = new MyErrorStateMatcher();
  public GenderMatcher = new MyErrorStateMatcher();
  public DOBMatcher = new MyErrorStateMatcher();
  public alternameNumberMatcher = new MyErrorStateMatcher();
  public UserCityMatcher = new MyErrorStateMatcher();
  public UserStateMatcher = new MyErrorStateMatcher();
  public UserCountryMatcher = new MyErrorStateMatcher();
  public UserZipMatcher = new MyErrorStateMatcher();

  public model = new User();


  onNoClick(): void {
    this.dialogRef.close({ success: false });
  }

  onSaveClick(): void {
    this.toastr.info('Please Wait !', 'info', {
      timeOut: 3000
    });
    this.model.FirstName = this.FirstNameFormControl.value;
    this.model.LastName = this.LastNameFormControl.value;
    this.model.Gender = this.GenderFormControl.value;
    this.model.DOB = this.DOBFormControl.value;
    this.model.Altername_Number = this.alternameNumberFormControl.value;
    this.model.UserCity = this.UserCityFormControl.value;
    this.model.UserState = this.UserStateFormControl.value;
    this.model.UserCountry = this.UserCountryFormControl.value;
    this.model.UserZip = this.UserZipFormControl.value;
    if (
      this.FirstNameFormControl.valid &&
      this.LastNameFormControl.valid &&
      this.GenderFormControl.valid &&
      this.DOBFormControl.valid &&
      this.alternameNumberFormControl.valid &&
      this.UserCityFormControl.valid &&
      this.UserStateFormControl.valid &&
      this.UserCountryFormControl.valid &&
      this.UserZipFormControl.valid
    ) {
      if (
        this.FirstNameFormControl.value ||
        this.LastNameFormControl.value ||
        this.GenderFormControl.value ||
        this.DOBFormControl.value ||
        this.alternameNumberFormControl.value ||
        this.UserCityFormControl.value ||
        this.UserStateFormControl.value ||
        this.UserCountryFormControl.value ||
        this.UserZipFormControl.value
      ) {
        this.model.UserId = this.data.UserID;

        this.store.dispatch(UPDATE_USER_DATA({reqData: this.model}));

        // return back new data
        this.dialogRef.close({ success: true });

      } else {
        this.toastr.error(
          'Please Fill at least one field before submitting !',
          'error',
          {
            timeOut: 3000
          }
        );
      }
    } else {
      this.toastr.error(
        'Please Fill valid details before submitting !',
        'error',
        {
          timeOut: 3000
        }
      );
    }
  }

  constructor(
    public dialogRef: MatDialogRef<UpdateUserDataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserInterface,
    private toastr: ToastrService,
    private store: Store<{
      user: UserState
    }>,
    private cd: ChangeDetectorRef
  ) {

    // subscribe to authState store state
    store
    .pipe(
      takeUntil(this.ngUnsubscribe),
      select('user')
    )
    .subscribe( (data: UserState) => {
      this.profileDataLoading = data.updateUserProfileDataState.loading;
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
