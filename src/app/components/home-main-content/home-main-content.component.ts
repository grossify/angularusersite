import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { StoreCategoriesInterface } from '../../interfaces/store-categories-interface';
import { Router } from '@angular/router';
import { UserInterface } from '../../interfaces/user-interface';
import { GetBannerInterface } from '../../interfaces/get-banner-interface';
import {
  GetNearByStoreInterface,
  GetNearByStoreReqInterface,
  GetNearByStoreItemInterface,
} from '../../interfaces/get-near-by-store';
import {
  GetNearByFevouriteStoresReqInterface,
  FevouriteStoresInterface,
  GetNearByFavouriteStoreItemInterface,
} from '../../interfaces/fevourite-stores-interface';
import { UserAddressInterface } from '../../interfaces/user-address-interface';
import {
  CityStoresInterface,
  GetCityStoresItemInterface,
  CityStoresReqInterface,
} from '../../interfaces/city-stores-interface';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../stores/reducers/auth.reducers';
import { StoreStateInterface } from '../../stores/reducers/store.reducer';
import { AddressStateInterface } from '../../stores/reducers/address.reducer';
import { LatLngCityStateInterface } from '../../stores/reducers/latLngCity.reducer';
import { GET_BANNER } from '../../stores/actions/application.action';
import {
  GET_NEARBY_STORES,
  GET_NEARBY_FAVOURITE_STORES,
  GET_STORE_BY_CITY,
} from '../../stores//actions/store.action';

@Component({
  selector: 'app-home-main-content',
  templateUrl: './home-main-content.component.html',
  styleUrls: ['./home-main-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeMainContentComponent implements OnInit, OnDestroy {
  public nearByStore: Array<GetNearByStoreItemInterface>;
  public allBanner: GetBannerInterface[];
  public FevouriteStores: Array<GetNearByFavouriteStoreItemInterface>;
  public storesByCity: CityStoresInterface;
  public storeCategories: { [catId: number]: StoreCategoriesInterface };
  public storePlaceholderImage =
    './assets/images/logo/image-store-placeholder-logo.png';
  public Userdata: UserInterface;
  public UserAddress: UserAddressInterface;

  private transform = 100;
  public selectedIndex = 0;

  private ngUnsubscribe = new Subject();

  public fevouriteStoresLoading: boolean;
  public fevouriteStoresLoaded: boolean;
  public nearByStoresLoading: boolean;
  public nearByStoresLoaded: boolean;
  public categoriesStoresLoading: boolean;
  public categoriesStoresLoaded: boolean;
  public storesByCityLoading: boolean;
  public storesByCityLoaded: boolean;
  public bannerLoading: boolean;
  public bannerLoaded: boolean;

  private getNearByStoresFromServer(reqData: GetNearByStoreReqInterface): void {
    this.store.dispatch(GET_NEARBY_STORES({ nearbyStoreData: reqData }));
  }

  private getNearByFavoriteStoresFromServer(
    reqData: GetNearByFevouriteStoresReqInterface
  ): void {
    this.store.dispatch(
      GET_NEARBY_FAVOURITE_STORES({ nearbyFavouriteStoreData: reqData })
    );
  }

  private getStoreByCity(reqDataCity: CityStoresReqInterface) {
    this.store.dispatch(GET_STORE_BY_CITY({ reqData: reqDataCity }));
  }

  public clickOnNearbyStoreItem(store: GetNearByStoreItemInterface): void {
    const id = store.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  public clickOnFavouriteStoresItem(
    store: GetNearByFavouriteStoreItemInterface
  ): void {
    const id = store.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  public clickOnCityStoresItem(cityStore: GetCityStoresItemInterface): void {
    const id = cityStore.store_id;
    this.router.navigate(['/etc/stores', id]);
  }

  public clickOnStoreCategoryItem(
    storeCategory: StoreCategoriesInterface
  ): void {
    const id = storeCategory.cat_id;
    this.router.navigate(['/etc/store-category', id]);
  }

  private getBanner(): void {
    this.store.dispatch(GET_BANNER());
  }

  private downSelected(i: number) {
    this.transform = 100 - i * 50;
    this.selectedIndex = this.selectedIndex + 1;
    if (this.selectedIndex > 4) {
      this.selectedIndex = 0;
    }
  }

  selected(x: number) {
    this.downSelected(x);
    this.selectedIndex = x;
  }

  keySelected(x: number) {
    this.downSelected(x);
    this.selectedIndex = x;
  }

  private loopSlider(): void {
    let loop = 0;
    const NumberOfImages = Object.keys(this.allBanner).length;
    setInterval(() => {
      if (loop === NumberOfImages - 1) {
        loop = 0;
      } else {
        loop++;
        this.cd.markForCheck();
      }
      this.selected(loop);
    }, 3000);
  }

  constructor(
    private router: Router,
    private store: Store<{
      applicationState: applicationState,
      authState: AuthStateInterface,
      store: StoreStateInterface,
      address: AddressStateInterface,
      latLngCity: LatLngCityStateInterface,
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.getBanner();

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('applicationState'))
      .subscribe((data: applicationState) => {
        if (data.bannerState) {
          this.bannerLoading = data.bannerState.loading;
          this.bannerLoaded = data.bannerState.loaded;
          // this.cd.markForCheck();
          this.allBanner = data.bannerState.bannerData;
          if (data.bannerState.loaded) {
            this.loopSlider();
          }
        }
      });

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
        }
      });

    // subscribe to store's store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('store'))
      .subscribe((data: StoreStateInterface) => {
        this.storeCategories = data.StoreCatState.storeCategories;
        this.categoriesStoresLoading = data.StoreCatState.loading;
        this.categoriesStoresLoaded = data.StoreCatState.loaded;
        this.fevouriteStoresLoading =
          data.getNearbyFavouriteStoresState.loading;
        this.fevouriteStoresLoaded = data.getNearbyFavouriteStoresState.loaded;
        this.nearByStoresLoading = data.getNearbyStoresState.loading;
        this.nearByStoresLoaded = data.getNearbyStoresState.loaded;
        this.storesByCityLoading = data.getCityStoresState.loading;
        this.storesByCityLoaded = data.getCityStoresState.loaded;
        this.storesByCity = data.getCityStoresState.StoresRes;

        if (
          data.getNearbyFavouriteStoresState &&
          data.getNearbyFavouriteStoresState.NearbyFavouriteStoresRes
        ) {
          this.FevouriteStores =
            data.getNearbyFavouriteStoresState.NearbyFavouriteStoresRes.result;
        }
        if (
          data.getNearbyStoresState &&
          data.getNearbyStoresState.NearbystoresRes
        ) {
          this.nearByStore = data.getNearbyStoresState.NearbystoresRes.result;
        }
        this.cd.markForCheck();
      });

    // subscribe to address store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('address'))
      .subscribe((data: AddressStateInterface) => {
        if (
          this.Userdata &&
          data.userAddressState &&
          data.userAddressState.loaded &&
          data.userAddressState.UserAddress &&
          data.userAddressState.UserAddress.city
        ) {
          this.UserAddress = data.userAddressState.UserAddress;
          const reqDataFav: GetNearByFevouriteStoresReqInterface = {
            userId: this.Userdata.UserID,
            latitude: this.UserAddress.latitude,
            longitude: this.UserAddress.longitude,
            pincode: this.UserAddress.postal_code,
            limit: 20,
            offset: 0,
            city: this.UserAddress.city,
          };

          const reqData: GetNearByStoreReqInterface = {
            userId: this.Userdata.UserID,
            latitude: this.UserAddress.latitude,
            longitude: this.UserAddress.longitude,
            pincode: this.UserAddress.postal_code,
            limit: 20,
            offset: 0,
            city: this.UserAddress.city,
          };

          const reqDataCity: CityStoresReqInterface = {
            userId: this.Userdata.UserID,
            city: this.UserAddress.city,
          };

          this.getNearByStoresFromServer(reqData);
          this.getNearByFavoriteStoresFromServer(reqDataFav);
          this.getStoreByCity(reqDataCity);
        }
      });

    // subscribe to latLngCity store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('latLngCity'))
      .subscribe((data: LatLngCityStateInterface) => {
        if (data.latitude && data.longitude) {
          const reqData: GetNearByStoreReqInterface = {
            userId: 1,
            latitude: data.latitude,
            longitude: data.longitude,
            pincode: data.postalCode ? data.postalCode : '',
            limit: 20,
            offset: 0,
            city: data.city ? data.city : '',
          };

          // const reqDataFav: GetNearByFevouriteStoresReqInterface = {
          //   userId: 1,
          //   latitude: data.latitude,
          //   longitude: data.longitude,
          //   pincode: data.postalCode ? data.postalCode : '',
          //   limit: 20,
          //   offset: 0,
          //   city: data.city ? data.city : '',
          // };

          this.getNearByStoresFromServer(reqData);

          // call  getNearByFavoriteStoresFromServer api only after login
          // this.getNearByFavoriteStoresFromServer(reqDataFav);
        }

        if (data.city) {
          const reqDataCity: CityStoresReqInterface = {
            userId: 1,
            city: data.city,
          };
          this.getStoreByCity(reqDataCity);
        }
      });
  }

  ngOnInit() {
    // this.getAllStoresFromServer();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
