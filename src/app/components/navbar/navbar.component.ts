import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
// import { MatDialog } from '@angular/material/dialog';
import { UserInterface } from '../../interfaces/user-interface';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
// import { UpdateAddressComponent } from '../../dialogs/update-address-dialog.component';
import { UserAddressInterface } from '../../interfaces/user-address-interface';
import { ToastrService } from 'ngx-toastr';

import { Store, select } from '@ngrx/store';
import { applicationState } from '../../stores/reducers/application.reducer';
import { AuthStateInterface } from '../../stores/reducers/auth.reducers';
import { StoreStateInterface } from '../../stores/reducers/store.reducer';
import { UserState } from '../../stores/reducers/user.reducer';
import { AddressStateInterface } from '../../stores/reducers/address.reducer';
import { CartStateInterface } from '../../stores/reducers/cart.reducer';
import { LatLngCityStateInterface } from '../../stores/reducers/latLngCity.reducer';
import { TOGGLE_MAIN_SIDENAV_BAR } from '../../stores/actions/application.action';
import { GET_USER_ADDRESS } from '../../stores/actions/address.action';
import { GET_CART } from '../../stores/actions/cart.action';
import { LOGOUT } from '../../stores/actions/auth.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent implements OnInit, OnDestroy {
  public UserAddress: UserAddressInterface;

  public loginRegistrationMenuState: boolean;

  public isUserLoggedIn: boolean;
  public isUserLocationSaved: boolean;

  public hideOnSearchIconClick: boolean;

  public addressText: string;

  public cartBadgeItemsInCart: number;

  private ngUnsubscribe = new Subject();
  public Userdata: UserInterface;

  @ViewChild('menuTriger', { static: false }) MenuTrigger: MatMenuTrigger;

  public openLocationDialog(): void {
    if (!this.Userdata) {
      // not logged in !this.Userdata.UserID
      this.router.navigate(['/etc/login']);
    } else {
      this.router.navigate(['/etc/settings/address/edit-address']);
      // const dialogRef = this.dialog.open(UpdateAddressComponent, {
      //   width: '85%',
      //   height: '85%',
      //   data: { thisUser: this.Userdata, UserAddress: this.UserAddress }
      // });
      // dialogRef.afterClosed().subscribe(result => {});
    }
  }

  public toggelState() {
    this.store.dispatch(TOGGLE_MAIN_SIDENAV_BAR());
  }

  public clickOnUserIcon(): void {
    this.MenuTrigger.openMenu();
  }

  public clickOnLogin(): void {
    if (!this.isUserLoggedIn) {
      this.router.navigate(['/etc/login']);
    } else {
      this.MenuTrigger.openMenu();
    }
  }

  public clickOnLogOut(): void {
    this.store.dispatch(LOGOUT());
  }

  public clickOnSearchIcon(): void {
    this.hideOnSearchIconClick = true;
  }

  public clickOnCloseSearch(): void {
    this.hideOnSearchIconClick = false;
  }

  private askForAddress(): void {
    this.toastr.info(
      'please update your delivery address before shopping, so that we can show you nearby stores.',
      'Please Update Your Address',
      {
        timeOut: 5000,
        closeButton: true,
      }
    );
    this.router.navigate(['/etc/settings/address']);
    this.openLocationDialog();
  }

  constructor(
    private toastr: ToastrService,
    // public dialog: MatDialog,
    private router: Router,
    private store: Store<{
      applicationState: applicationState;
      authState: AuthStateInterface;
      store: StoreStateInterface;
      user: UserState;
      address: AddressStateInterface;
      cart: CartStateInterface;
      latLngCity: LatLngCityStateInterface;
    }>,
    private cd: ChangeDetectorRef
  ) {
    this.loginRegistrationMenuState = true;

    // subscribe to authState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('authState'))
      .subscribe((data: AuthStateInterface) => {
        this.isUserLoggedIn = data.isAuthenticated;
        // check for user login
        // if logged in check for data.user[0].isAddressAvilabe
        // isAddressAvilabe = true, load saved address
        // isAddressAvilabe = false, rediret user to address setting
        if (data.user && data.user[0]) {
          this.Userdata = data.user[0];
          this.store.dispatch(GET_CART({ userId: this.Userdata.UserID }));
          if (data.user[0].isAddressAvilabe) {
            this.store.dispatch(
              GET_USER_ADDRESS({ userId: this.Userdata.UserID })
            );
          } else {
            // ask for address upload
            this.askForAddress();
          }
        }
      });

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('address'))
      .subscribe((data: AddressStateInterface) => {
        if (
          data.userAddressState &&
          data.userAddressState.UserAddress &&
          data.userAddressState.UserAddress.city
        ) {
          this.addressText = data.userAddressState.UserAddress.city;
          this.UserAddress = data.userAddressState.UserAddress;
          this.isUserLocationSaved = data.userAddressState.loaded;
          this.cd.markForCheck();
          if (
            data.userAddressState.UserAddress &&
            (!data.userAddressState.UserAddress.city ||
              !data.userAddressState.UserAddress.latitude ||
              !data.userAddressState.UserAddress.longitude)
          ) {
            // ask for address upload
            this.askForAddress();
          }
        } else {
          if (
            data.userAddressState.UserAddress &&
            (!data.userAddressState.UserAddress.city ||
              !data.userAddressState.UserAddress.latitude ||
              !data.userAddressState.UserAddress.longitude)
          ) {
            // ask for address upload
            this.askForAddress();
          }
        }
      });

    // subscribe to cart store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('cart'))
      .subscribe((data: CartStateInterface) => {
        if (data.CartState.loaded) {
          this.cartBadgeItemsInCart = data.CartState.Cart.num_of_items;
          this.cd.markForCheck();
        } else {
          this.cartBadgeItemsInCart = undefined;
          this.cd.markForCheck();
        }
      });

    // subscribe to applicationState store state
    store
      .pipe(takeUntil(this.ngUnsubscribe), select('latLngCity'))
      .subscribe((data: LatLngCityStateInterface) => {
        if (data.city && !this.isUserLoggedIn) {
          // user not logged in and city is taken by google geocode reverse api
          this.addressText = data.city;
          this.cd.markForCheck();
        } else if (data.city && this.isUserLoggedIn && !this.UserAddress) {
          // user logged in and city is taken by google geocode reverse api address is not saved by user
          this.addressText = data.city;
          this.cd.markForCheck();
        } else if (
          data.city &&
          this.isUserLoggedIn &&
          this.UserAddress &&
          !this.UserAddress.city
        ) {
          // user logged in and city is taken by google geocode reverse api address city is not saved by user
          this.addressText = data.city;
          this.cd.markForCheck();
        }
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
