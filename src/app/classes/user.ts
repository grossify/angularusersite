export class User {
  constructor(
    public password?: string,
    public rememberToken?: string,
    public base64ProfilePic?: string,
    public c_password?: string,

    public UserId?: number,
    public UserEmail?: string,
    public UserPassword?: string,
    public UserName?: string,
    public FirstName?: string,
    public LastName?: string,
    public UserCity?: string,
    public UserState?: string,
    public UserZip?: string,
    public UserEmailVerified?: boolean,
    public UserRegistrationDate?: Date,
    public UserVerificationCode?: string,
    public UserIP?: string,
    public UserPhone?: string,
    public UserCountry?: string,
    public isAddressAvilabe?: boolean,
    public userProfile?: string,
    public DOB?: Date,
    public Gender?: string,
    public Altername_Number?: string
  ) {}
}
