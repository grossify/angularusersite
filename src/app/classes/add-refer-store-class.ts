export class AddReferStoreClass {
  constructor(
    public userid?: number,
    public store_name?: string,
    public stor_address?: string,
    public owner_name?: string,
    public contact_number?: string,
    public email?: string,
  ) {}
}
