export class Order {
  constructor(
    public UserID?: number,
    public storeId?: number,
    public OrderNote?: string
  ) {}
}
