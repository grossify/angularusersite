export class Address {
  constructor(
    public userid: number,
    public address_line1: string,
    public address_line2: string,
    public city: string,
    public postal_code: string,
    public lattitude: number,
    public longitude: number,
    public state?: string,
    public country?: string
  ) {}
}
