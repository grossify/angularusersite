export class UserPassword {
  constructor(
    public newPass: string,
    public oldPass: string,
    public userId: number
  ) {}
}
