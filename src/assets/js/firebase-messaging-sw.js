// import {
//   firebaseConfig
// } from './app/globels';

const firebaseConfig = {
  apiKey: 'AIzaSyCmzFDuMEmjtjutBfVgLCdIwAehMU8ifIg',
  authDomain: 'grossify-v1.firebaseapp.com',
  databaseURL: 'https://grossify-v1.firebaseio.com',
  projectId: 'grossify-v1',
  storageBucket: 'grossify-v1.appspot.com',
  messagingSenderId: '293070585185',
  appId: '1:293070585185:web:62e7b34a04cc32b37a1920'
};

// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
// importScripts('/__/firebase/6.3.4/firebase-app.js');
// importScripts('/__/firebase/6.3.4/firebase-messaging.js');
// importScripts('/__/firebase/init.js');

importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');

firebase.initializeApp({
  'messagingSenderId': firebaseConfig.messagingSenderId
});


const messaging = firebase.messaging();

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.
 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');
 // Initialize the Firebase app in the service worker by passing in the
 // messagingSenderId.
 firebase.initializeApp({
   'messagingSenderId': 'YOUR-SENDER-ID'
 });
 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]
 **/


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]

// navigator.serviceWorker.register('./assets/js/firebase-messaging-sw.js')
//   .then((registration) => {
//     messaging.useServiceWorker(registration);

//     // Request permission and get token.....
//   });


messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

// [END background_handler]
